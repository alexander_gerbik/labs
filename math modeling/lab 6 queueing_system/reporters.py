from collections import defaultdict

import numpy as np
from matplotlib import pyplot as plt

from utils import Signal, nwise


class RequestReporter(object):
    def __init__(self, expected_request_amount, bins=50, batch_size=1000):
        self._scheduler = None
        self._expected_amount = expected_request_amount
        self._processed_requests = []
        self._bins = bins
        self._batch_size = batch_size
        self._processed_requests_amount = 0
        self.got_space = Signal()

    def prepare(self, scheduler):
        self._scheduler = scheduler

    def deliver(self, request):
        request.finished_at = self._scheduler.current_time
        self._processed_requests.append(request)
        self._processed_requests_amount += 1
        if self._processed_requests_amount % self._batch_size == 0:
            print(f'Processed {self._processed_requests_amount} requests.')
        return True

    def report(self):
        denial = self._denial_probability()
        print(f'Denial probability: {denial:.3%}')
        intensity = self._output_intensity()
        print(f'Output intensity: {intensity:.3} requests/sec')
        intervals, mean, variance = self._sequential_intervals()
        plt.title('Intervals between consecutive requests.'
                  f' Mean = {mean:.3}, Variance = {variance:.3}')
        plt.hist(intervals, normed=True, bins=self._bins)
        plt.figure()
        times, mean, variance = self._process_time()
        plt.title('Request processing times.'
                  f' Mean = {mean:.3}, Variance = {variance:.3}')
        plt.hist(times, normed=True, bins=self._bins)
        plt.show()

    def _sequential_intervals(self):
        finishes = np.array([r.finished_at for r in self._processed_requests])
        intervals = np.diff(finishes)
        mean = intervals.mean()
        variance = intervals.std()
        variance *= variance
        return intervals, mean, variance

    def _process_time(self):
        times = np.array(
            [r.finished_at - r.created_at for r in self._processed_requests])
        mean = times.mean()
        variance = times.std()
        variance *= variance
        return times, mean, variance

    def _output_intensity(self):
        interval = (self._processed_requests[-1].finished_at
                    - self._processed_requests[0].finished_at)
        return len(self._processed_requests) / interval

    def _denial_probability(self):
        return 1 - len(self._processed_requests) / self._expected_amount


class BufferReporter(object):
    def __init__(self, scheduler, buffer, phase_id):
        self._scheduler = scheduler
        self._phase_id = phase_id
        self._buffer = buffer
        self._changes = []
        self._buffer.state_changed.connect(self._log)

    def report(self):
        print(f'Buffer {self._phase_id} average requests amount:'
              f' {self._average_load():.3}')

    def _average_load(self):
        full_duration = 0.0
        s = 0.0
        for prev, current in nwise(self._changes, 2):
            dur = current[0] - prev[0]
            state = prev[2]
            full_duration += dur
            s += state * dur
        return s / full_duration

    def _log(self, before, after):
        self._changes.append((self._scheduler.current_time, before, after))


class ChannelReporter(object):
    def __init__(self, scheduler, channel, phase_id, channel_id):
        self._scheduler = scheduler
        self._phase_id = phase_id
        self._channel_id = channel_id
        self._channel = channel
        self._changes = []
        self._channel.changed.connect(self._log)

    def report(self):
        probabilities = self._calculate_probabilities()
        s = '\n'.join(f'\t{s}: {p:.3%}' for s, p in probabilities.items())
        print(f'Phase {self._phase_id} channel {self._channel_id}'
              f' state probabilities:\n{s}')

    def _log(self, before, after):
        self._changes.append((self._scheduler.current_time, before, after))

    def _calculate_probabilities(self):
        full_duration = 0.0
        probabilities = defaultdict(int)
        probabilities['WAITING'] += self._changes[0][0]
        full_duration += self._changes[0][0]
        for prev, current in nwise(self._changes, 2):
            dur = current[0] - prev[0]
            state = prev[2]
            full_duration += dur
            probabilities[state] += dur
        for k in probabilities:
            probabilities[k] /= full_duration
        return probabilities
