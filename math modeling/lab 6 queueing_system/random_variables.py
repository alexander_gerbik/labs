import random

from scipy.stats import expon, norm, triang


class Uniform(object):
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def rv(self):
        return random.uniform(self.a, self.b)


class Expon(object):
    def __init__(self, lambd=1.0):
        self.lambd = lambd
        self._expon = expon(scale=1 / self.lambd)

    def rv(self):
        return self._expon.rvs()
        # return expon.rvs(scale=1 / self.lambd)


class Gauss(object):
    def __init__(self, mean, std):
        self.mean = mean
        self.std = std
        self._norm = norm(loc=self.mean, scale=self.std)

    def rv(self):
        return self._norm.rvs()


class Triang(object):
    def __init__(self, a, b, c=0.5):
        self.a = a
        self.b = b
        self.c = c
        loc = a
        scale = b - a
        self._triang = triang(loc=loc, scale=scale, c=c)

    def rv(self):
        return self._triang.rvs()
