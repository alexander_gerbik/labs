from random_variables import Expon
from scheduler import Scheduler
from system import Buffer, Channel, Phase


def test_buffer():
    scheduler = Scheduler()
    input_buffer = Buffer(3)
    output_buffer = Buffer(2)
    next_phase = Phase(output_buffer, [])
    channel = Channel(Expon(1))
    channel.prepare(scheduler, input_buffer, next_phase)
    channel.begin_work()
    assert channel.state == Channel.WAITING
    input_buffer.push(1)
    assert channel.state == Channel.WORKING
    assert channel.request == 1
    scheduler.advance()
    assert channel.state == Channel.WAITING
    assert channel.request is None
    input_buffer.push(2)
    assert channel.state == Channel.WORKING
    assert channel.request == 2
    scheduler.advance()
    input_buffer.push(3)
    assert channel.state == Channel.WORKING
    input_buffer.push(4)
    scheduler.advance()
    assert channel.state == Channel.BLOCKED
    output_buffer.pop()
    assert channel.state == Channel.WORKING
    assert channel.request == 4
    output_buffer.pop()
    scheduler.advance()
    assert channel.state == Channel.WAITING
    assert channel.request is None
