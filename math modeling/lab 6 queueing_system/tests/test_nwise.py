from utils import nwise


def test_pairwise():
    a = list(range(5))
    b = list(nwise(a, 2))
    assert b == [(0, 1), (1, 2), (2, 3), (3, 4)]


def test_nwise_3():
    a = list(range(5))
    b = list(nwise(a, 3))
    assert b == [(0, 1, 2), (1, 2, 3), (2, 3, 4)]


def test_nwise_length_equal_n():
    a = list(range(5))
    b = list(nwise(a, 5))
    assert b == [(0, 1, 2, 3, 4), ]


def test_nwise_length_less_than_n():
    a = list(range(5))
    b = list(nwise(a, 6))
    assert b == []
