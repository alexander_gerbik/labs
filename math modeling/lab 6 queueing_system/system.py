from collections import deque

from reporters import BufferReporter, ChannelReporter
from scheduler import Scheduler
from utils import Signal


class System(object):
    def __init__(self, request_generator, phases, final_receiver,
                 initial_time=0.0):
        self._scheduler = Scheduler(initial_time)
        self._reporters = []
        self._phases = phases
        self._generator = request_generator
        self._receiver = final_receiver
        self.prepare()
        self.__init_reporters()

    def prepare(self):
        prev_phase = self._receiver
        prev_phase.prepare(self._scheduler)
        for phase in reversed(self._phases):
            phase.prepare(self._scheduler, prev_phase)
            prev_phase = phase
            for channel in phase._channels:
                channel.begin_work()
        self._generator.prepare(self._scheduler, prev_phase)

    def __init_reporters(self):
        rs = self._reporters
        rs.append(self._receiver)
        for i, phase in reversed(list(enumerate(self._phases))):
            for j, channel in enumerate(phase._channels):
                rs.append(ChannelReporter(self._scheduler, channel, i, j))
            rs.append(BufferReporter(self._scheduler, phase._buffer, i))
        self._reporters = list(reversed(rs))

    def run(self):
        self._generator.generate(self._scheduler)
        self._scheduler.run_until_complete()
        assert all(p.is_finished for p in self._phases)

    def report(self):
        for r in self._reporters:
            r.report()


class Phase(object):
    def __init__(self, buffer, channels):
        self._scheduler = None
        self._receiver = None
        self._buffer = buffer
        self._channels = channels
        self.got_space = buffer.got_space

    def deliver(self, item):
        return self._buffer.push(item)

    def prepare(self, scheduler, receiver):
        self._scheduler = scheduler
        self._receiver = receiver
        for channel in self._channels:
            channel._buffer = self._buffer
            channel._scheduler = self._scheduler
            channel._receiver = self._receiver

    @property
    def is_finished(self):
        return len(self._buffer) == 0 and all(
            c.state == Channel.WAITING and c.request is None for c in
            self._channels)


class Buffer(object):
    def __init__(self, capacity):
        self._capacity = capacity
        self._items = deque(maxlen=capacity)
        self.got_item = Signal()
        self.got_space = Signal()
        self.state_changed = Signal()

    def __len__(self, *args, **kwargs):
        return self._items.__len__(*args, **kwargs)

    def push(self, item):
        n = len(self._items)
        if n == self._capacity:
            return False
        self._items.append(item)
        self.state_changed(n, n + 1)
        self.got_item.send()
        return True

    def pop(self):
        n = len(self._items)
        try:
            item = self._items.popleft()
        except IndexError:
            return None
        else:
            self.state_changed(n, n - 1)
            self.got_space.send()
            return item


class Channel(object):
    WAITING = "WAITING"
    WORKING = "WORKING"
    BLOCKED = "BLOCKED"

    def __init__(self, rv):
        self._scheduler = None
        self._buffer: Buffer = None
        self._receiver = None
        self._rv = rv
        self.request = None
        self._state = self.WAITING
        self.changed = Signal()

    def prepare(self, scheduler, buffer, receiver):
        self._scheduler = scheduler
        self._buffer = buffer
        self._receiver = receiver

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        if self._state == value:
            return
        prev = self._state
        self._state = value
        self.changed(prev, value)

    def begin_work(self):
        r = self._buffer.pop()
        if r is None:
            self.state = self.WAITING
            self._buffer.got_item.connect(self.begin_work)
        else:
            self._buffer.got_item.disconnect(self.begin_work)
            self.request = r
            time = self._rv.rv()
            self._scheduler.delay(self.end_work, time)
            self.state = self.WORKING

    def end_work(self, scheduler=None):
        success = self._receiver.deliver(self.request)
        if success:
            self._receiver.got_space.disconnect(self.end_work)
            self.request = None
            self.begin_work()
        else:
            self.state = self.BLOCKED
            self._receiver.got_space.connect(self.end_work)


class RequestGenerator(object):
    def __init__(self, rv, request_amount):
        self._scheduler = None
        self._next_phase = None
        self._rv = rv
        self._request_amount = request_amount
        self._generated_request_amount = 0

    def prepare(self, scheduler, next_phase):
        self._scheduler = scheduler
        self._next_phase = next_phase

    def generate(self, scheduler):
        if self._generated_request_amount == self._request_amount:
            return
        request = Request()
        request.created_at = scheduler.current_time
        self._next_phase.deliver(request)
        self._scheduler.delay(self.generate, self._rv.rv())
        self._generated_request_amount += 1


class Request(object):
    pass
