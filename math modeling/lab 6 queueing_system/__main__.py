from random_variables import Expon, Uniform, Gauss, Triang
from system import Channel, Buffer, System, RequestGenerator, Phase
from reporters import RequestReporter


def main():
    n = 10000
    # original
    system = System(
        RequestGenerator(Expon(1), n),
        [
            Phase(Buffer(3), [
                Channel(Uniform(3, 9)),
                Channel(Uniform(3, 9)),
                Channel(Uniform(3, 9)),
            ]),
            Phase(Buffer(5), [
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
            ]),
            Phase(Buffer(3), [
                Channel(Triang(3, 9)),
                Channel(Triang(3, 9)),
                Channel(Triang(3, 9)),
            ]),
            Phase(Buffer(4), [
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
            ]),
            Phase(Buffer(1), [
                Channel(Triang(2, 5)),
                Channel(Triang(2, 5)),
                Channel(Triang(2, 5)),
            ]),
        ],
        RequestReporter(n)
    )
    # enhanced
    system = System(
        RequestGenerator(Expon(1), n),
        [
            Phase(Buffer(9), [
                Channel(Uniform(3, 9)),
                Channel(Uniform(3, 9)),
                Channel(Uniform(3, 9)),
                Channel(Uniform(3, 9)),
                Channel(Uniform(3, 9)),
                Channel(Uniform(3, 9)),
                Channel(Uniform(3, 9)),
            ]),
            Phase(Buffer(11), [
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
            ]),
            Phase(Buffer(9), [
                Channel(Triang(3, 9)),
                Channel(Triang(3, 9)),
                Channel(Triang(3, 9)),
                Channel(Triang(3, 9)),
                Channel(Triang(3, 9)),
                Channel(Triang(3, 9)),
            ]),
            Phase(Buffer(10), [
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
                Channel(Gauss(5, 1)),
            ]),
            Phase(Buffer(10), [
                Channel(Triang(2, 5)),
                Channel(Triang(2, 5)),
                Channel(Triang(2, 5)),
                Channel(Triang(2, 5)),
            ]),
        ],
        RequestReporter(n)
    )
    system.run()
    system.report()


if __name__ == '__main__':
    main()
