from heapq import heappush, heappop


class Scheduler(object):
    def __init__(self, initial_time=0.0):
        self.current_time = initial_time
        self._order = 0
        self._events = []

    def schedule(self, event, at):
        if at < self.current_time:
            message = 'Scheduled time {} can not be less than current time {}'
            raise ValueError(message.format(at, self.current_time))
        self._schedule(event, at)

    def delay(self, event, interval):
        if interval < 0:
            raise ValueError('interval cannot be less than zero')
        self._schedule(event, self.current_time + interval)

    def _schedule(self, event, at):
        heappush(self._events, (at, self._order, event))
        self._order += 1

    def advance(self):
        try:
            time, order, event = heappop(self._events)
        except IndexError:
            raise RuntimeError('Trying execute event on empty queue')
        self.current_time = time
        event(self)

    def run_until_complete(self):
        while self._events:
            self.advance()
