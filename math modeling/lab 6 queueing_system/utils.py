from itertools import tee


class Signal(object):
    def __init__(self):
        self._receivers = set()

    def connect(self, receiver):
        self._receivers.add(receiver)

    def disconnect(self, receiver):
        self._receivers.discard(receiver)

    def send(self, *args, **kwargs):
        for receiver in self._receivers.copy():
            receiver(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        return self.send(*args, **kwargs)


def nwise(iterable, n=2):
    iters = tee(iterable, n)
    for i in range(n):
        for j in range(i):
            next(iters[i], None)
    return zip(*iters)
