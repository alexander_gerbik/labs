import events


class TwoDimensionalRandomVariable:
    def __init__(self, random_generator, probabilities, x_values, y_values):
        self.x_values = x_values
        self.y_values = y_values
        self.second_var_probs = []
        first_var_prob = []
        for i in range(probabilities.shape[0]):
            current_probability = 0
            current_probs = []
            for j in range(probabilities.shape[1]):
                current_probability += probabilities[i, j]
                current_probs.append(probabilities[i, j])
            first_var_prob.append(current_probability)
            self.second_var_probs.append(events.EventGroup(random_generator, current_probs))
        self.first_var_probs = events.EventGroup(random_generator, first_var_prob)

    def value(self):
        i = self.first_var_probs.occurs()
        j = self.second_var_probs[i].occurs()
        return self.x_values[i], self.y_values[j]
