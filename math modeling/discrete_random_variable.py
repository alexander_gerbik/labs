import events


class DiscreteRandomVariable:
    def __init__(self, random_generator, pdf):
        self.values = []
        probabilities = []
        for val, prob in pdf:
            self.values.append(val)
            probabilities.append(prob)
        self.event_group = events.EventGroup(random_generator, probabilities)

    def value(self):
        i = self.event_group.occurs()
        return self.values[i]
