class Generator:
    def __init__(self, seed, length, multiplier):
        self.length = length
        self.multiplier = multiplier
        self.state = seed % length

    def random(self):
        self.state = (self.state * self.multiplier) % self.length
        return self.state / self.length
