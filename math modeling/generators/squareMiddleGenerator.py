class Generator:
    def __init__(self, seed, power, base=10):
        self.base = base
        self.power = power + (power % 2)
        self.halfLength = power // 2
        self.state = seed % self._length()

    def random(self):
        self.state *= self.state
        self.state //= self.base ** self.halfLength
        self.state %= self._length()
        return self.state / self._length()

    def _length(self):
        return self.base ** self.power
