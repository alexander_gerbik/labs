import functools
import math

import scipy.stats as st


def get_mean_value(values):
    n = len(values)
    assert (n > 0)
    return sum(values) / n


def get_variance(values, mean_value):
    v = functools.reduce(lambda t, i: t + i * i, values, 0)
    return v / len(values) - mean_value * mean_value


def test_independence(values, s):
    n = len(values)
    v = 0
    for i in range(n - s):
        v += values[i] * values[i + s]
    return 12 * v / (n - s) - 3


def get_bar_chart(start, stop, amount, values):
    step = (stop - start) / amount
    freqs = [0.0] * amount
    for value in values:
        j = math.floor((value - start) / step)
        freqs[j] += 1
    for i in range(len(freqs)):
        freqs[i] /= len(values)
    return start, step, freqs


def pearson_test(analytic_distribution_function, sample, n, alpha):
    bins_amount = int(1 + math.log(n, 2))
    while n % bins_amount != 0:
        bins_amount -= 1
    degrees_of_freedom_amount = bins_amount - 1
    y_min = sample[0]
    y_max = sample[-1]
    a = []
    b = []
    n = []
    h = (y_max - y_min) / bins_amount
    values_amount = 0
    for i in range(bins_amount):
        a.append(y_min + i * h)
        b.append(y_min + (i + 1) * h)
        n.append(0)
        for x in sample:
            if a[i] <= x < b[i]:
                values_amount += 1
                n[i] += 1
    value = 0
    for i in range(bins_amount):
        analytic_probability = analytic_distribution_function(b[i]) - analytic_distribution_function(a[i])
        empiric_probability = n[i] / values_amount
        value += (analytic_probability - empiric_probability) ** 2 / analytic_probability
    value *= values_amount
    critical_point = st.chi2.ppf(1 - alpha, degrees_of_freedom_amount)
    accept = value < critical_point
    return accept, value, critical_point


def discrete_pearson_test(pdf, counts, n, alpha):
    analytic_dict = {}
    for val, prob in pdf:
        analytic_dict[val] = prob
    bins_amount = len(counts.keys())
    degrees_of_freedom_amount = bins_amount - 1
    value = 0
    for val in counts.keys():
        analytic_probability = analytic_dict[val]
        empiric_probability = counts[val] / n
        value += (analytic_probability - empiric_probability) ** 2 / analytic_probability
    value *= n
    critical_point = st.chi2.ppf(1 - alpha, degrees_of_freedom_amount)
    accept = value < critical_point
    return accept, value, critical_point


def get_mean_confidence_interval(n, alpha, mean, variance):
    stud_value = st.t.ppf(1 - alpha / 2, n)
    delta = math.fabs(stud_value * math.sqrt(variance) / math.sqrt(n))
    return mean - delta, mean + delta


def get_variance_confidence_interval(n, alpha, variance):
    right_chi = st.chi2.ppf((1 - (1 - alpha)) / 2, n - 1)
    left_chi = st.chi2.ppf((1 + (1 - alpha)) / 2, n - 1)
    return (n - 1) * variance / left_chi, (n - 1) * variance / right_chi


def get_distribution_function(pdf):
    def distr_func(x):
        if x < cdf[0][0]:
            return 0
        for j in range(len(pdf)):
            if x < cdf[j][0]:
                return cdf[j - 1][1]
        return 1

    cdf = pdf[:]
    cdf.sort(key=lambda x: x[0])
    for i in range(1, len(cdf)):
        current = cdf[i]
        cdf[i] = (current[0], current[1] + cdf[i - 1][1])
    return distr_func


def get_correlation(values_a, values_b):
    mean_a = get_mean_value(values_a)
    mean_b = get_mean_value(values_b)
    variance_a = get_variance(values_a, mean_a)
    variance_b = get_variance(values_b, mean_b)
    assert len(values_b) == len(values_a)
    ab = [0] * len(values_a)
    for i in range(len(values_a)):
        ab[i] = values_a[i] * values_b[i]
    mean_ab = get_mean_value(ab)
    result = mean_ab - mean_a * mean_b
    result /= math.sqrt(variance_a * variance_b)
    return result
