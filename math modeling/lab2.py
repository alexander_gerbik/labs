import functools
import math
import random
from collections import Counter

import events
import statistics


def main():
    n = 100000
    check_random_event(n)
    print()
    check_independent_events(n)
    print()
    check_dependent_events(n)
    print()
    check_event_group(n)


def check_random_event(n):
    prob = 0.5
    expected_mean = prob
    expected_variance = prob - prob * prob
    event = events.RandomEvent(random, prob)
    values = [event.occurs() for _ in range(n)]
    mean = statistics.get_mean_value(values)
    variance = statistics.get_variance(values, mean)
    print("Random event:")
    print("Probability: {}".format(prob))
    print("n = {}".format(n))
    happened = functools.reduce(lambda t, i: t + i, values, 0)
    not_happened = len(values) - happened
    print("Happened: {} times. Not happened: {} times.".format(happened, not_happened))
    print("Mean: {}, expected: {}, error: {}".format(mean, expected_mean, math.fabs(mean - expected_mean)))
    print("Variance: {}, expected: {}, error: {}".format(variance, expected_variance,
                                                         math.fabs(variance - expected_variance)))


def check_independent_events(n):
    probs = [0.2, 0.6]
    expected_mean = probs[0] * probs[1] * 3 + probs[0] * (1 - probs[1]) * 2 + (1 - probs[0]) * probs[1]
    expected_variance = probs[0] * probs[1] * 9 + probs[0] * (1 - probs[1]) * 4 + (1 - probs[0]) * probs[
        1] - expected_mean * expected_mean
    ind_events = events.IndependentEvents(random, probs)
    values = []
    for _ in range(n):
        a, b = ind_events.occurs()
        values.append(a * 2 + b)
    mean = statistics.get_mean_value(values)
    variance = statistics.get_variance(values, mean)
    not_happened_a_b, happened_b, happened_a, happened_a_b = count_occurances(values)
    print("Independent events:")
    print("n = {}".format(n))
    print("Probabilities: P(A)={}, P(B)={}".format(probs[0], probs[1]))
    print_occurances(not_happened_a_b, happened_b, happened_a, happened_a_b, n)
    print("Mean: {}, expected: {}, error: {}".format(mean, expected_mean, math.fabs(mean - expected_mean)))
    print("Variance: {}, expected: {}, error: {}".format(variance, expected_variance,
                                                         math.fabs(variance - expected_variance)))


def check_dependent_events(n):
    prob_a = 0.5
    prob_b_after_a = 0.7
    prob_b_after_not_a = 0.3
    prob_b = prob_a*prob_b_after_a + (1-prob_a)*prob_b_after_not_a
    dep_events = events.DependentEvents(random, prob_a, prob_b, prob_b_after_a)
    values = []
    for _ in range(n):
        a, b = dep_events.occurs()
        values.append(a * 2 + b)
    mean = statistics.get_mean_value(values)
    variance = statistics.get_variance(values, mean)
    not_happened_a_b, happened_b, happened_a, happened_a_b = count_occurances(values)
    print("Dependent events:")
    print("n = {}".format(n))
    print("Probabilities: P(A)={}, P(B/!A)={}, P(B/A)={}".format(prob_a, prob_b_after_not_a, prob_b_after_a))
    print_occurances(not_happened_a_b, happened_b, happened_a, happened_a_b, n)
    print("Mean: {}".format(mean))
    print("Variance: {}.".format(variance))


def check_event_group(n):
    probs = [0.3, 0.2, 0.1, 0.4]
    expected_mean = 0
    for i, p in enumerate(probs):
        expected_mean += i * p
    expected_variance = -expected_mean * expected_mean
    for i, p in enumerate(probs):
        expected_variance += i * i * p
    group_events = events.EventGroup(random, probs)
    values = [group_events.occurs() for _ in range(n)]
    mean = statistics.get_mean_value(values)
    variance = statistics.get_variance(values, mean)
    cnt = Counter(values)
    print("Event group:")
    print("n = {}".format(n))
    print("Probabilities: ")
    for i, p in enumerate(probs):
        print("P({})={} ".format(i, p), end="")
    print()
    for i, _ in enumerate(probs):
        print("Event {} happened: {} times({}%).".format(i, cnt[i], round(100 * cnt[i] / n, 2)))
    print("Mean: {}, expected: {}, error: {}".format(mean, expected_mean, math.fabs(mean - expected_mean)))
    print("Variance: {}, expected: {}, error: {}".format(variance, expected_variance,
                                                         math.fabs(variance - expected_variance)))


def print_occurances(not_happened_a_b, happened_b, happened_a, happened_a_b, n):
    ab = round(100 * happened_a_b / n, 2)
    a = round(100 * happened_a / n, 2)
    b = round(100 * happened_b / n, 2)
    not_ab = round(100 * not_happened_a_b / n, 2)
    print("B Happened: {} times({}%).".format(happened_a_b+happened_b, ab+b))
    print("A Happened: {} times({}%).".format(happened_a_b+happened_a, ab+a))
    print("A and B Happened: {} times({}%).".format(happened_a_b, ab))
    print("A Happened, B not Happened: {} times({}%).".format(happened_a, a))
    print("B Happened, A not Happened: {} times({}%).".format(happened_b, b))
    print("A and B not Happened: {} times({}%).".format(not_happened_a_b, not_ab))


def count_occurances(values):
    counter = Counter(values)
    not_happened_a_b = counter[0]
    happened_b = counter[1]
    happened_a = counter[2]
    happened_a_b = counter[3]
    return not_happened_a_b, happened_b, happened_a, happened_a_b


if __name__ == '__main__':
    main()
