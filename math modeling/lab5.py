import math
import random
from collections import Counter

import matplotlib.pyplot as plt
import numpy

import statistics
from two_dimensional_random_variable import TwoDimensionalRandomVariable


def main():
    n = 1000
    alpha = 0.05
    a_values = [0, 1, 2, 3]
    b_values = [0, 2, 4, 6, 8]
    probabilities = numpy.matrix([[0.04, 0.03, 0.03, 0.3, 0.1],
                                  [0.01, 0.08, 0.07, 0.05, 0.04],
                                  [0.05, 0.02, 0.03, 0.02, 0.03],
                                  [0.04, 0.03, 0.01, 0.01, 0.01]])
    pdf_a = []
    pdf_b = []
    probs_a = [0.0] * probabilities.shape[0]
    probs_b = [0.0] * probabilities.shape[1]
    for i in range(probabilities.shape[0]):
        for j in range(probabilities.shape[1]):
            probs_a[i] += probabilities[i, j]
            probs_b[j] += probabilities[i, j]
    expected_mean_a = 0
    expected_mean_b = 0
    for i, p in enumerate(probs_a):
        pdf_a.append((a_values[i], p))
        expected_mean_a += p * a_values[i]
    for i, p in enumerate(probs_b):
        pdf_b.append((b_values[i], p))
        expected_mean_b += p * b_values[i]
    expected_variance_a = -expected_mean_a * expected_mean_a
    expected_variance_b = -expected_mean_b * expected_mean_b
    for i, p in enumerate(probs_a):
        expected_variance_a += a_values[i] * a_values[i] * p
    for i, p in enumerate(probs_b):
        expected_variance_b += b_values[i] * b_values[i] * p

    two_dim_generator = TwoDimensionalRandomVariable(random, probabilities, a_values, b_values)
    values_gen_a = []
    values_gen_b = []
    for _ in range(n):
        a, b = two_dim_generator.value()
        values_gen_a.append(a)
        values_gen_b.append(b)
    mean_a = statistics.get_mean_value(values_gen_a)
    mean_b = statistics.get_mean_value(values_gen_b)
    variance_a = statistics.get_variance(values_gen_a, mean_a)
    variance_b = statistics.get_variance(values_gen_b, mean_b)

    lower_mean_a, upper_mean_a = statistics.get_mean_confidence_interval(len(values_gen_a), alpha, mean_a, variance_a)
    lower_mean_b, upper_mean_b = statistics.get_mean_confidence_interval(len(values_gen_b), alpha, mean_b, variance_b)
    lower_variance_a, upper_variance_a = statistics.get_variance_confidence_interval(len(values_gen_a), alpha,
                                                                                     variance_a)
    lower_variance_b, upper_variance_b = statistics.get_variance_confidence_interval(len(values_gen_b), alpha,
                                                                                     variance_b)
    cnt_a = Counter(values_gen_a)
    cnt_b = Counter(values_gen_b)
    accept_a, criterion_a, critical_point_a = statistics.discrete_pearson_test(pdf_a, cnt_a, n, alpha)
    accept_b, criterion_b, critical_point_b = statistics.discrete_pearson_test(pdf_b, cnt_b, n, alpha)
    print("Two dimensional random variable:")
    print("n = {}".format(n))
    print("Alpha: {}.".format(alpha))
    print("Probabilities: ")
    print(probabilities)

    print("First variable:")
    print("Values:")
    print(a_values)
    print("Mean: {}, expected: {}, error: {}".format(mean_a, expected_mean_a, math.fabs(mean_a - expected_mean_a)))
    mean_message_a = "" if lower_mean_a <= expected_mean_a <= upper_mean_a else "not "
    print("Mean is {}within confidence interval: [{}; {}].".format(mean_message_a, lower_mean_a, upper_mean_a))
    print("Variance: {}, expected: {}, error: {}".format(variance_a, expected_variance_a,
                                                         math.fabs(variance_a - expected_variance_a)))
    variance_message_a = "" if lower_variance_a <= expected_variance_a <= upper_variance_a else "not "
    print(
        "Variance is {}within confidence interval: [{}; {}].".format(variance_message_a, lower_variance_a,
                                                                     upper_variance_a))
    message_a = "Do not reject." if accept_a else "Reject."
    print("Pearson criterion. Critical point: {}. Value: {}. {}".format(critical_point_a,
                                                                        criterion_a, message_a))

    print("Second variable:")
    print("Values:")
    print(b_values)
    print("Mean: {}, expected: {}, error: {}".format(mean_b, expected_mean_b, math.fabs(mean_b - expected_mean_b)))
    mean_message_b = "" if lower_mean_b <= expected_mean_b <= upper_mean_b else "not "
    print("Mean is {}within confidence interval: [{}; {}].".format(mean_message_b, lower_mean_b, upper_mean_b))
    print("Variance: {}, expected: {}, error: {}".format(variance_b, expected_variance_b,
                                                         math.fabs(variance_b - expected_variance_b)))
    variance_message_b = "" if lower_variance_b <= expected_variance_b <= upper_variance_b else "not "
    print(
        "Variance is {}within confidence interval: [{}; {}].".format(variance_message_b, lower_variance_b,
                                                                     upper_variance_b))
    message_b = "Do not reject." if accept_b else "Reject."
    print("Pearson criterion. Critical point: {}. Value: {}. {}".format(critical_point_b,
                                                                        criterion_b, message_b))

    correlation = statistics.get_correlation(values_gen_a, values_gen_b)
    print("Correlation: {}.".format(correlation))

    fig, plots = plt.subplots(ncols=2)

    freqs = []
    args = []
    for v in cnt_a.keys():
        args.append(v)
        freqs.append(cnt_a[v] / n)
    plots[0].bar(args, freqs)
    plots[0].set_title("First variable")

    freqs = []
    args = []
    for v in cnt_b.keys():
        args.append(v)
        freqs.append(cnt_b[v] / n)
    plots[1].bar(args, freqs)
    plots[1].set_title("Second variable")
    plt.show()


if __name__ == '__main__':
    main()
