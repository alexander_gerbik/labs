import math

import matplotlib.pyplot as plt

import statistics
from generators import multiplicativeCongruentialGenerator, squareMiddleGenerator


def main():
    s = 3
    bar_amount = 10
    amounts, tests = init()
    print("Accurate expectation: 0.5")
    print("Accurate variance: {0}".format(1 / 12))
    for test in tests:
        gen = test["generator"]
        name = test["name"]
        print(name + ":")
        fig, plots = plt.subplots(ncols=len(amounts))
        fig.suptitle(name)
        for i, amount in enumerate(amounts):
            values = [gen.random() for i in range(amount)]
            assert (all(0 <= val and val < 1 for val in values))
            mean = statistics.get_mean_value(values)
            variance = statistics.get_variance(values, mean)
            correlation = statistics.test_independence(values, s)
            print("\tn = {0}".format(amount))
            print("\t\tExpectation: {0}, error: {1}".format(mean, math.fabs(mean - 0.5)))
            print("\t\tVariance: {0}, error: {1}".format(variance, math.fabs(variance - 1 / 12)))
            print("\t\tCorrelation: {0}".format(correlation))
            start, step, freqs = statistics.get_bar_chart(0, 1, bar_amount, values)
            args = [step / 2 + i * step for i in range(len(freqs))]
            plot = plots[i]
            plot.bar(args, freqs, width=step)
            plot.set_title("n = {0}".format(amount))

    plt.show()


def init():
    amounts = [10, 100, 1000]
    tests = [{
        "generator": multiplicativeCongruentialGenerator.Generator(1957, 2048, 1337)
        , "name": "multiplicative congruential method"
    }, {
        "generator": squareMiddleGenerator.Generator(957, 4)
        , "name": "square middle method"

    }]
    return amounts, tests


if __name__ == '__main__':
    main()
