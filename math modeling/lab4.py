import math
import random
from collections import Counter

import matplotlib.pyplot as plt

from discrete_random_variable import DiscreteRandomVariable
import statistics


def main():
    n = 100000
    alpha = 0.05
    pdf = [(2, 0.3), (7, 0.2), (-1, 0.1), (5, 0.15), (12, 0.25)]
    expected_mean = 0
    for i, p in pdf:
        expected_mean += i * p
    expected_variance = -expected_mean * expected_mean
    for i, p in pdf:
        expected_variance += i * i * p
    discrete_var = DiscreteRandomVariable(random, pdf)
    values = [discrete_var.value() for _ in range(n)]
    mean = statistics.get_mean_value(values)
    variance = statistics.get_variance(values, mean)
    lower_mean, upper_mean = statistics.get_mean_confidence_interval(len(values), alpha, mean, variance)
    lower_variance, upper_variance = statistics.get_variance_confidence_interval(len(values), alpha, variance)
    cnt = Counter(values)
    accept, criterion, critical_point = statistics.discrete_pearson_test(pdf, cnt, n, alpha)
    print("Discrete random variable:")
    print("n = {}".format(n))
    print("Probabilities: ")
    for i, p in pdf:
        print("P({})={} ".format(i, p), end="")
    print()
    for i, _ in pdf:
        print("Value {} happened: {} times({}%).".format(i, cnt[i], round(100 * cnt[i] / n, 2)))
    print("Alpha: {}.".format(alpha))
    print("Mean: {}, expected: {}, error: {}".format(mean, expected_mean, math.fabs(mean - expected_mean)))
    mean_message = "" if lower_mean <= expected_mean <= upper_mean else "not "
    print("Mean is {}within confidence interval: [{}; {}].".format(mean_message, lower_mean, upper_mean))
    print("Variance: {}, expected: {}, error: {}".format(variance, expected_variance,
                                                         math.fabs(variance - expected_variance)))
    variance_message = "" if lower_variance <= expected_variance <= upper_variance else "not "
    print(
        "Variance is {}within confidence interval: [{}; {}].".format(variance_message, lower_variance, upper_variance))
    message = "Do not reject." if accept else "Reject."
    print("Pearson criterion. Critical point: {}. Value: {}. {}".format(critical_point,
                                                                       criterion, message))
    freqs = []
    args = []
    for v in cnt.keys():
        args.append(v)
        freqs.append(cnt[v]/n)
    plt.bar(args, freqs)
    plt.show()


if __name__ == '__main__':
    main()
