from events import randomEvent


class IndependentEvents:
    def __init__(self, random_generator, probabilities):
        self.random = random_generator
        self.events = []
        for prob in probabilities:
            self.events.append(randomEvent.RandomEvent(self.random, prob))

    def occurs(self):
        return [event.occurs() for event in self.events]
