from events import randomEvent


class DependentEvents:
    def __init__(self, random_generator, probability_a, probability_b, probability_b_after_a):
        self.random = random_generator
        self.event_a = randomEvent.RandomEvent(self.random, probability_a)
        self.event_b_after_a = randomEvent.RandomEvent(self.random, probability_b_after_a)
        probability_b_after_not_a = probability_b - probability_b_after_a * probability_a
        probability_b_after_not_a /= 1 - probability_a
        self.event_b_after_not_a = randomEvent.RandomEvent(self.random, probability_b_after_not_a)

    def occurs(self):
        result_a = self.event_a.occurs()
        if result_a == 1:
            result_b = self.event_b_after_a.occurs()
        else:
            result_b = self.event_b_after_not_a.occurs()
        return result_a, result_b
