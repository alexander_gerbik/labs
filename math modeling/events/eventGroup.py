class EventGroup:
    def __init__(self, random_generator, probabilities):
        self.random = random_generator
        probs = EventGroup._normalize(probabilities)
        self.distributionFunction = EventGroup._get_distribution_function(probs)

    def occurs(self):
        rand = self.random.random()
        for i, prob in enumerate(self.distributionFunction):
            if rand < prob:
                return i
        assert False

    @staticmethod
    def _normalize(probabilities):
        s = sum(probabilities)
        return [i / s for i in probabilities]

    @staticmethod
    def _get_distribution_function(probs):
        n = len(probs)
        for i in range(1, n):
            probs[i] += probs[i - 1]
        return probs
