class RandomEvent:
    def __init__(self, random_generator, probability):
        self.random = random_generator
        self.probability = probability

    def occurs(self):
        if self.random.random() < self.probability:
            return 1
        return 0
