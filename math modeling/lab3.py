import math
import random

import matplotlib.pyplot as plt

import statistics

lambda_ = 0.5


def distribution_function(x):
    return 1 - math.exp(-lambda_ * x)


def inv_distribution_function(x):
    return - math.log(1 - x) / lambda_


def probability_density_function(x):
    return lambda_ * math.exp(-lambda_ * x)


def get_expected_mean():
    return 1 / lambda_


def get_expected_variance():
    return 1 / lambda_ / lambda_


def main():
    n = 10000
    bar_amount = 100
    alpha = 0.05
    expected_mean = get_expected_mean()
    expected_variance = get_expected_variance()
    values = [inv_distribution_function(random.random()) for _ in range(n)]
    mean = statistics.get_mean_value(values)
    variance = statistics.get_variance(values, mean)
    lower_mean, upper_mean = statistics.get_mean_confidence_interval(len(values), alpha, mean, variance)
    lower_variance, upper_variance = statistics.get_variance_confidence_interval(len(values), alpha, variance)
    values.sort()
    accept, criterion, critical_point = statistics.pearson_test(distribution_function, values, len(values), alpha)
    print("Exponential distribution:")
    print("n = {}".format(n))
    print("Alpha: {}.".format(alpha))
    print("Mean: {}, expected: {}, error: {}".format(mean, expected_mean, math.fabs(mean - expected_mean)))
    mean_message = "" if lower_mean <= expected_mean <= upper_mean else "not "
    print("Mean is {}within confidence interval: [{}; {}].".format(mean_message, lower_mean, upper_mean))
    print("Variance: {}, expected: {}, error: {}".format(variance, expected_variance,
                                                         math.fabs(variance - expected_variance)))
    variance_message = "" if lower_variance <= expected_variance <= upper_variance else "not "
    print(
        "Variance is {}within confidence interval: [{}; {}].".format(variance_message, lower_variance, upper_variance))
    message = "Do not reject." if accept else "Reject."
    print("Pearson criterion. Critical point: {}. Value: {}. {}".format(critical_point,
                                                                        criterion, message))

    a = min(values)
    b = max(values)
    resolution = 100
    fy_theoretic_xs = [a + i * (b - a) / resolution for i in range(0, resolution)]
    fy_theoretic_ys = [probability_density_function(y) for y in fy_theoretic_xs]
    filtered_values = [i for i in values if a < i < b]
    start, step, freqs = statistics.get_bar_chart(a, b, bar_amount, filtered_values)
    freqs = [i / step for i in freqs]
    args = [start + i * step for i in range(len(freqs))]
    plt.bar(args, freqs, width=step)
    plt.plot(fy_theoretic_xs, fy_theoretic_ys, color="green")
    plt.show()


if __name__ == '__main__':
    main()
