import random
import hashlib
import math


class SievePrimeGenerator(object):
    def __init__(self, minPrimeNumber, maxPrimeNumber):
        self._primesArray = eratosthenesSieve(maxPrimeNumber)
        self._leastPrimeIndex = 0
        while self._primesArray[self._leastPrimeIndex] < minPrimeNumber:
            self._leastPrimeIndex += 1
        self._current = 0

    def reset(self):
        self._current = self._leastPrimeIndex - 1

    def primes(self, n):
        assert n <= len(self._primesArray) - self._leastPrimeIndex
        self.reset()
        for i in range(n):
            self._current = random.randrange(self._current + 1, len(self._primesArray) - n + i + 1)
            yield self._primesArray[self._current]

    def sequence(self, n):
        assert (n <= len(self._primesArray) - self._leastPrimeIndex)
        self.reset()
        k = (len(self._primesArray) - self._leastPrimeIndex) // n
        for i in range(n):
            yield self._primesArray[self._leastPrimeIndex + i * k + random.randrange(k)]


def eratosthenesSieve(n):
    prime = [True] * n
    prime[0] = prime[1] = False
    iterationAmount = math.ceil(math.sqrt(n))
    for i in range(iterationAmount):
        if prime[i]:
            for j in range(i * i, n, i):
                prime[j] = False
    result = []
    for i in range(n):
        if prime[i]:
            result.append(i)
    return result

class PrimeGenerator(object):

    @staticmethod
    def primes(n):
        primes=[]
        for i in range(n):
            while True:
                q, p = getPrimeNumber()
                isAppropriate = True
                for prime in primes:
                    if gcd(prime, p) != 1:
                        isAppropriate = False
                        break
                if isAppropriate:
                    break
            primes.append(p)
        primes.sort()
        return primes


def gcd(a, b):
    if b > a:
        a, b = b, a
    while b:
        a %= b
        a, b = b, a
    return a


def getPrimeNumber():
    g = 160
    n = 6
    L = 1024
    b = 63
    while True:
        while True:
            seed = random.getrandbits(g)
            U = sha(seed) ^ sha((seed + 1) % (1 << g))
            q = U | 1 | (1 << (g - 1))
            if isPrime(q):
                break
        counter = 0
        offset = 2
        while True:
            v = [0] * (n + 1)
            for i in range(n+1):
                v[i] = sha((seed + offset + i) % (1 << g))
            w = (v[n] % (1 << b)) * (1 << (n * g))
            for i in range(n):
                w += v[i] * (1 << (i * 160))
            X = w + (1 << (L - 1))
            c = X % (2 * q)
            p = X - (c - 1)
            if p >= (1 << (L - 1)) and isPrime(p):
                break
            counter += 1
            offset += n + 1
            if counter >= 4096:
                break
        if counter < 4096:
            break
    return q, p


def isPrime(m):
    if not m & 1:
        return False
    rounds = 50
    t = m - 1
    s = 0
    while not t & 1:
        t >>= 1
        s += 1
    for i in range(rounds):
        a = random.randint(2, m - 1)
        x = pow(a, t, m)
        if x == 1 or x == m - 1:
            continue
        for j in range(s - 1):
            x = (x * x) % m
            if x == m - 1:
                break
            if (x == 1):
                return False
        if (x != m-1):
            return False
    return True


def sha(number):
    hlsha = hashlib.sha1()
    hlsha.update(intToBytes(number))
    return bytesToInt(hlsha.digest())


def intToBytes(number):
    length = math.ceil(number.bit_length() / 8)
    return number.to_bytes(length, byteorder='big')


def bytesToInt(bytes):
    return int.from_bytes(bytes, byteorder='big')


if __name__ == "__main__":
    (q, p) = getPrimeNumber()
    print("Prime numbers:")
    print("p={0}".format(p))
    print("q={0}".format(q))