import random
import primeGenerator as pg
import bisect


def main():
    generator = pg.PrimeGenerator()
    sharer = ThresholdSecretSharer(generator, 3, 5)
    print("Threshold Secret Sharing Scheme k=3, n=5")
    secret = random.getrandbits(260)
    print("Secret={0}".format(secret))
    shares = sharer.divide(secret)
    complete = [shares[0], shares[1], shares[3]]
    result = sharer.combine(complete)
    print("Combining secret using 1,2 and 4 part: result={0}".format(result))
    print("Secret is restored: {0}".format(result == secret))
    complete2 = [shares[0], shares[1], shares[2], shares[3]]
    result2 = sharer.combine(complete2)
    print("Combining secret using 1,2, 3 and 4 part: result={0}".format(result2))
    print("Secret is restored: {0}".format(result2 == secret))
    badResult = sharer.combine([shares[3], shares[4]])
    print("Combining secret using only 4 and 5 part: result={0}".format(badResult))
    print("Secret is restored: {0}".format(badResult == secret))


class ThresholdSecretSharer(object):
    def __init__(self, generator, threshold, n):
        self.n = n
        self.threshold = threshold
        self.generator = generator

    def divide(self, data):
        d = pg.PrimeGenerator.primes(self.n + 1)
        while True:
            r = d.pop(0)
            if self._check(d, r):
                break
            else:
                while True:
                    q, p = pg.getPrimeNumber()
                    isAppropriate = True
                    for prime in d:
                        if pg.gcd(prime, p) != 1:
                            isAppropriate = False
                            break
                    if isAppropriate:
                        break
                bisect.insort_left(d, p)
        data = data + r * self._getRandomAlpha(d, r)
        shares = [(r, i, data % i) for i in d]
        return shares

    def combine(self, shares):
        n = len(shares)
        x = []
        primes = []
        for (r, d, k) in shares:
            p = r
            x.append(k)
            primes.append(d)
        result = garnerAlgorithm(x, primes, n)
        result %= p
        return result

    def _getRandomAlpha(self, d, r):
        k = 1
        for i in range(self.threshold):
            k *= d[i]
        k //= r + 1
        return random.randrange(k)

    def _check(self, d, r):
        left = 1
        for i in range(self.threshold):
            left *= d[i]
        right = r
        for i in range(self.n - self.threshold + 1, self.n):
            right *= d[i]
        return left > right


def garnerAlgorithm(x, primes, n):
    assert len(x) == n and len(primes) == n
    for i in range(n):
        for j in range(i):
            x[i] = inverse(primes[j], primes[i]) * (x[i] - x[j])
            x[i] %= primes[i]
            if x[i] < 0:
                x[i] += primes[i]
    result = 0
    for i in range(n):
        y = x[i]
        for j in range(i):
            y *= primes[j]
        result += y
    return result


def inverse(element, n):
    assert element < n
    d, x, y = extendedGreatestCommonDivisor(n, element)
    if d != 1:
        # inverse element doesn't exist
        return 0
    y = (y % n + n) % n
    return y


def extendedGreatestCommonDivisor(a, b):
    assert a >= b
    S = oldT = 0
    T = oldS = 1
    while b:
        quotient = a // b
        oldS, S = S, oldS - quotient * S
        oldT, T = T, oldT - quotient * T
        a %= b
        a, b = b, a
    return a, oldS, oldT

if __name__ == "__main__":
    main()