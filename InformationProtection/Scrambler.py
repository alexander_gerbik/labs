class Scrambler:
    def __init__(self, key: int, key_length: int = 8):
        self._key = key
        self._length = key_length
        self._state = None

    def encrypt(self, plain: bytes) -> bytes:
        self._state = self._key
        return self._process(plain)

    def decrypt(self, cipher: bytes) -> bytes:
        self._state = self._key
        return self._process(cipher)

    def _process(self, data: bytes) -> bytes:
        return bytes([self._transform(i) for i in data])

    def _transform(self, byte):
        for i in range(8):
            bit = self._get_bit()
            byte ^= bit << i
        return byte

    def _get_bit(self):
        result = (self._state >> (self._length - 1)) & 1
        sub = self._state >> (self._length - 2)
        sub = (sub ^ result ^ self._state) & 1
        self._state = (self._state << 1) | sub
        assert (result == 1 or result == 0)
        return result
