package com.mzi.DES;

public class TripleDESCipher {
    private final DESCipher cipher1;
    private final DESCipher cipher2;
    private final DESCipher cipher3;

    public TripleDESCipher(long key1, long key2, long key3) {
        cipher1 = new DESCipher(key1);
        cipher2 = new DESCipher(key2);
        cipher3 = new DESCipher(key3);
    }


    public byte[] encrypt(byte[] plain) {
        return cipher3.encrypt(cipher2.encrypt(cipher1.encrypt(plain)));
    }

    public byte[] decrypt(byte[] cipher) {
        return cipher1.decrypt(cipher2.decrypt(cipher3.decrypt(cipher)));
    }

}
