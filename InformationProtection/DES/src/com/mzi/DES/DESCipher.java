package com.mzi.DES;

import com.mzi.utils.ArrayConverter;
import com.mzi.utils.Padder;


public class DESCipher {
    private final static int BLOCK_SIZE = 8;
    private final static int ROUND_AMOUNT = 16;
    private final static int MASK28BIT = 0x0FFFFFFF;
    private static final byte[] IP = {
            58, 50, 42, 34, 26, 18, 10, 2,
            60, 52, 44, 36, 28, 20, 12, 4,
            62, 54, 46, 38, 30, 22, 14, 6,
            64, 56, 48, 40, 32, 24, 16, 8,
            57, 49, 41, 33, 25, 17, 9, 1,
            59, 51, 43, 35, 27, 19, 11, 3,
            61, 53, 45, 37, 29, 21, 13, 5,
            63, 55, 47, 39, 31, 23, 15, 7
    };
    // Permuted Choice 1 table
    private static final byte[] PC1 = {
            57, 49, 41, 33, 25, 17, 9,
            1, 58, 50, 42, 34, 26, 18,
            10, 2, 59, 51, 43, 35, 27,
            19, 11, 3, 60, 52, 44, 36,
            63, 55, 47, 39, 31, 23, 15,
            7, 62, 54, 46, 38, 30, 22,
            14, 6, 61, 53, 45, 37, 29,
            21, 13, 5, 28, 20, 12, 4
    };
    // Permuted Choice 2 table
    private static final byte[] PC2 = {
            14, 17, 11, 24, 1, 5,
            3, 28, 15, 6, 21, 10,
            23, 19, 12, 4, 26, 8,
            16, 7, 27, 20, 13, 2,
            41, 52, 31, 37, 47, 55,
            30, 40, 51, 45, 33, 48,
            44, 49, 39, 56, 34, 53,
            46, 42, 50, 36, 29, 32
    };
    // Array to store the number of rotations that are to be done on each round
    private static final byte[] rotations = {
            1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1
    };
    // Expansion (aka P-box) table
    private static final byte[] E = {
            32, 1, 2, 3, 4, 5,
            4, 5, 6, 7, 8, 9,
            8, 9, 10, 11, 12, 13,
            12, 13, 14, 15, 16, 17,
            16, 17, 18, 19, 20, 21,
            20, 21, 22, 23, 24, 25,
            24, 25, 26, 27, 28, 29,
            28, 29, 30, 31, 32, 1
    };
    // S-boxes (i.e. Substitution boxes)
    private static final byte[][] S = {{
            14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
            0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
            4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
            15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13
    }, {
            15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
            3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
            0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
            13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9
    }, {
            10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
            13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
            13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
            1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12
    }, {
            7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
            13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
            10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
            3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14
    }, {
            2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
            14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
            4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
            11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3
    }, {
            12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
            10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
            9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
            4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13
    }, {
            4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
            13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
            1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
            6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12
    }, {
            13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
            1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
            7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
            2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11
    }};
    // Permutation table
    private static final byte[] P = {
            16, 7, 20, 21,
            29, 12, 28, 17,
            1, 15, 23, 26,
            5, 18, 31, 10,
            2, 8, 24, 14,
            32, 27, 3, 9,
            19, 13, 30, 6,
            22, 11, 4, 25
    };
    // Final permutation (aka Inverse permutation) table
    private static final byte[] FP = {
            40, 8, 48, 16, 56, 24, 64, 32,
            39, 7, 47, 15, 55, 23, 63, 31,
            38, 6, 46, 14, 54, 22, 62, 30,
            37, 5, 45, 13, 53, 21, 61, 29,
            36, 4, 44, 12, 52, 20, 60, 28,
            35, 3, 43, 11, 51, 19, 59, 27,
            34, 2, 42, 10, 50, 18, 58, 26,
            33, 1, 41, 9, 49, 17, 57, 25
    };
    private final long[] roundKeys;
    private final long[] decryptRoundKeys;

    public DESCipher(long key) {
        this.roundKeys = getRoundKeys(key);
        this.decryptRoundKeys = roundKeys.clone();
        reverse(decryptRoundKeys);
    }

    private void reverse(long[] array) {
        int length = array.length;
        for (int i = 0; i < length / 2; ++i) {
            long temp = array[i];
            array[i] = array[length - i - 1];
            array[length - i - 1] = temp;
        }
    }

    private long[] getRoundKeys(long key) {
        final int HALF_SIZE = 28;
        long roundKeys[] = new long[ROUND_AMOUNT];
        key = permute(PC1, PC1.length, key);
        int c = (int) (key >> HALF_SIZE);
        int d = (int) (key & MASK28BIT);
        for (int i = 0; i < ROUND_AMOUNT; ++i) {
            int rotation = rotations[i];
            c = ((c << rotation) & MASK28BIT) | (c >> (HALF_SIZE - rotation));
            d = ((d << rotation) & MASK28BIT) | (d >> (HALF_SIZE - rotation));
            long cd = ((long) c & MASK28BIT) << HALF_SIZE | (d & MASK28BIT);
            roundKeys[i] = permute(PC2, PC2.length, cd);
        }
        return roundKeys;
    }

    private static long permute(byte[] table, int srcWidth, long src) {
        long dst = 0;
        for (int i = 0; i < table.length; i++) {
            int srcPos = srcWidth - table[i];
            dst = (dst << 1) | (src >> srcPos & 0x01);
        }
        return dst;
    }

    public byte[] encrypt(byte[] plain) {
        byte[] padded = Padder.pad(plain, BLOCK_SIZE);
        long[] blocks = ArrayConverter.bytesToLongs(padded);
        for (int i = 0; i < blocks.length; i++) {
            blocks[i] = encryptBlock(blocks[i], roundKeys);
        }
        return ArrayConverter.longsToBytes(blocks);
    }

    private long encryptBlock(long block, long[] keys) {
        block = permute(IP, IP.length, block);
        int l = (int) (block >> Integer.SIZE);
        int r = (int) (block);
        for (int i = 0; i < ROUND_AMOUNT; ++i) {
            int previous = l;
            l = r;
            r = previous ^ feistel(r, keys[i]);
        }
        block = ((long) l) & 0xFFFF_FFFFL;
        block |= ((long) r) << Integer.SIZE;
        block = permute(FP, FP.length, block);
        return block;
    }

    private int feistel(int r, long roundKey) {
        long e = permute(E, Integer.SIZE, r);
        long x = e ^ roundKey;
        int dst = 0;
        for (int i = 0; i < 8; i++) {
            dst >>>= 4;
            byte index = (byte) ((x & 0b100000) | ((x & 1) << 4) | (x & 0b11110) >> 1);
            int s = S[7 - i][index];
            dst |= s << 28;
            x >>= 6;
        }
        return (int) permute(P, Integer.SIZE, dst);
    }

    public byte[] decrypt(byte[] cipher) {
        long[] blocks = ArrayConverter.bytesToLongs(cipher);
        for (int i = 0; i < blocks.length; i++) {
            blocks[i] = encryptBlock(blocks[i], decryptRoundKeys);
        }
        byte[] padded = ArrayConverter.longsToBytes(blocks);
        return Padder.unpad(padded);
    }
}
