package com.mzi;


import com.mzi.RSA.PrivateKey;
import com.mzi.RSA.PublicKey;
import com.mzi.RSA.RSACipher;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Arrays;

public class lab4 {

    public static void main(String[] args) throws IOException {
        String inputFileName = "input";
        String outputFileName = "output";
        Path inputFile = Paths.get(inputFileName);
        Path outputFile = Paths.get(outputFileName);
        byte[] input = Files.readAllBytes(inputFile);
        SecureRandom random = new SecureRandom();
        Object[] keys = RSACipher.generateKeys(random);
        PublicKey publicKey = (PublicKey) keys[0];
        PrivateKey privateKey = (PrivateKey) keys[1];
        byte[] encrypted = RSACipher.encrypt(publicKey, input);
        Files.write(outputFile, encrypted);
        byte[] decrypted = RSACipher.decrypt(privateKey, encrypted);
        System.out.println("RSA:");
        System.out.println(Arrays.equals(decrypted, input) ? "success" : "fail");
        System.out.println("Input:");
        for (byte b : input) {
            System.out.print(b);
            System.out.print(" ");
        }
        System.out.println();
        System.out.println("Decrypted:");
        for (byte b : decrypted) {
            System.out.print(b);
            System.out.print(" ");
        }
        System.out.println();
    }
}
