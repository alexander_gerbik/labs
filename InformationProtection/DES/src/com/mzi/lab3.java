package com.mzi;


import com.mzi.AES.AESCipher;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class lab3 {

    public static void main(String[] args) throws IOException {
        byte[] key = {12, 13, 14, 15, 16, 12, 13, 14, 15, 16, 12, 13, 14, 15, 16, 17};
        String inputFileName = "input";
        String outputFileName = "output";
        Path inputFile = Paths.get(inputFileName);
        Path outputFile = Paths.get(outputFileName);
        byte[] input = Files.readAllBytes(inputFile);
        AESCipher cipher = new AESCipher(key);
        byte[] encrypted = cipher.encrypt(input);
        Files.write(outputFile, encrypted);
        byte[] decrypted = cipher.decrypt(encrypted);
        System.out.println("AES:");
        System.out.println(Arrays.equals(decrypted, input) ? "success" : "fail");
        System.out.println("Input:");
        for (byte b : input) {
            System.out.print(b);
            System.out.print(" ");
        }
        System.out.println();
        System.out.println("Decrypted:");
        for (byte b : decrypted) {
            System.out.print(b);
            System.out.print(" ");
        }
        System.out.println();
    }
}
