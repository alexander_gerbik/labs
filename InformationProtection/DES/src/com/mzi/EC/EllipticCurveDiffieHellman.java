package com.mzi.EC;

import com.sun.istack.internal.NotNull;

import java.math.BigInteger;
import java.util.Random;

public class EllipticCurveDiffieHellman {
    final static BigInteger p = new BigInteger("6277101735386680763835789423207666416083908700390324961279",
            10);
    private final static BigInteger n = new BigInteger("6277101735386680763835789423176059013767194773182842284081",
            10);
    final static BigInteger a = new BigInteger("-3", 10);
    final static BigInteger b = new BigInteger("64210519e59c80e70fa7e9ab72243049feb8deecc146b9b1", 16);
    final static Point G = new Point(new BigInteger("188da80eb03090f67cbf20eb43a18800f4ff0afd82ff1012", 16),
                                     new BigInteger("07192b95ffc8da78631011ed6b24cdd573f977a11e794811", 16));
    final static int MODULUS_BYTES = n.bitLength() / Byte.SIZE;
    final static int MAX_POWER_BYTES = MODULUS_BYTES - 1;
    final static int PRIVATE_KEY_BYTES = MAX_POWER_BYTES / 2;
    final static BigInteger THREE = new BigInteger("3", 10);
    final static BigInteger TWO = new BigInteger("2", 10);

    private BigInteger privateKey;
    private byte[] sessionKey;

    public EllipticCurveDiffieHellman(Random random) {
        byte[] tmp = new byte[PRIVATE_KEY_BYTES];
        random.nextBytes(tmp);
        setPrivateKey(tmp);
    }

    public EllipticCurveDiffieHellman(byte[] privateKey) {
        setPrivateKey(privateKey);
    }

    public byte[] getPrivateKey() {
        byte[] result = new byte[PRIVATE_KEY_BYTES];
        byte[] tmp = privateKey.toByteArray();
        System.arraycopy(tmp, tmp.length - result.length, result, 0, result.length);
        return result;
    }

    public void setPrivateKey(byte[] key) {
        privateKey = new BigInteger(1, key);
    }

    public byte[] getPublicKey() {
        Point pk = G.multiply(privateKey);
        return pk.toByteArray();
    }

    public byte[] setUpSessionKey(byte[] foreignPublicKey) {
        Point fpk = new Point(foreignPublicKey);
        Point commonKey = fpk.multiply(privateKey);
        sessionKey = commonKey.toByteArray();
        return sessionKey;
    }

    public byte[] getSessionKey(byte[] foreignPublicKey) {
        if (sessionKey == null)
            throw new IllegalStateException("You should call setUpSessionKey method before.");
        return sessionKey;
    }

    private static class Point {
        public static final Point ZERO;

        static {
            ZERO = new Point(BigInteger.ZERO, BigInteger.ZERO);
        }

        private BigInteger x;
        private BigInteger y;

        public Point(@NotNull BigInteger x, @NotNull BigInteger y) {
            this.x = x;
            this.y = y;
        }

        public Point(Point other) {
            this.x = other.x;
            this.y = other.y;
        }

        public Point(byte[] bytes) {
            byte[] xa = new byte[MODULUS_BYTES];
            byte[] ya = new byte[MODULUS_BYTES];
            System.arraycopy(bytes, 0, xa, 0, MODULUS_BYTES);
            System.arraycopy(bytes, MODULUS_BYTES, ya, 0, MODULUS_BYTES);
            this.x = new BigInteger(1, xa);
            this.y = new BigInteger(1, ya);
        }

        public byte[] toByteArray() {
            byte[] result = new byte[2 * MODULUS_BYTES];
            byte[] xa = this.x.toByteArray();
            byte[] ya = this.y.toByteArray();
            int xaLength = MODULUS_BYTES < xa.length ? MODULUS_BYTES : xa.length;
            int yaLength = MODULUS_BYTES < ya.length ? MODULUS_BYTES : ya.length;
            System.arraycopy(xa, xa.length - xaLength, result, MODULUS_BYTES - xaLength, xaLength);
            System.arraycopy(ya, ya.length - yaLength, result, 2 * MODULUS_BYTES - yaLength, yaLength);
            return result;
        }

        public boolean isZero() {
            return this == ZERO;
        }

        public Point add(Point other) {
            if (this.isZero()) return other;
            if (other.isZero()) return this;
            if (this.equals(other))
                return square();
            BigInteger rx, ry;
            BigInteger dx = other.x.subtract(this.x).mod(p);
            BigInteger dy = other.y.subtract(this.y).mod(p);
            if (dx.equals(BigInteger.ZERO)) return ZERO;
            BigInteger k = dy.multiply(dx.modInverse(p)).mod(p);
            rx = k.multiply(k).subtract(this.x).subtract(other.x).mod(p);
            ry = this.x.subtract(rx).multiply(k).subtract(this.y).mod(p);
            return new Point(rx, ry);
        }

        public Point square() {
            if (this.isZero()) return this;
            BigInteger rx, ry;
            BigInteger dy = THREE.multiply(this.x).multiply(this.x).add(a).mod(p);
            BigInteger dx = TWO.multiply(this.y).mod(p);
            if (dx.equals(BigInteger.ZERO)) return ZERO;
            BigInteger k = dy.multiply(dx.modInverse(p)).mod(p);
            rx = k.multiply(k).subtract(this.x).subtract(this.x).mod(p);
            ry = this.x.subtract(rx).multiply(k).subtract(this.y).mod(p);
            return new Point(rx, ry);
        }

        public Point multiply(BigInteger n) {
            Point result = ZERO;
            Point a = this;
            for (int i = 0; i < n.bitLength(); ++i) {
                if (n.testBit(i)) {
                    result = result.add(a);
                }
                a = a.square();
            }
            return result;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Point)) return false;
            Point point = (Point) o;
            if (this.isZero() ^ point.isZero()) return false;
            //noinspection SuspiciousNameCombination
            return x.equals(point.x) && y.equals(point.y);
        }

        @Override
        public int hashCode() {
            int result = x.hashCode();
            result = 31 * result + y.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return "Point{x=" + x + ", y=" + y + '}';
        }
    }
}
