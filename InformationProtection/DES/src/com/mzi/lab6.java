package com.mzi;

import com.mzi.RSA.PrivateKey;
import com.mzi.RSA.PublicKey;
import com.mzi.RSA.RSACipher;
import com.mzi.RSA.Signature;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

import static com.mzi.lab5.toHexString;

public class lab6 {
    public static void main(String[] args) {
        String message = "The quick brown fox jumps over the lazy dog";
        byte[] m = message.getBytes(StandardCharsets.US_ASCII);
        SecureRandom random = new SecureRandom();
        Object[] keys = RSACipher.generateKeys(random);
        PublicKey publicKey = (PublicKey) keys[0];
        PrivateKey privateKey = (PrivateKey) keys[1];
        byte[] signature = Signature.sign(privateKey, m);
        boolean result = Signature.verify(publicKey, m, signature);
        System.out.println("Message verification successful:");
        System.out.println(result);
        m[0] ^= 0xff;
        result = Signature.verify(publicKey, m, signature);
        System.out.println("Changed message verification successful:");
        System.out.println(result);
        System.out.println("Signature:");
        System.out.println(toHexString(signature));
    }
}
