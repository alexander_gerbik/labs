package com.mzi;


import com.mzi.DES.TripleDESCipher;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class lab2 {

    public static void main(String[] args) throws IOException {
        long key1 = 0xAABB09CD2736CCDDL;
        long key2 = 0xACAB09182736CCDDL;
        long key3 = 0xAABB09182736CCDDL;
        String inputFileName = "input";
        String outputFileName = "output";
        Path inputFile = Paths.get(inputFileName);
        Path outputFile = Paths.get(outputFileName);
        byte[] input = Files.readAllBytes(inputFile);
        TripleDESCipher cipher = new TripleDESCipher(key1, key2, key3);
        byte[] encrypted = cipher.encrypt(input);
        Files.write(outputFile, encrypted);
        byte[] decrypted = cipher.decrypt(encrypted);
        System.out.println(Arrays.equals(decrypted, input) ? "success" : "fail");
        System.out.println("DES:");
        System.out.println("Input:");
        for (byte b : input) {
            System.out.print(b);
            System.out.print(" ");
        }
        System.out.println();
        System.out.println("Decrypted:");
        for (byte b : decrypted) {
            System.out.print(b);
            System.out.print(" ");
        }
        System.out.println();
    }
}
