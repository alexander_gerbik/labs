package com.mzi.sha2;

import com.mzi.utils.ArrayConverter;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

public class SHA256 {
    private static final int HASH_INTS = 256 / Integer.SIZE;
    private static final int BLOCK_BYTES = 512 / Byte.SIZE;
    private static final int W_SIZE = 512 / Byte.SIZE;
    private static final int[] INITIAL_H = {0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c,
                                            0x1f83d9ab, 0x5be0cd19};
    private static final int[] K = {
            0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
            0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
            0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
            0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
            0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
            0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
            0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
            0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
            0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
            0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
            0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
            0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
            0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
            0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
            0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
            0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
    };

    public static byte[] digest(byte[] message) {
        message = pad(message);
        IntBuffer buffer = ByteBuffer.wrap(message).asIntBuffer();
        int[] H = initializeH();
        int[] W = new int[W_SIZE];
        for (int i = 0; i < message.length / BLOCK_BYTES; i++) {
            getW(buffer, W);
            compress(H, W);
        }
        return ArrayConverter.intsToBytes(H);
    }

    private static byte[] pad(byte[] message) {
        int resLength = ((message.length + Long.BYTES) / BLOCK_BYTES + 1) * BLOCK_BYTES;
        byte[] res = new byte[resLength];
        System.arraycopy(message, 0, res, 0, message.length);
        res[message.length] = (byte) 0x80;
        long length = message.length;
        length = length * Byte.SIZE;
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(length);
        byte[] addend = buffer.array();
        System.arraycopy(addend, 0, res, resLength - addend.length, addend.length);
        return res;
    }

    private static int[] initializeH() {
        int[] H = new int[HASH_INTS];
        System.arraycopy(INITIAL_H, 0, H, 0, HASH_INTS);
        return H;
    }

    private static void getW(IntBuffer buffer, int[] w) {
        int i = 0;
        for (; i < BLOCK_BYTES / Integer.BYTES; i++) {
            w[i] = buffer.get();
        }
        while (i < W_SIZE) {
            int s0 = rotateRight(w[i - 15], 7) ^ rotateRight(w[i - 15], 18) ^ (w[i - 15] >>> 3);
            int s1 = rotateRight(w[i - 2], 17) ^ rotateRight(w[i - 2], 19) ^ (w[i - 2] >>> 10);
            w[i] = w[i - 16] + s0 + w[i - 7] + s1;
            i++;
        }
    }

    private static void compress(int[] H, int[] w) {
        int a = H[0],
                b = H[1],
                c = H[2],
                d = H[3],
                e = H[4],
                f = H[5],
                g = H[6],
                h = H[7];
        for (int i = 0; i < W_SIZE; i++) {
            int sum0 = rotateRight(a, 2) ^ rotateRight(a, 13) ^ rotateRight(a, 22);
            int mA = (a & b) ^ (a & c) ^ (b & c);
            int t2 = sum0 + mA;
            int sum1 = rotateRight(e, 6) ^ rotateRight(e, 11) ^ rotateRight(e, 25);
            int Ch = (e & f) ^ ((~e) & g);
            int t1 = h + sum1 + Ch + K[i] + w[i];
            h = g;
            g = f;
            f = e;
            e = d + t1;
            d = c;
            c = b;
            b = a;
            a = t1 + t2;
        }
        H[0] += a;
        H[1] += b;
        H[2] += c;
        H[3] += d;
        H[4] += e;
        H[5] += f;
        H[6] += g;
        H[7] += h;
    }

    private static int rotateRight(int x, int offset) {
        offset = offset % Integer.SIZE;
        if (offset < 0) {
            offset += Integer.SIZE;
        }
        return (x >>> offset) | (x << (Integer.SIZE - offset));
    }
}
