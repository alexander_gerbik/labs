package com.mzi;

import com.mzi.sha2.SHA256;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class lab5 {
    public static void main(String[] args) throws NoSuchAlgorithmException {
        String message = "The quick brown fox jumps over the lazy dog";
        byte[] m = message.getBytes(StandardCharsets.US_ASCII);
        byte[] hash = SHA256.digest(m);
        byte[] expectedHash = MessageDigest.getInstance("SHA-256").digest(m);
        System.out.println(Arrays.equals(hash, expectedHash) ? "success" : "fail");
        System.out.println(toHexString(hash));
    }

    public static String toHexString(byte[] bytes) {
        char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for ( int j = 0; j < bytes.length; j++ ) {
            v = bytes[j] & 0xFF;
            hexChars[j*2] = hexArray[v/16];
            hexChars[j*2 + 1] = hexArray[v%16];
        }
        return new String(hexChars);
    }
}
