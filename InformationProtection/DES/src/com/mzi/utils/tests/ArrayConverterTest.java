package com.mzi.utils.tests;

import com.mzi.utils.ArrayConverter;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArrayConverterTest {
    @Test
    public void convertLongs() throws Exception {
        long[] source = {12, 13, 24};
        long[] res = ArrayConverter.bytesToLongs(ArrayConverter.longsToBytes(source));
        assertArrayEquals(source, res);
    }

    @Test
    public void convertBytes() throws Exception {
        byte[] source = {12, 13, 24, -1, 127, 3, 2, 1};
        byte[] res = ArrayConverter.longsToBytes(ArrayConverter.bytesToLongs(source));
        assertArrayEquals(source, res);
    }

}