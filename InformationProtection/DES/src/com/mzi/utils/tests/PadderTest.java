package com.mzi.utils.tests;

import com.mzi.utils.Padder;

import static org.junit.Assert.*;

public class PadderTest {
    @org.junit.Test
    public void pad15() throws Exception {
        byte[] source = new byte[15];
        byte[] res = Padder.pad(source, 8);
        assertArrayEquals(source, Padder.unpad(res));
    }

    @org.junit.Test
    public void pad32() throws Exception {
        byte[] source = new byte[32];
        byte[] res = Padder.pad(source, 8);
        assertArrayEquals(source, Padder.unpad(res));
    }
    @org.junit.Test
    public void pad37() throws Exception {
        byte[] source = new byte[37];
        byte[] res = Padder.pad(source, 8);
        assertArrayEquals(source, Padder.unpad(res));
    }

}