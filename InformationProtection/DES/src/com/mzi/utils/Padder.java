package com.mzi.utils;


import java.util.Arrays;

public class Padder {

    public static byte[] pad(byte[] b, int blockSize) {
        int padding = blockSize - b.length % blockSize;
        byte[] result = new byte[b.length + padding];
        System.arraycopy(b, 0, result, 0, b.length);
        Arrays.fill(result, b.length, b.length + padding, (byte) padding);
        return result;
    }

    public static byte[] unpad(byte[] b) {
        int length = b.length - b[b.length - 1];
        byte[] result = new byte[length];
        System.arraycopy(b, 0, result, 0, length);
        return result;
    }
}
