package com.mzi.utils;

import java.nio.ByteBuffer;

public class ArrayConverter {
    public static long[] bytesToLongs(byte[] source) {
        int length = source.length * Byte.SIZE / Long.SIZE;
        long[] res = new long[length];
        ByteBuffer.wrap(source).asLongBuffer().get(res);
        return res;

    }

    public static byte[] longsToBytes(long[] source) {
        int length = Long.SIZE * source.length / Byte.SIZE;
        ByteBuffer buffer = ByteBuffer.allocate(length);
        buffer.asLongBuffer().put(source);
        return buffer.array();
    }

    public static byte[] intsToBytes(int[] h) {
        int length = Integer.BYTES * h.length;
        ByteBuffer buffer = ByteBuffer.allocate(length);
        buffer.asIntBuffer().put(h);
        return buffer.array();
    }
}
