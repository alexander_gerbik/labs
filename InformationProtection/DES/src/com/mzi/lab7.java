package com.mzi;


import com.mzi.AES.AESCipher;
import com.mzi.EC.EllipticCurveDiffieHellman;
import com.sun.istack.internal.NotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Arrays;

import static com.mzi.lab5.toHexString;

public class lab7 {

    public static void main(String[] args) throws IOException {
        SecureRandom random = new SecureRandom();
        String inputFileName = "input";
        String outputFileName = "output";
        Path inputFile = Paths.get(inputFileName);
        Path outputFile = Paths.get(outputFileName);
        byte[] message = Files.readAllBytes(inputFile);

        EllipticCurveDiffieHellman alice = new EllipticCurveDiffieHellman(random);
        EllipticCurveDiffieHellman bob = new EllipticCurveDiffieHellman(random);
        byte[] alicePublicKey = alice.getPublicKey();
        byte[] bobPublicKey = bob.getPublicKey();
        byte[] aliceSessionKey = alice.setUpSessionKey(bobPublicKey);
        byte[] bobSessionKey = bob.setUpSessionKey(alicePublicKey);

        byte[] aliceAESKey = toAESKey(aliceSessionKey);
        AESCipher aliceCipher = new AESCipher(aliceAESKey);
        byte[] encrypted = aliceCipher.encrypt(message);
        Files.write(outputFile, encrypted);

        byte[] bobAESKey = toAESKey(bobSessionKey);
        AESCipher bobCipher = new AESCipher(bobAESKey);
        byte[] decrypted = bobCipher.decrypt(encrypted);

        System.out.println("Elliptic Curve Diffie-Hellman:");
        System.out.println("Session key:");
        System.out.println(toHexString(aliceSessionKey));
        System.out.println("Session keys equal:");
        System.out.println(Arrays.equals(aliceSessionKey, bobSessionKey));
        System.out.println("Decryption successful:");
        System.out.println(Arrays.equals(decrypted, message));
    }

    private static byte[] toAESKey(@NotNull byte[] data) {
        int aesKeyLength = AESCipher.KEY_BYTES;
        byte[] result = new byte[aesKeyLength];
        int length = aesKeyLength < data.length ? aesKeyLength : data.length;
        System.arraycopy(data, 0, result, 0, length);
        return result;
    }
}
