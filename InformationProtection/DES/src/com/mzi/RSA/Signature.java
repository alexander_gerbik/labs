package com.mzi.RSA;

import com.mzi.sha2.SHA256;

import java.util.Arrays;

public class Signature {
    public static byte[] sign(PrivateKey privateKey, byte[] m) {
        byte[] hash = SHA256.digest(m);
        PublicKey key = new PublicKey(privateKey.getD(), privateKey.getN());
        return RSACipher.encrypt(key, hash);
    }

    public static boolean verify(PublicKey publicKey, byte[] message, byte[] signature) {
        PrivateKey key = new PrivateKey(publicKey.getE(), publicKey.getN());
        byte[] messageHash = SHA256.digest(message);
        byte[] hash = RSACipher.decrypt(key, signature);
        return Arrays.equals(messageHash, hash);
    }
}
