package com.mzi.RSA;

import com.mzi.utils.Padder;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Random;

public class RSACipher {
    private static final int KEY_SIZE = 1024;
    private static final int MESSAGE_BLOCK_SIZE = (KEY_SIZE / Byte.SIZE) - 1;
    private static final int CIPHERED_BLOCK_SIZE = (KEY_SIZE / Byte.SIZE);

    /**
     * Generate a public-private key pair for RSA cipher.
     *
     * @return {@code []{publicKey, privateKey}}.
     */
    public static Object[] generateKeys(Random random) {
        BigInteger p, q, n, e, phi, d;
        do {
            do {
                p = BigInteger.probablePrime(KEY_SIZE / 2, random);
                q = BigInteger.probablePrime(KEY_SIZE / 2, random);
                n = p.multiply(q);
            } while (n.bitLength() < KEY_SIZE);
            phi = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
            e = new BigInteger("65537");
        } while (!e.gcd(phi).equals(BigInteger.ONE));
        d = e.modInverse(phi);
        PrivateKey privateKey = new PrivateKey(d, n);
        PublicKey publicKey = new PublicKey(e, n);
        return new Object[]{publicKey, privateKey};
    }

    public static byte[] encrypt(PublicKey publicKey, byte[] message) {
        message = Padder.pad(message, MESSAGE_BLOCK_SIZE);
        return process(publicKey.getE(), publicKey.getN(), message, MESSAGE_BLOCK_SIZE, CIPHERED_BLOCK_SIZE);
    }

    public static byte[] decrypt(PrivateKey privateKey, byte[] cipherText) {
        cipherText = cipherText.clone();
        cipherText = process(privateKey.getD(), privateKey.getN(), cipherText, CIPHERED_BLOCK_SIZE, MESSAGE_BLOCK_SIZE);
        cipherText = Padder.unpad(cipherText);
        return cipherText;
    }

    private static byte[] process(BigInteger e, BigInteger n, byte[] message, int inputBlockSize, int outputBLockSize) {
        byte[] inputBlock = new byte[inputBlockSize];
        byte[] outputBlock = new byte[outputBLockSize];
        byte[] result = new byte[message.length / inputBlockSize * outputBLockSize];
        int j = 0;
        for (int i = 0; i < message.length; i += inputBlockSize) {
            System.arraycopy(message, i, inputBlock, 0, inputBlockSize);
            BigInteger m = new BigInteger(1, inputBlock);
            BigInteger c = m.modPow(e, n);
            byte[] tmp = c.toByteArray();
            Arrays.fill(outputBlock, (byte) 0);
            int length = tmp.length > outputBLockSize ? outputBLockSize : tmp.length;
            System.arraycopy(tmp, tmp.length - length, outputBlock, outputBLockSize - length, length);
            System.arraycopy(outputBlock, 0, result, j, outputBLockSize);
            j += outputBLockSize;
        }
        return result;
    }
}
