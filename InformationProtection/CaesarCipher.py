class CaesarCipher(object):
    def __init__(self, char_amount: int, encoding: str, key: int):
        self.char_amount = char_amount
        self.key = key
        self.encoding = encoding

    def encrypt(self, plain: str) -> str:
        return self._shift(plain, self.key)

    def decrypt(self, cipher: str) -> str:
        return self._shift(cipher, -self.key)

    def _shift(self, string: str, key: int) -> str:
        x = string.encode(encoding=self.encoding, errors='ignore')
        x = bytes([(i + key) % self.char_amount for i in x])
        return x.decode(encoding=self.encoding, errors='ignore')
