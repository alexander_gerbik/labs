class KeyBreaker:
    def __init__(self, char_amount: int, encoding: str):
        self.char_amount = char_amount
        self.encoding = encoding
        self.ru_frequencies = [0.0866, 0.0151, 0.04190000000000001, 0.0141, 0.0256, 0.081, 0.0078000000000000005,
                               0.0181,
                               0.0745, 0.0131, 0.0347, 0.0432, 0.0329, 0.0635, 0.0928, 0.0335, 0.0553, 0.0545, 0.063,
                               0.028999999999999998, 0.004, 0.0092, 0.0052, 0.0127, 0.0077, 0.0049, 0.0004,
                               0.021099999999999997, 0.019, 0.0017000000000000001, 0.0103, 0.0222]
        self.eng_frequencies = [0.08166999999999999, 0.01492, 0.02782, 0.04253, 0.12702, 0.02228, 0.02015, 0.06094,
                                0.06966,
                                0.00153,
                                0.00772, 0.04025, 0.02406, 0.06749, 0.07507, 0.01929, 0.00095, 0.05987,
                                0.06326999999999999,
                                0.09055999999999999, 0.02758, 0.00978, 0.0236, 0.0015, 0.01974, 0.00074]

    def guess(self, cipher: str) -> int:
        res = 0
        min_error = float('inf')
        frequencies = self._all_frequencies(cipher)
        for key in range(self.char_amount):
            ru_frequencies = self._decrypted_frequencies(frequencies, key, 'а', 'я')
            eng_frequencies = self._decrypted_frequencies(frequencies, key, 'a', 'z')
            ru_error = self._error(ru_frequencies, self.ru_frequencies)
            eng_error = self._error(eng_frequencies, self.eng_frequencies)
            error = min(ru_error, eng_error)
            if error < min_error:
                min_error = error
                res = key
        return res

    def _all_frequencies(self, message: str):
        length = self.char_amount
        frequencies = [0 for _ in range(length)]
        for c in message.encode(encoding=self.encoding, errors='ignore'):
            frequencies[c] += 1
        message_length = len(message)
        for i in range(len(frequencies)):
            frequencies[i] /= message_length
        return frequencies

    def _decrypted_frequencies(self, frequencies, key, frm, to):
        l = frm.encode(encoding=self.encoding, errors='ignore')[0]
        u = to.encode(encoding=self.encoding, errors='ignore')[0]
        length = u - l + 1
        return [frequencies[(i + l + key) % self.char_amount] for i in range(length)]

    @staticmethod
    def _error(a, b):
        assert (len(a) == len(b))
        error = 0
        for i in range(len(a)):
            error += (a[i] - b[i]) ** 2
        error /= len(a)
        return error
