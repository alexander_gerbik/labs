import random

from CaesarCipher import CaesarCipher
from KeyBreaker import KeyBreaker


def main():
    encoding = 'cp1251'
    key = random.randrange(26)
    char_amount = 256
    with open("in.txt", mode='r', encoding=encoding) as infile:
        plain = infile.read()
    caesar = CaesarCipher(char_amount, encoding, key)
    cipher = caesar.encrypt(plain)
    with open("out.txt", mode='w', encoding=encoding) as outfile:
        outfile.write(cipher)
    broken_key = KeyBreaker(char_amount, encoding).guess(cipher)
    print("key was {}. Guessed key: {}".format(key, broken_key))


if __name__ == '__main__':
    main()
