from Scrambler import Scrambler


def main():
    key = 137
    with open("in.txt", mode='rb') as infile:
        plain = infile.read()
    scrambler = Scrambler(key)
    cipher = scrambler.encrypt(plain)
    with open("out.txt", mode='wb') as outfile:
        outfile.write(cipher)
    encrypted = scrambler.decrypt(cipher)
    print("Decryption successful: {}".format(encrypted == plain))


if __name__ == '__main__':
    main()
