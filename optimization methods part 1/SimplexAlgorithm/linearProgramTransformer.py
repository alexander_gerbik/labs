import numpy as np
def transform(findMax, A, sourceC, E):
    C = np.copy(sourceC)
    varsAmount = A.shape[1]
    equationAmount = A.shape[0]
    if not findMax:
        for i in C:
            i *= -1
    for i, el in enumerate(E):
        if el == "==":
            continue
        aColumn = [0]*equationAmount
        if el == ">=":
            aColumn[i] = -1
        elif el == "<=":
            aColumn[i] = 1
        else:
            raise SyntaxError()
        C = np.c_[C, [0]]
        A = np.c_[A, aColumn]
    return varsAmount, A, C