import numpy as np

import inverseMatrix


class feasibleRegionIsEmptyError(Exception):
    def __init__(self):
        pass


def solve(C, A, b, Jb, Jnb):
    Ab = np.matrix([[A[j, i] for i in Jb] for j in range(A.shape[0])])
    B = inverseMatrix.getInverseMatrix(Ab)
    Anb = np.matrix([[A[j, i] for i in Jnb] for j in range(A.shape[0])])
    Cb = np.matrix([[C[j, i] for i in Jb] for j in range(C.shape[0])])
    Cnb = np.matrix([[C[j, i] for i in Jnb] for j in range(C.shape[0])])
    deltas = Cb * B * Anb - Cnb
    return cycle(deltas, B, Jb, Jnb, A, b)


def cycle(deltas, B, Jb, Jnb, A, b):
    n = A.shape[1]
    while True:
        for i in range(deltas.shape[1]):
            if deltas[0, i] < 0:
                raise feasibleRegionIsEmptyError()
        x = B * b
        k = findNegative(x)
        if k == -1:
            result = [0.0] * n
            for i, j in enumerate(Jb):
                result[j] = x[i, 0]
            return result
        nu = []
        for i in Jnb:
            nu.append((B[k, :] * A[:, i])[0, 0])
        negativeIndexes = [i for i in range(len(nu)) if nu[i] < 0]
        if len(negativeIndexes) == 0:
            raise feasibleRegionIsEmptyError()
        s, sigma = findSigma(nu, negativeIndexes, deltas)
        for i in range(len(Jnb)):
            deltas[0, i] += sigma * nu[i]
        deltas[0, s] = sigma
        B = inverseMatrix.getNewInverseMatrix(B, k, A[:, Jnb[s]])
        Jb[k], Jnb[s] = Jnb[s], Jb[k]


def findSigma(nu, negativeIndexes, deltas):
    assert len(negativeIndexes) > 0
    s = negativeIndexes[0]
    minElement = - deltas[0, s] / nu[s]
    for i in range(1, len(negativeIndexes)):
        current = - deltas[0, negativeIndexes[i]] / nu[negativeIndexes[i]]
        if current < minElement or (current == minElement and negativeIndexes[i] < s):
            minElement = current
            s = negativeIndexes[i]
    return s, minElement


def findNegative(x):
    for i in range(x.shape[0]):
        if x[i, 0] < 0:
            return i
    return -1