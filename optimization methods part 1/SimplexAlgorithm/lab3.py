import numpy as np

import dualSimplexAlgorithm as dsa
import linearProgramTransformer as lpt

def main():
    findMax = False
    C = np.matrix([[3, -2, 0, -5, 1]])
    E = ['<=', '<=', '<=', '>=']
    A = np.matrix([[2, 0, 1, 1, 1], [1, 0, -1, 2, 1], [0, 0, 1, -1, 2], [1, -1, 0, 1, -5]])
    b = np.matrix([[2], [3], [6], [8]])
    Jb, Jnb = [0, 1, 2, 4], [3]

    findMax = False
    C = np.matrix([[-3, -20, 1, 0, 1, -1, 1, 0]])
    E = ['>=', '>=', '>=']
    A = np.matrix([[1, -2, 1, -1, 0, -3, 0, 2], [-1, -3, 0, 1, 1, 2, 0, 3], [0, -4, 0, 1, 0, 1, 1, -4]])
    b = np.matrix([[-2], [3], [-5]])
    #A = np.matrix([[1, 2, 1, -1, 0, 3, 0, 2], [-1, -3, 0, 1, 1, 2, 0, 3], [0, 4, 0, 1, 0, 1, 1, 4]])
    #b = np.matrix([[-2], [3], [-5]])
    Jb, Jnb = [1, 5, 7], [0, 2, 3, 4, 6]
    n1, a1, c1 = lpt.transform(findMax, A, C, E)
    try:
        result = dsa.solve(c1, a1, b, Jb, Jnb)
    except dsa.feasibleRegionIsEmptyError:
        print("There is no solutions")
        return
    for i in range(n1, len(result)):
        result.pop(n1)
    print("Optimal solution:", result)
    f = 0
    for i in range(len(result)):
        f += result[i] * C[0, i]
    print("Objective function:", f)

if __name__ == "__main__":
    main()