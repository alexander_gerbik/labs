import numpy as np
import linearProgramTransformer as lpt
import simplexAlgorithm as sa
import inverseMatrix

def main():
    C = np.matrix([[3, -7, 5, 11, 15, 64]])
    #C = -1*C
    print(C)
    A = np.matrix([[1, 0, 0, -3, 4, 6], [1, 1, 0, 4, 2, 6], [0, 0, 1, 6, -5, 6]])
    b = np.matrix([[12], [12], [-46]])
    lower = np.matrix([[-1, 0, 4, 2, -2, -3]])
    upper = np.matrix([[5, 7, 8, 8, 9, 4]])
    #initX = np.matrix([[0, 4, 5, 2, 9, -3]])
    #Jb = [0, 1, 2]
    #Jpos = [4]
    #Jneg = [3, 5]
    try:
        result = solveLower(C, A, b, lower, upper)
    except sa.feasibleRegionIsEmptyError:
        print("There is no solutions")
        return
    print("Optimal solution:", result)
    print("Objective function:", result*C.T)
    print(b)
    print(A*result.T)

def solveLower(C, A, b, lower, upper):
    newB = b - A*lower.T
    newUpper = upper - lower
    result1 = solveUpper(C, A, newB, newUpper)
    return result1+lower

def solveUpper(C, A, b, upper):
    m, n = A.shape
    newA = np.matrix(np.zeros((m+n, n+n)))
    newC = np.matrix(np.zeros((1, n+n)))
    newB = np.matrix(np.zeros((m+n, 1)))
    for i in range(n):
        newC[0, i] = C[0, i]
        newB[i+m, 0] = upper[0, i]
        newA[i+m, i] = 1
        newA[i+m, i+n] = 1
    for i in range(m):
        newB[i, 0] = b[i, 0]
    for i in range(m):
        for j in range(n):
            newA[i, j] = A[i, j]

    solver = sa.SimplexSolver(newA, newB, newC)
    result2 = solver.solve()
    newResult = np.matrix(np.zeros((1, n)))
    for i in range(n):
        newResult[0, i] = result2[0, i]
    return newResult

if __name__ == "__main__":
    main()

