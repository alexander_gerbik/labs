import numpy as np
import linearProgramTransformer as lpt
import simplexAlgorithm as sa
if __name__ == "__main__":
    findMax = True
    C = np.matrix([[-2, -3, -1, -2, 10, -3, 1]])
    E = ['==', '==', '==',]
    A = np.matrix([[1, 1, -2, 2, -2, 0, 8], [0, 1, -1, 3, -2, 0, 7], [1, 1, 0, 4, 2, 1, 4]])
    b = np.matrix([[6], [4], [13]])
    n, a1, c1 = lpt.transform(findMax, A, C, E)
    solver = sa.SimplexSolver(a1, b, c1)
    result = solver.solve()
    for i in range(n, result.shape[1]):
        result = np.delete(result, n, 1)
    print("Optimal solution:", result)
    print("Objective function:", result*C.T)