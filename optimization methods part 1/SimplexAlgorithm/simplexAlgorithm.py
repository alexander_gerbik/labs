import numpy as np
import inverseMatrix


class objectiveFunctionIsUnboundedError(Exception):
    def __init__(self):
        pass


class feasibleRegionIsEmptyError(Exception):
    def __init__(self):
        pass


class SimplexSolver(object):
    def __init__(self, A, B, C, eps=0.001):
        self.eps = eps
        self.m, self.n = A.shape
        assert B.shape[0] == self.m and B.shape[1] == 1, "Vector B should have as much rows as matrix A does"
        assert C.shape[0] == 1 and C.shape[1] == self.n, "Vector C should have as much columns as matrix A does"
        self.A = A
        self.B = B
        self.C = C

    def solve(self):
        x, B, Jb, Jnb, A = self.firstStageCalculation()
        x, B, Jb, Jnb = self.transformFirstStageSolution(x, A, B, Jb, Jnb)
        for i in range(self.n, x.shape[1]):
            x = np.delete(x, self.n, 1)
        Ab = np.matrix([[A[j, i] for i in Jb] for j in range(self.A.shape[0])])
        B = inverseMatrix.getInverseMatrix(Ab)
        Jnb = [i for i in range(self.n) if i not in Jb]
        x, B, Jb, Jnb = SimplexSolver._cycle(x, B, Jb, Jnb, self.A, self.C)
        return x

    def firstStageCalculation(self):
        SimplexSolver._liquidateNegativeB(self.B, self.A)
        C = np.matrix([[0] * (self.m + self.n)])
        for i in range(self.m):
            C[0, self.n + i] = -1
        A = self._getMatrixAForFirstStage()
        Jnb = [i for i in range(self.n)]
        Jb = [i+self.n for i in range(self.m)]
        B = np.matrix(np.identity(self.m))
        x = np.matrix([[0.0] * (self.m + self.n)])
        for i in range(self.m):
            x[0, i + self.n] = self.B[i, 0]
        x, B, Jb, Jnb = SimplexSolver._cycle(x, B, Jb, Jnb, A, C)
        return x, B, Jb, Jnb, A

    def transformFirstStageSolution(self, x, A, B, Jb, Jnb):
        for i in range(self.m):
            if abs(x[0, i + self.n]) > self.eps:
                raise feasibleRegionIsEmptyError()
        while True:
            k = SimplexSolver._getBasicSyntheticIndex(Jb, self.n)
            if k == -1:
                break
            v = SimplexSolver._firstNonZero(B, A, k, self.n, Jnb, self.eps)
            if v == -1:
                i = Jb[k] - self.n
                self.A = np.delete(self.A, i, 0)
                self.B = np.delete(self.B, i, 0)
                self.m -= 1

                A = self._getMatrixAForFirstStage()
                el = Jb.pop(k)
                temp = []
                for j in Jb:
                    if j > el:
                        temp.append(j - 1)
                    else:
                        temp.append(j)
                Jb = temp
                temp = []
                for j in Jnb:
                    if j > el:
                        temp.append(j - 1)
                    else:
                        temp.append(j)
                Jnb = temp
                B = np.delete(B, i, 1)
                B = np.delete(B, k, 0)
                # Ab = np.matrix([[A[j, i] for i in Jb] for j in range(A.shape[0])])
                # B = inverseMatrix.getInverseMatrix(Ab)
                continue
            B = inverseMatrix.getNewInverseMatrix(B, k, A[:, Jnb[v]])
            Jnb[v], Jb[k] = Jb[k], Jnb[v]
        return x, B, Jb, Jnb

    def _getMatrixAForFirstStage(self):
        A = np.matrix(np.zeros((self.m, self.n + self.m)))
        for i in range(self.m):
            for j in range(self.n + self.m):
                if j < self.n:
                    A[i, j] = self.A[i, j]
                elif j == i + self.n:
                    A[i, j] = 1
                else:
                    A[i, j] = 0
        return A

    @staticmethod
    def _cycle(x, B, Jb, Jnb, A, C):
        while True:
            j0 = SimplexSolver._findNegative(A, B, C, Jb, Jnb)
            if j0 == -1:
                return x, B, Jb, Jnb
            Z = B * A[:, j0]
            positiveIndexes = [i for i in range(Z.shape[0]) if Z[i, 0] > 0]
            if len(positiveIndexes) == 0:
                raise objectiveFunctionIsUnboundedError()
            s, theta = SimplexSolver._findS(Z, positiveIndexes, x, Jb)
            x[0, j0] = theta
            for i in range(len(Jb)):
                x[0, Jb[i]] -= theta * Z[i, 0]
            s1 = Jnb.index(j0)
            Jnb[s1], Jb[s] = Jb[s], Jnb[s1]
            B = inverseMatrix.getNewInverseMatrix(B, s, A[:, j0])

    @staticmethod
    def _findNegative(A, B, C, Jb, Jnb):
        Cb = np.matrix([[C[0, i] for i in Jb]])
        U = Cb * B
        delta = np.matrix([(U * A[:, j] - C[0, j])[0, 0] for j in range(A.shape[1])])
        result = delta.shape[1]
        for i in Jnb:
            if delta[0, i] < 0 and i < result:
                result = i
        if result == delta.shape[1]:
            result = -1
        return result

    @staticmethod
    def _firstNonZero(B, A, k, n, Jnb, eps):
        for i, j in enumerate(Jnb):
            if j < n and abs((B * A[:, j])[k, 0]) > eps:
                return i
        return -1

    @staticmethod
    def _findS(Z, positiveIndexes, x, Jb):
        assert len(positiveIndexes) > 0
        s = positiveIndexes[0]
        minElement = x[0, Jb[s]] / Z[s, 0]
        for i in range(1, len(positiveIndexes)):
            current = x[0, Jb[positiveIndexes[i]]] / Z[positiveIndexes[i], 0]
            if current < minElement or (current == minElement and Jb[positiveIndexes[i]] < Jb[s]):
                minElement = current
                s = positiveIndexes[i]
        return s, minElement

    @staticmethod
    def _getBasicSyntheticIndex(Jb, n):
        for i, j in enumerate(Jb):
            if j >= n:
                return i
        return -1

    @staticmethod
    def _liquidateNegativeB(B, A):
        for i in range(B.shape[0]):
            if B[i, 0] < 0:
                B[i, 0] *= -1
                for j in range(A.shape[1]):
                    A[i, j] *= -1