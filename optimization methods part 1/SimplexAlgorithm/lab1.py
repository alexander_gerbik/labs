import numpy as np
import inverseMatrix


def checkSolution(c, columnIndex, column):
    (n, z) = c.shape
    for i in range(n):
        c[i, columnIndex] = column[i, 0]
    return np.linalg.inv(c)


if __name__ == "__main__":
    columnIndex = 2
    c = np.matrix([[0, 1, 0], [4, 3, 1], [2, 2, 3]])
    column = np.matrix([[3], [7], [4]])
    invC = np.linalg.inv(c)

    (success, matrix) = inverseMatrix.getNewInverseMatrix(invC, columnIndex, column)
    if success:
        print(matrix)
        print(checkSolution(c, columnIndex, column))