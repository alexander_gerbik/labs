import numpy as np
import random
import inverseMatrix


def checkSolution(matrix):
    return np.linalg.inv(matrix)


if __name__ == "__main__":
    c = np.matrix([[0, 2, 1], [0, 1, 1], [1, 1, 1]])
    # size = 3
    # c = np.mat(np.zeros((size, size)))
    # (n, z) = c.shape
    # for i in range(n):
    #        for j in range(z):
    #         c[i, j] = random.randint(0, 5)
    print(c)
    (success, matrix) = inverseMatrix.getInverseMatrix(c)
    if success:
        print(matrix)
        print(checkSolution(c))

