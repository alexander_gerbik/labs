import numpy as np
import simplexAlgorithm

if __name__ == "__main__":
    # lab 3
    C = np.matrix([[1, 1, 1, 0, -1]])
    A = np.matrix([[1, 1, 0, -4, 2], [0, 1, 1, 1, -2], [1, 0, 1, 3, -2]])
    b = np.matrix([[-2], [-2], [-2]])
    Jb = [0, 1, 2]
    Jnb = [3, 4]
    #(Нет решений)

    C = np.matrix([[1, 1, -4]])
    A = np.matrix([[1, 1, -3], [1, -2, 1]])
    b = np.matrix([[0], [3]])
    Jb = [0, 1]
    Jnb = [2]
    #(9/4, 0, 3/4)

    C = np.matrix([[1, 1, -2, -3]])
    A = np.matrix([[1, -1, 3, -2], [1, -5, 11, -6]])
    b = np.matrix([[1], [9]])
    Jb = [0, 1]
    Jnb = [2, 3]
    #(0, 0, 3, 4)

    C = np.matrix([[1, -1, 2, 3]])
    A = np.matrix([[2, -1, 1, 0], [-1, 2, 0, 1]])
    b = np.matrix([[1], [1]])
    Jb = [1, 3]
    Jnb = [0, 2]
    #(0, 0, 3, 4)
    # end lab 3

    A = np.matrix([[2, -1, 0, -2, 1, 0], [3, 2, 1, -3, 0, 0], [-1, 3, 0, 4, 0, 1]])
    C = np.matrix([[2, 3, 0, -1, 0, 0]])
    b = np.matrix([[16], [18], [24]])
    # F = 25.63

    A = np.matrix([[1, 4, 4, 1], [1, 7, 8, 2]])
    C = np.matrix([[1, -3, -5, -1]])
    b = np.matrix([[5], [9]])
    #x = np.matrix([[1, 0, 1, 0]])
    #Jb = [0, 2]
    #Jnb = [1, 3]
    # 1 0 0 4

    A = np.matrix([[3, 1, 2, 6, 9, 3], [1, 2, -1, 2, 3, 1]])
    C = np.matrix([[-2, 1, 1, -1, 4, 1]])
    b = np.matrix([[15], [5]])
    #x = np.matrix([[5, 0, 0, 0, 0, 0]])
    #Jb = [0, 1]
    #Jnb = [2, 3, 4, 5]
    # 0 5 5 0 0 0

    A = np.matrix([[3.0, 1, 1, 0], [1, -2, 0, 1]])
    C = np.matrix([[1.0, 4, 1, -1]])
    b = np.matrix([[1.0], [1]])
    #x = np.matrix([[0, 0, 1, 1]])
    #Jb = [2, 3]
    #Jnb = [1, 0]
    # 0 1 0 3

    A = np.matrix([[0, -1, 1, 1, 0], [-5, 1, 1, 0, 0], [-8, 1, 2, 0, -1]])
    C = np.matrix([[-3, 1, 4, 0, 0]])
    b = np.matrix([[1], [2], [3]])
    # 1 3 4 0 0

    A = np.matrix([[1, 1, -2, 3], [2, -1, -1, 3]])
    C = np.matrix([[1, 2, -1, 1]])
    b = np.matrix([[1], [2]])
    # no solution

    A = np.matrix([[2, 0, -1, 1], [3, 3, 0, 3]])
    C = np.matrix([[1, 1, 1, 1]])
    b = np.matrix([[1], [6]])
    #x = np.matrix([[0, 1, 0, 1]])
    #Jb = [1, 3]
    #Jnb = [0, 2]
    # 2 0 3 0

    A = np.matrix([[1, 3, 1, 2], [2, 0, -1, 1], [3, 3, 0, 3]])
    C = np.matrix([[1, 1, 1, 1]])
    b = np.matrix([[5], [1], [6]])
    # x = np.matrix([[0, 1, 0, 1]])
    #Jb = [1, 3]
    #Jnb = [0, 2]
    # 2 0 3 0

    findMax = False
    A = np.matrix([[3, 6, -4, 1], [4, -13, 10, 5], [3, 7, 1, 0]])
    C = np.matrix([[2, 5, 3, 8]])
    b = np.matrix([[12], [6], [1]])
    E = ['<=', '>=', '>=']
    # F = 1.924

    findMax = False
    C = np.matrix([[-1, -2, 1]])
    A = np.matrix([[-1, 4, -2], [1, 1, 2], [2, -1, 2]])
    b = np.matrix([[6], [6], [4]])
    E = ['<=', '>=', '==']
    # F = -7.2

    findMax = False
    C = np.matrix([[2, 5, -3]])
    A = np.matrix([[7, 3, -7], [4, 1, -8], [2, 0, -3]])
    b = np.matrix([[6], [-1], [2]])
    E = ['>=', '>=', '>=']
    # F = 2

    findMax = True
    C = np.matrix([[3, 5, 4]])
    E = ['<=', '<=', '<=']
    A = np.matrix([[0.1, 0.2, 0.4], [0.05, 0.02, 0.02], [3, 1, 2]])
    b = np.matrix([[1100], [120], [8000]])
    # F = 27625

    findMax = False
    C = np.matrix([[19, 21]])
    E = ['>=', '>=']
    A = np.matrix([[2, 5], [4, 1]])
    b = np.matrix([[20], [20]])

    findMax = True
    C = np.matrix([[3, -2, 0, -5, 1]])
    E = ['<=', '<=', '<=', '>=']
    A = np.matrix([[2, 0, 1, 1, 1], [1, 0, -1, 2, 1], [0, 0, 1, -1, 2], [1, -1, 0, 1, -5]])
    b = np.matrix([[2], [3], [6], [8]])

    solver = simplexAlgorithm.SimplexSolver(A, b, C)
    result = solver.solve()
    print(result)
    print(result * C.T)