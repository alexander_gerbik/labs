import numpy as np
import solver


def main():
    C = np.matrix([[-3, -4, 0, 0]])
    A = np.matrix([[1, 0.25, 1, 0], [1, 2, 0, 1]])
    B = np.matrix([[4], [6]])
    D = np.matrix([[1, -0.5, 0, 0], [-0.5, 4, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])

    # solved
    C = np.matrix([[-8, -6, -4, -6]])
    A = np.matrix([[1, 0, 2, 1], [0, 1, -1, 2]])
    B = np.matrix([[2], [3]])
    D = np.matrix([[2, 1, 1, 0], [1, 1, 0, 0], [1, 0, 1, 0], [0, 0, 0, 0]])

    # 1
    C = np.matrix([[0, 0, 0, 0]])
    A = np.matrix([[2, -2, 14, 1], [1, -2, 10, 2]])
    B = np.matrix([[2], [0]])
    D = np.matrix([[4, 0, 0, 0], [0, 3, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])

    # 2
    C = np.matrix([[0, 0, 0, 0, 0, 0]])
    A = np.matrix([[0, 1, 1, 0, -2, 1], [1, 0, 1, 1, 1, 2], [1, 1, 0, 1, -1, 1]])
    B = np.matrix([[4], [3], [3]])
    D = np.matrix([[1, 0, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0], [0, 0, 0, 1, 2, 1], [0, 0, 0, 2, 4, 2],
                   [0, 0, 0, 1, 2, 1]])

    A = np.matrix([[2, 0, 3, 1, 0, 0, -8, 2]
                      , [-1, 1, 0, 1, 1, 0, 6, 3]
                      , [-1, 4, 0, -1, 0, 1, -5, 4]])
    b = np.matrix([[0], [11], [2]])
    B = np.matrix([[2, 5, -7, 0, -3, -5, 1, 3]
                      , [3, -2, 0, 0, 8, -2, -1, 7]
                      , [1, 4, 0, 0, 6, 1, 1, -9]])
    d = np.matrix([[-4], [3], [4]])
    eps = 0.001
    C = -d.T * B
    D = B.T * B + eps * np.matrix(np.identity(8))
    try:
        X = solver.solve(A, b, C, D, eps)
    except solver.feasibleRegionIsEmptyError:
        print("there is no solutions")
        return
    except solver.objectiveFunctionIsUnboundedError:
        print("Objective function is unbounded")
        return
    print("Optimal solution:", X)
    print("Objective function:", (C * X.T + 0.5 * X * D * X.T)[0, 0])


if __name__ == "__main__":
    main()
