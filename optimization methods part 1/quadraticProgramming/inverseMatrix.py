import numpy as np


class determinantIsZeroException(Exception):
    def __init__(self):
        pass


def getInverseMatrix(matrix):
    n = matrix.shape[0]
    B = np.identity(n)
    J = [i for i in range(n)]
    s = [0 for _ in range(n)]
    for i in range(n):
        k = getFirstNonZeroElement(i, matrix, B, J)
        if k == -1:
            # inverse matrix doesn't exist
            raise determinantIsZeroException()
        J.remove(k)
        s[k] = i
        B = np.dot(getMatrixD(n, i, B * matrix[:, k]), B)
    result = np.matrix(np.identity(n))
    for i in range(n):
        result[i, :] = B[s[i], :]
    return result


def getNewInverseMatrix(inverseMatrix, columnIndex, column):
    n = inverseMatrix.shape[0]
    e = getVectorE(n, columnIndex)
    bc = inverseMatrix * column
    a = np.dot(e, bc)[0, 0]
    if a == 0:
        # inverse matrix doesn't exist
        raise determinantIsZeroException()
    d = getMatrixD(n, columnIndex, bc)
    result = d * inverseMatrix
    return result


def getFirstNonZeroElement(i, matrix, B, J):
    for j in J:
        a = (B * matrix[:, j])[i, 0]
        if a != 0:
            return j
    return -1


def getVectorE(n, k):
    e = np.zeros(n)
    e[k] = 1
    return e


def getMatrixD(n, k, column):
    d = np.matrix(np.identity(n))
    element = -1 / column[k, 0]
    for i in range(n):
        d[i, k] = element * column[i, 0]
    d[k, k] = element * -1
    return d