import numpy as np
import transportationProblem


def main():
    producers = np.matrix([[20], [30], [50], [14]])
    consumers = np.matrix([6, 8, 7, 11, 10, 5])
    costs = np.matrix([[2, -3, 4, 8, 9, 15], [6, 3, 8, -9, 5, 6], [4, 3, -1, 0, -1, -5], [7, 14, 0, 1, -3, -5]])
    #producers = np.matrix([[4], [4], [4], [4], [4], [4]])
    #consumers = np.matrix([4, 4, 4, 4, 4, 4])
    #costs = np.matrix([[5, 3, 0, 11, -2, -1], [7, 16, 25, 3, 1, 0], [17, 15, 0, 2, 6, 3], [1, 6, 7, 5, 8, 1], [0, 3, 5, 5, 5, 3], [8, 2, 8, 11, 1, 5]])
    plan = transportationProblem.solve(producers, consumers, costs)
    result = 0
    for i in range(plan.shape[0]):
        for j in range(plan.shape[1]):
            result += plan[i, j] * costs[i, j]
    print(plan)
    print("Costs:", result)

if __name__ == "__main__":
    main()