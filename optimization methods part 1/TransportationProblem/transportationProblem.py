import numpy as np


def solve(producers, consumers, costs):
    x, producers, consumers, costs = transformProblem(producers, consumers, costs)
    producersAmount = producers.shape[0]
    consumersAmount = consumers.shape[1]
    assert costs.shape[0] == producersAmount and costs.shape[1] == consumersAmount
    plan = np.matrix(np.zeros((producersAmount, consumersAmount)))
    U = getFirstPlan(plan, producers, consumers)
    while True:
        printPlan(plan, U)
        producersPotentials, consumersPotentials = getPotentials(costs, U)
        i0, j0 = getPositiveAssessments(producersPotentials, consumersPotentials, costs, U)
        if i0 == -1 and j0 == -1:
            break
        cyclePoints = [(i0, j0)]
        U1 = U[:]
        U1.append((i0, j0))
        a = lookHorizontal(cyclePoints, i0, U1, j0)
        assert a, "There should be a cycle."
        theta, i1, j1 = getTheta(cyclePoints, plan)
        recalculatePlan(cyclePoints, plan, theta)
        U.remove((i1, j1))
        U.append((i0, j0))
    assert plan.shape[0] == producersAmount and plan.shape[1] == consumersAmount
    plan = transformProblemBack(x, plan)
    return plan


def getFirstPlan(plan, producers, consumers):
    """ north-west method """
    m = producers.shape[0]
    n = consumers.shape[1]
    producersLeft = [producers[i, 0] for i in range(m)]
    consumersLeft = [consumers[0, i] for i in range(n)]
    U = []
    i, j = 0, 0
    lastHorizontal = False
    while i < m and j < n:
        U.append((i, j))
        if producersLeft[i] < consumersLeft[j] or (producersLeft[i] == consumersLeft[j] and lastHorizontal):
            plan[i, j] = producersLeft[i]
            consumersLeft[j] -= producersLeft[i]
            i += 1
            lastHorizontal = False
        else:
            plan[i, j] = consumersLeft[j]
            producersLeft[i] -= consumersLeft[j]
            j += 1
            lastHorizontal = True
    return U


def getPotentials(costs, U):
    U = U[:]
    producersPotentials, consumersPotentials = {}, {}
    i, j = U.pop(0)
    producersPotentials[i] = 0
    consumersPotentials[j] = costs[i, j] - producersPotentials[i]
    while len(U) != 0:
        for k, (i, j) in enumerate(U):
            if i in producersPotentials:
                consumersPotentials[j] = costs[i, j] - producersPotentials[i]
                U.pop(k)
                break
            elif j in consumersPotentials:
                producersPotentials[i] = costs[i, j] - consumersPotentials[j]
                U.pop(k)
                break
    return producersPotentials, consumersPotentials


def getPositiveAssessments(producersPotentials, consumersPotentials, costs, U):
    m, n = costs.shape
    for i in range(m):
        for j in range(n):
            if (i, j) not in U:
                assessment = producersPotentials[i] + consumersPotentials[j] - costs[i, j]
                if assessment > 0:
                    return i, j
    return -1, -1


def lookHorizontal(result, i, U, jEnd):
    for k, (l, m) in enumerate(U):
        if l == i:
            U1 = U[:]
            U1.pop(k)
            result.append((i, m))
            if lookVertical(result, m, U1, jEnd):
                return True
            result.pop(len(result) - 1)
    return False


def lookVertical(result, j, U, jEnd):
    if j == jEnd:
        return True
    for k, (l, m) in enumerate(U):
        if m == j:
            U1 = U[:]
            U1.pop(k)
            result.append((l, j))
            if lookHorizontal(result, l, U1, jEnd):
                return True
            result.pop(len(result) - 1)
    return False


def getTheta(cyclePoints, x):
    theta = float('inf')
    thetaI, thetaJ = -1, -1
    for k in range(1, len(cyclePoints), 2):
        i, j = cyclePoints[k]
        if x[i, j] < theta:
            theta = x[i, j]
            thetaI, thetaJ = i, j
    return theta, thetaI, thetaJ


def recalculatePlan(cyclePoints, x, theta):
    for k in range(len(cyclePoints)):
        i, j = cyclePoints[k]
        if k % 2 == 0:
            x[i, j] += theta
        else:
            x[i, j] -= theta


def transformProblem(producers, consumers, costs):
    a = 0
    for i in range(producers.shape[0]):
        for j in range(producers.shape[1]):
            a += producers[i, j]
    b = 0
    for i in range(consumers.shape[0]):
        for j in range(consumers.shape[1]):
            b += consumers[i, j]
    c = a - b
    if c == 0:
        return 0, producers, consumers, costs
    if c > 0:
        return 1, producers, np.c_[consumers, [c]], np.c_[costs, [0 for _ in range(producers.shape[0])]]
    if c < 0:
        return -1, np.r_[producers, [-c]], consumers, np.r_[costs, [0 for _ in range(consumers.shape[1])]]


def transformProblemBack(x, plan):
    if x == 0:
        return plan
    if x < 0:
        return np.delete(plan, plan.shape[0] - 1, 0)
    return np.delete(plan, plan.shape[1] - 1, 1)


def printPlan(plan, U):
    z = np.copy(plan)
    for i in range(plan.shape[0]):
        for j in range(plan.shape[1]):
            if (i, j) not in U:
                z[i, j] = float('inf')
    print(z)