import unittest

import numpy

from algorithms import dual_simplex_algorithm

eps = 0.001


class DualSimplexAlgorithmTests(unittest.TestCase):
    def test_example1(self):
        a_matrix = numpy.matrix([[2, 1, -1, 0, 0, 1], [1, 0, 1, 1, 0, 0], [0, 1, 0, 0, 1, 0]])
        b_matrix = numpy.matrix([[2.], [5.], [0.]])
        c_matrix = numpy.matrix([[3, 2, 0, 3, -2, -4]])
        d_lower = numpy.matrix([[0, -1, 2, 1, -1, 0]])
        d_upper = numpy.matrix([[2, 4, 4, 3, 3, 5]])
        j_basis = [3, 4, 5]
        expected = 13
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper, j_basis)
        self.assertAlmostEqual(result, expected)

    def test_example2(self):
        a_matrix = numpy.matrix([[1, -5, 3, 1, 0, 0], [4, -1, 1, 0, 1, 0], [2, 4, 2, 0, 0, 1]])
        b_matrix = numpy.matrix([[-7.], [22.], [30.]])
        c_matrix = numpy.matrix([[7, -2, 6, 0, 5, 2]])
        d_lower = numpy.matrix([[2, 1, 0, 0, 1, 1]])
        d_upper = numpy.matrix([[6, 6, 5, 2, 4, 6]])
        j_basis = [3, 4, 5]
        expected = 67
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper, j_basis)
        self.assertAlmostEqual(result, expected)

    def test_example3(self):
        a_matrix = numpy.matrix([[1, 0, 2, 2, -3, 3], [0, 1, 0, -1, 0, 1], [1, 0, 1, 3, 2, 1]])
        b_matrix = numpy.matrix([[15.], [0.], [13.]])
        c_matrix = numpy.matrix([[3, 0.5, 4, 4, 1, 5]])
        d_lower = numpy.matrix([[0, 0, 0, 0, 0, 0]])
        d_upper = numpy.matrix([[3, 5, 4, 3, 3, 4]])
        j_basis = [0, 1, 2]
        expected = 36.2727
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper, j_basis)
        self.assertAlmostEqual(result, expected, places=4)

    def test_example4(self):
        a_matrix = numpy.matrix([[1, 0, 0, 12, 1, -3, 4, -1], [0, 1, 0, 11, 12, 3, 5, 3], [0, 0, 1, 1, 0, 22, -2, 1]])
        b_matrix = numpy.matrix([[40.], [107.], [61.]])
        c_matrix = numpy.matrix([[2, 1, -2, -1, 4, -5, 5, 5]])
        d_lower = numpy.matrix([[0, 0, 0, 0, 0, 0, 0, 0]])
        d_upper = numpy.matrix([[3, 5, 5, 3, 4, 5, 6, 3]])
        j_basis = [0, 1, 2]
        expected = 49.6577
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper, j_basis)
        self.assertAlmostEqual(result, expected, places=4)

    def test_example5(self):
        a_matrix = numpy.matrix(
            [[1, -3, 2, 0, 1, -1, 4, -1, 0], [1, -1, 6, 1, 0, -2, 2, 2, 0], [2, 2, -1, 1, 0, -3, 8, -1, 1],
             [4, 1, 0, 0, 1, -1, 0, -1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1]])
        b_matrix = numpy.matrix([[3.], [9.], [9.], [5.], [9.]])
        c_matrix = numpy.matrix([[-1, 5, -2, 4, 3, 1, 2, 8, 3]])
        d_lower = numpy.matrix([[0, 0, 0, 0, 0, 0, 0, 0, 0]])
        d_upper = numpy.matrix([[5, 5, 5, 5, 5, 5, 5, 5, 5]])
        j_basis = [0, 1, 2, 3, 4]
        expected = 38.7218
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper, j_basis)
        self.assertAlmostEqual(result, expected, places=4)

    def test_example6(self):
        a_matrix = numpy.matrix([[1, 7, 2, 0, 1, -1, 4], [0, 5, 6, 1, 0, -3, -2], [3, 2, 2, 1, 1, 1, 5]])
        b_matrix = numpy.matrix([[1.], [4.], [7.]])
        c_matrix = numpy.matrix([[1, 2, 1, -3, 3, 1, 0]])
        d_lower = numpy.matrix([[-1, 1, -2, 0, 1, 2, 4]])
        d_upper = numpy.matrix([[3, 2, 2, 5, 3, 4, 5]])
        j_basis = [0, 1, 2]
        with self.assertRaises(dual_simplex_algorithm.FeasibleRegionIsEmptyError):
            result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper, j_basis)

    def test_example7(self):
        a_matrix = numpy.matrix([[2, -1, 1, 0, 0, -1, 3], [0, 4, -1, 2, 3, -2, 2], [3, 1, 0, 1, 0, 1, 4]])
        b_matrix = numpy.matrix([[1.5], [9.], [2.]])
        c_matrix = numpy.matrix([[0, 1, 2, 1, -3, 4, 7]])
        d_lower = numpy.matrix([[0, 0, -3, 0, -1, 1, 0]])
        d_upper = numpy.matrix([[3, 3, 4, 7, 5, 3, 2]])
        j_basis = [0, 1, 2]
        expected = 1.5
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper, j_basis)
        self.assertAlmostEqual(result, expected)

    def test_example8(self):
        a_matrix = numpy.matrix([[2, 1, 0, 3, -1, -1], [0, 1, -2, 1, 0, 3], [3, 0, 1, 1, 1, 1]])
        b_matrix = numpy.matrix([[2.], [2.], [5.]])
        c_matrix = numpy.matrix([[0, -1, 1, 0, 4, 3]])
        d_lower = numpy.matrix([[2, 0, -1, -3, 2, 1]])
        d_upper = numpy.matrix([[7, 3, 2, 3, 4, 5]])
        j_basis = [0, 1, 2]
        with self.assertRaises(dual_simplex_algorithm.FeasibleRegionIsEmptyError):
            result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper, j_basis)

    def test_example9(self):
        a_matrix = numpy.matrix([[1, 3, 1, -1, 0, -3, 2, 1], [2, 1, 3, -1, 1, 4, 1, 1], [-1, 0, 2, -2, 2, 1, 1, 1]])
        b_matrix = numpy.matrix([[4.], [12.], [4.]])
        c_matrix = numpy.matrix([[2, -1, 2, 3, -2, 3, 4, 1]])
        d_lower = numpy.matrix([[-1, -1, -1, -1, -1, -1, -1, -1]])
        d_upper = numpy.matrix([[2, 3, 1, 4, 3, 2, 4, 4]])
        j_basis = [0, 1, 2]
        expected = 37.5556
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper, j_basis)
        self.assertAlmostEqual(result, expected, places=4)


def solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper, j_basis):
    plan, _ = dual_simplex_algorithm.solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper, j_basis, eps)
    return (plan * c_matrix.T)[0, 0]

if __name__ == "__main__":
    unittest.main()
