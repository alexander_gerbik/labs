import unittest

from algorithms.shortest_path import ShortestPath

f = float('inf')


class ShortestPathTests(unittest.TestCase):
    def test_task1(self):
        paths = [[0, 9, f, 3, f, f, f, f],
                 [9, 0, 2, f, 7, f, f, f],
                 [f, 2, 0, 2, 4, 8, 6, f],
                 [3, f, 2, 0, f, f, 5, f],
                 [f, 7, 4, f, 0, 10, f, f],
                 [f, f, 8, f, 10, 0, 7, f],
                 [f, f, 6, 5, f, 7, 0, f],
                 [f, f, f, f, 9, 12, 10, 0]]
        expected_result = [[0, 7, 5, 3, 9, 13, 8, f],
                           [7, 0, 2, 4, 6, 10, 8, f],
                           [5, 2, 0, 2, 4, 8, 6, f],
                           [3, 4, 2, 0, 6, 10, 5, f],
                           [9, 6, 4, 6, 0, 10, 10, f],
                           [13, 10, 8, 10, 10, 0, 7, f],
                           [8, 8, 6, 5, 10, 7, 0, f],
                           [18, 15, 13, 15, 9, 12, 10, 0]]
        expected_next_node = [[1, 4, 4, 4, 4, 4, 4, 8],
                              [3, 2, 3, 3, 3, 3, 3, 8],
                              [4, 2, 3, 4, 5, 6, 7, 8],
                              [1, 3, 3, 4, 3, 3, 7, 8],
                              [3, 3, 3, 3, 5, 6, 3, 8],
                              [3, 3, 3, 3, 5, 6, 7, 8],
                              [4, 3, 3, 4, 3, 6, 7, 8],
                              [5, 5, 5, 5, 5, 6, 7, 8]]
        result, next_node = solve(paths)
        self.assertEqual(result, expected_result)
        self.assertEqual(next_node, expected_next_node)

    def test_task2(self):
        paths = [[0, 3, 2, 6, f, f, f, f, f],
                 [f, 0, f, 2, f, f, f, f, f],
                 [f, f, 0, f, f, 4, f, f, f],
                 [f, f, 3, 0, 1, f, 6, f, f],
                 [f, f, f, f, 0, f, 7, 5, f],
                 [f, f, f, f, 5, 0, f, 4, f],
                 [f, f, f, f, f, f, 0, 2, 4],
                 [f, f, f, f, f, f, f, 0, 4],
                 [f, f, f, f, f, f, f, f, 0]]
        expected_result = [[0, 3, 2, 5, 6, 6, 11, 10, 14],
                           [f, 0, 5, 2, 3, 9, 8, 8, 12],
                           [f, f, 0, f, 9, 4, 16, 8, 12],
                           [f, f, 3, 0, 1, 7, 6, 6, 10],
                           [f, f, f, f, 0, f, 7, 5, 9],
                           [f, f, f, f, 5, 0, 12, 4, 8],
                           [f, f, f, f, f, f, 0, 2, 4],
                           [f, f, f, f, f, f, f, 0, 4],
                           [f, f, f, f, f, f, f, f, 0]]
        expected_next_node = [[1, 2, 3, 2, 2, 3, 2, 3, 3],
                              [1, 2, 4, 4, 4, 4, 4, 4, 4],
                              [1, 2, 3, 4, 6, 6, 6, 6, 6],
                              [1, 2, 3, 4, 5, 3, 7, 5, 7],
                              [1, 2, 3, 4, 5, 6, 7, 8, 8],
                              [1, 2, 3, 4, 5, 6, 5, 8, 8],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9]]
        result, next_node = solve(paths)
        self.assertEqual(result, expected_result)
        self.assertEqual(next_node, expected_next_node)

    def test_task3(self):
        paths = [[0, 3, 2, 6, f, f, f, f, f],
                 [f, 0, f, 2, f, f, f, f, f],
                 [f, f, 0, f, f, 4, f, f, f],
                 [f, f, 3, 0, 1, f, 6, f, f],
                 [f, f, f, f, 0, f, 7, 5, f],
                 [f, f, f, f, 5, 0, f, 4, f],
                 [f, f, f, f, f, f, 0, 2, 4],
                 [f, f, f, f, f, f, f, 0, 15],
                 [f, f, f, f, f, f, f, f, 0]]
        expected_result = [[0, 3, 2, 5, 6, 6, 11, 10, 15],
                           [f, 0, 5, 2, 3, 9, 8, 8, 12],
                           [f, f, 0, f, 9, 4, 16, 8, 20],
                           [f, f, 3, 0, 1, 7, 6, 6, 10],
                           [f, f, f, f, 0, f, 7, 5, 11],
                           [f, f, f, f, 5, 0, 12, 4, 16],
                           [f, f, f, f, f, f, 0, 2, 4],
                           [f, f, f, f, f, f, f, 0, 15],
                           [f, f, f, f, f, f, f, f, 0]]
        expected_next_node = [[1, 2, 3, 2, 2, 3, 2, 3, 2],
                              [1, 2, 4, 4, 4, 4, 4, 4, 4],
                              [1, 2, 3, 4, 6, 6, 6, 6, 6],
                              [1, 2, 3, 4, 5, 3, 7, 5, 7],
                              [1, 2, 3, 4, 5, 6, 7, 8, 7],
                              [1, 2, 3, 4, 5, 6, 5, 8, 5],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9]]
        result, next_node = solve(paths)
        self.assertEqual(result, expected_result)
        self.assertEqual(next_node, expected_next_node)

    def test_task4(self):
        paths = [[0, 3, 4, f, 5, f, f, f],
                 [f, 0, 2, 1, f, f, 4, f],
                 [f, f, 0, 3, 2, f, f, f],
                 [f, f, f, 0, f, f, 3, f],
                 [f, f, f, 4, 0, 8, f, 3],
                 [f, f, f, 5, f, 0, f, 2],
                 [f, f, f, f, f, 2, 0, 1],
                 [f, f, f, f, f, f, f, 0]]
        expected_result = [[0, 3, 4, 4, 5, 9, 7, 8],
                           [f, 0, 2, 1, 4, 6, 4, 5],
                           [f, f, 0, 3, 2, 8, 6, 5],
                           [f, f, f, 0, f, 5, 3, 4],
                           [f, f, f, 4, 0, 8, 7, 3],
                           [f, f, f, 5, f, 0, 8, 2],
                           [f, f, f, 7, f, 2, 0, 1],
                           [f, f, f, f, f, f, f, 0]]
        expected_next_node = [[1, 2, 3, 2, 5, 2, 2, 5],
                              [1, 2, 3, 4, 3, 7, 7, 7],
                              [1, 2, 3, 4, 5, 4, 4, 5],
                              [1, 2, 3, 4, 5, 7, 7, 7],
                              [1, 2, 3, 4, 5, 6, 4, 8],
                              [1, 2, 3, 4, 5, 6, 4, 8],
                              [1, 2, 3, 6, 5, 6, 7, 8],
                              [1, 2, 3, 4, 5, 6, 7, 8]]
        result, next_node = solve(paths)
        self.assertEqual(result, expected_result)
        self.assertEqual(next_node, expected_next_node)

    def test_task5(self):
        paths = [[0, 3, 4, f, 5, f, f, f],
                 [f, 0, f, 1, f, f, 4, f],
                 [f, f, 0, 3, 2, f, f, f],
                 [f, f, f, 0, f, f, 1, f],
                 [f, f, f, 4, 0, 8, f, 3],
                 [f, f, f, 5, f, 0, f, 2],
                 [f, f, f, f, f, 2, 0, 1],
                 [f, f, f, f, f, f, f, 0]]
        expected_result = [[0, 3, 4, 4, 5, 7, 5, 6],
                           [f, 0, f, 1, f, 4, 2, 3],
                           [f, f, 0, 3, 2, 6, 4, 5],
                           [f, f, f, 0, f, 3, 1, 2],
                           [f, f, f, 4, 0, 7, 5, 3],
                           [f, f, f, 5, f, 0, 6, 2],
                           [f, f, f, 7, f, 2, 0, 1],
                           [f, f, f, f, f, f, f, 0]]
        expected_next_node = [[1, 2, 3, 2, 5, 2, 2, 2],
                              [1, 2, 3, 4, 5, 4, 4, 4],
                              [1, 2, 3, 4, 5, 4, 4, 5],
                              [1, 2, 3, 4, 5, 7, 7, 7],
                              [1, 2, 3, 4, 5, 4, 4, 8],
                              [1, 2, 3, 4, 5, 6, 4, 8],
                              [1, 2, 3, 6, 5, 6, 7, 8],
                              [1, 2, 3, 4, 5, 6, 7, 8]]
        result, next_node = solve(paths)
        self.assertEqual(result, expected_result)
        self.assertEqual(next_node, expected_next_node)

    def test_task6(self):
        paths = [[0, 6, 1, 5, f, f, f, f, f, f, f, f, f, f, f],
                 [f, 0, 4, f, 2, 3, f, f, f, f, f, f, f, f, f],
                 [f, f, 0, 2, f, f, 5, f, f, f, f, f, f, f, f],
                 [f, f, f, 0, f, f, 6, 6, f, f, f, f, f, f, f],
                 [f, f, f, f, 0, f, f, f, 10, f, f, f, f, f, f],
                 [f, f, f, f, 4, 0, f, f, f, 7, f, f, f, f, f],
                 [f, f, f, f, f, 20, 0, f, f, 10, 5, f, f, f, f],
                 [f, f, f, f, f, f, 2, 0, f, f, 3, 4, f, f, f],
                 [f, f, f, f, f, f, f, f, 0, 1, f, f, 3, f, f],
                 [f, f, f, f, f, f, f, f, f, 0, f, f, 2, f, f],
                 [f, f, f, f, f, f, f, f, f, 8, 0, f, f, 2, f],
                 [f, f, f, f, f, f, f, f, f, f, f, 0, f, 1, f],
                 [f, f, f, f, f, f, f, f, f, f, f, f, 0, 3, 4],
                 [f, f, f, f, f, f, f, f, f, f, f, f, f, 0, 5],
                 [f, f, f, f, f, f, f, f, f, f, f, f, f, f, 0]]
        expected_result = [[0, 6, 1, 3, 8, 9, 6, 9, 18, 16, 11, 13, 18, 13, 18],
                           [f, 0, 4, 6, 2, 3, 9, 12, 12, 10, 14, 16, 12, 15, 16],
                           [f, f, 0, 2, 29, 25, 5, 8, 39, 15, 10, 12, 17, 12, 17],
                           [f, f, f, 0, 30, 26, 6, 6, 40, 16, 9, 10, 18, 11, 16],
                           [f, f, f, f, 0, f, f, f, 10, 11, f, f, 13, 16, 17],
                           [f, f, f, f, 4, 0, f, f, 14, 7, f, f, 9, 12, 13],
                           [f, f, f, f, 24, 20, 0, f, 34, 10, 5, f, 12, 7, 12],
                           [f, f, f, f, 26, 22, 2, 0, 36, 11, 3, 4, 13, 5, 10],
                           [f, f, f, f, f, f, f, f, 0, 1, f, f, 3, 6, 7],
                           [f, f, f, f, f, f, f, f, f, 0, f, f, 2, 5, 6],
                           [f, f, f, f, f, f, f, f, f, 8, 0, f, 10, 2, 7],
                           [f, f, f, f, f, f, f, f, f, f, f, 0, f, 1, 6],
                           [f, f, f, f, f, f, f, f, f, f, f, f, 0, 3, 4],
                           [f, f, f, f, f, f, f, f, f, f, f, f, f, 0,
                            5],
                           [f, f, f, f, f, f, f, f, f, f, f, f, f,
                            f, 0]]
        expected_next_node = [[1, 2, 3, 3, 2, 2, 3, 3, 2, 2, 3, 3, 2, 3, 3],
                              [1, 2, 3, 3, 5, 6, 3, 3, 5, 6, 3, 3, 6, 6, 6],
                              [1, 2, 3, 4, 7, 7, 7, 4, 7, 7, 7, 4, 7, 7, 7],
                              [1, 2, 3, 4, 7, 7, 7, 8, 7, 7, 8, 8, 7, 8, 8],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 11, 12, 9, 9, 9],
                              [1, 2, 3, 4, 5, 6, 7, 8, 5, 10, 11, 12, 10, 10, 10],
                              [1, 2, 3, 4, 6, 6, 7, 8, 6, 10, 11, 12, 10, 11, 11],
                              [1, 2, 3, 4, 7, 7, 7, 8, 7, 11, 11, 12, 11, 11, 11],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 13, 13],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 13, 13],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 10, 14, 14],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 14],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]]
        result, next_node = solve(paths)
        self.assertEqual(result, expected_result)
        self.assertEqual(next_node, expected_next_node)

    def test_task7(self):
        paths = [[0, 2, f, f, 5, f, f, 7, f, f, 3, f, f, f],
                 [f, 0, 3, f, 4, f, f, f, f, f, f, f, f, f],
                 [f, f, 0, 7, f, f, f, f, f, f, f, f, f, f],
                 [f, f, f, 0, f, f, 9, f, f, f, f, f, f, 3],
                 [f, f, 1, f, 0, 2, f, f, f, f, f, f, f, f],
                 [f, f, 5, 8, f, 0, f, f, 2, 3, f, f, f, f],
                 [f, f, f, f, f, f, 0, f, f, f, f, f, f, 5],
                 [f, f, f, f, f, 1, f, 0, f, f, 4, 2, f, f],
                 [f, f, f, f, f, f, f, f, 0, f, f, 7, 1, f],
                 [f, f, f, f, f, f, 4, f, f, 0, f, f, f, 2],
                 [f, f, f, f, f, f, f, f, f, f, 0, 6, f, f],
                 [f, f, f, f, f, f, f, f, f, f, f, 0, 4, f],
                 [f, f, f, f, f, f, f, f, f, 3, f, f, 0, 4],
                 [f, f, f, f, f, f, f, f, f, f, f, f, f, 0]]
        expected_result = [[0, 2, 5, 12, 5, 7, 14, 7, 9, 10, 3, 9, 10, 12],
                           [f, 0, 3, 10, 4, 6, 13, f, 8, 9, f, 15, 9, 11],
                           [f, f, 0, 7, f, f, 16, f, f, f, f, f, f, 10],
                           [f, f, f, 0, f, f, 9, f, f, f, f, f, f, 3],
                           [f, f, 1, 8, 0, 2, 9, f, 4, 5, f, 11, 5, 7],
                           [f, f, 5, 8, f, 0, 7, f, 2, 3, f, 9, 3, 5],
                           [f, f, f, f, f, f, 0, f, f, f, f, f, f, 5],
                           [f, f, 6, 9, f, 1, 8, 0, 3, 4, 4, 2, 4, 6],
                           [f, f, f, f, f, f, 8, f, 0, 4, f, 7, 1, 5],
                           [f, f, f, f, f, f, 4, f, f, 0, f, f, f, 2],
                           [f, f, f, f, f, f, 17, f, f, 13, 0, 6, 10, 14],
                           [f, f, f, f, f, f, 11, f, f, 7, f, 0, 4, 8],
                           [f, f, f, f, f, f, 7, f, f, 3, f, f, 0, 4],
                           [f, f, f, f, f, f, f, f, f, f, f, f, f, 0]]
        expected_next_node = [[1, 2, 2, 2, 5, 5, 5, 8, 5, 5, 11, 8, 5, 5],
                              [1, 2, 3, 3, 5, 5, 5, 8, 5, 5, 11, 5, 5, 5],
                              [1, 2, 3, 4, 5, 6, 4, 8, 9, 10, 11, 12, 13, 4],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
                              [1, 2, 3, 3, 5, 6, 6, 8, 6, 6, 11, 6, 6, 6],
                              [1, 2, 3, 4, 5, 6, 10, 8, 9, 10, 11, 9, 9, 10],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
                              [1, 2, 6, 6, 5, 6, 6, 8, 6, 6, 11, 12, 6, 6],
                              [1, 2, 3, 4, 5, 6, 13, 8, 9, 13, 11, 12, 13, 13],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
                              [1, 2, 3, 4, 5, 6, 12, 8, 9, 12, 11, 12, 12, 12],
                              [1, 2, 3, 4, 5, 6, 13, 8, 9, 13, 11, 12, 13, 13],
                              [1, 2, 3, 4, 5, 6, 10, 8, 9, 10, 11, 12, 13, 14],
                              [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]]
        result, next_node = solve(paths)
        self.assertEqual(result, expected_result)
        self.assertEqual(next_node, expected_next_node)


def solve(paths):
    solver = ShortestPath(paths)
    return solver._distance, increase_each_element_by_one(solver._next)


def increase_each_element_by_one(matrix):
    return [[i + 1 for i in row] for row in matrix]


if __name__ == "__main__":
    unittest.main()
