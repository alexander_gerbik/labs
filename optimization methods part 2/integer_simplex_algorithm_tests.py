import unittest

import numpy as np

from algorithms import integer_simplex_algorithm


class IntegerSimplexAlgorithmTests(unittest.TestCase):
    def test_example1(self):
        a_matrix = np.matrix([[1, -5, 3, 1, 0, 0], [4, -1, 1, 0, 1, 0], [2, 4, 2, 0, 0, 1]])
        b_matrix = np.matrix([[-8.], [22.], [30.]])
        c_matrix = np.matrix([[7, -2, 6, 0, 5, 2]])
        d_lower = np.matrix([[2, 1, 0, 0, 1, 1]])
        d_upper = np.matrix([[6, 6, 5, 2, 4, 6]])
        expected = 53
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper)
        self.assertAlmostEqual(result, expected)

    def test_example2(self):
        a_matrix = np.matrix([[1, 0, 3, 1, 0, 0], [0, -1, 1, 1, 1, 2], [-2, 4, 2, 0, 0, 1]])
        b_matrix = np.matrix([[10.], [8.], [10.]])
        c_matrix = np.matrix([[7, -2, 6, 0, 5, 2]])
        d_lower = np.matrix([[0, 1, -1, 0, -2, 1]])
        d_upper = np.matrix([[3, 3, 6, 2, 4, 6]])
        expected = 37
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper)
        self.assertAlmostEqual(result, expected)

    def test_example3(self):
        a_matrix = np.matrix([[1, 0, 1, 0, 0, 1], [1, 2, -1, 1, 1, 2], [-2, 4, 1, 0, 1, 0]])
        b_matrix = np.matrix([[-3.], [3.], [13.]])
        c_matrix = np.matrix([[-3, 2, 0, -2, -5, 2]])
        d_lower = np.matrix([[-2, -1, -2, 0, 1, -4]])
        d_upper = np.matrix([[2, 3, 1, 5, 4, -1]])
        expected = -1
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper)
        self.assertAlmostEqual(result, expected)

    def test_example4(self):
        a_matrix = np.matrix([[1, 0, 0, 12, 1, -3, 4, -1], [0, 1, 0, 11, 12, 3, 5, 3], [0, 0, 1, 1, 0, 22, -2, 1]])
        b_matrix = np.matrix([[40.], [107.], [61.]])
        c_matrix = np.matrix([[2, 1, -2, -1, 4, -5, 5, 5]])
        d_lower = np.matrix([[0.0, 0, 0, 0, 0, 0, 0, 0]])
        d_upper = np.matrix([[3.0, 5, 5, 3, 4, 5, 6, 3]])
        expected = 39
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper)
        self.assertAlmostEqual(result, expected)

    def test_example5(self):
        a_matrix = np.matrix([
            [1, -3, 2, 0, 1, -1, 4, -1, 0],
            [1, -1, 6, 1, 0, -2, 2, 2, 0],
            [2, 2, -1, 1, 0, -3, 8, -1, 1],
            [4, 1, 0, 0, 1, -1, 0, -1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1]])
        b_matrix = np.matrix([[3.], [9.], [9.], [5.], [9.]])
        c_matrix = np.matrix([[-1, 5, -2, 4, 3, 1, 2, 8, 3]])
        d_lower = np.matrix([[0, 0, 0, 0, 0, 0, 0, 0, 0]])
        d_upper = np.matrix([[5, 5, 5, 5, 5, 5, 5, 5, 5]])
        expected = 23
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper)
        self.assertAlmostEqual(result, expected)

    def test_example6(self):
        a_matrix = np.matrix([[1, 0, 0, 12, 1, -3, 4, -1, 2.5, 3],
                              [0, 1, 0, 11, 12, 3, 5, 3, 4, 5.1],
                              [0, 0, 1, 1, 0, 22, -2, 1, 6.1, 7]])
        b_matrix = np.matrix([[43.5], [107.3], [106.3]])
        c_matrix = np.matrix([[2, 1, -2, -1, 4, -5, 5, 5, 1, 2]])
        d_lower = np.matrix([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
        d_upper = np.matrix([[2, 4, 5, 3, 4, 5, 4, 4, 5, 6]])
        expected = 29
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper)
        self.assertAlmostEqual(result, expected)

    def test_example7(self):
        a_matrix = np.matrix([
            [4, 0, 0, 0, 0, -3, 4, -1, 2, 3],
            [0, 1, 0, 0, 0, 3, 5, 3, 4, 5],
            [0, 0, 1, 0, 0, 22, -2, 1, 6, 7],
            [0, 0, 0, 1, 0, 6, -2, 7, 5, 6],
            [0, 0, 0, 0, 1, 5, 5, 1, 6, 7]])
        b_matrix = np.matrix([[8.], [5.], [4.], [7.], [8.]])
        c_matrix = np.matrix([[2, 1, -2, -1, 4, -5, 5, 5, 1, 2]])
        d_lower = np.matrix([[0.0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
        d_upper = np.matrix([[10.0, 10, 10, 10, 10, 10, 10, 10, 10, 10]])
        expected = 26
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper)
        self.assertAlmostEqual(result, expected)

    def test_example8(self):
        a_matrix = np.matrix([[1, -5, 3, 1, 0, 0], [4, -1, 1, 0, 1, 0], [2, 4, 2, 0, 0, 1]])
        b_matrix = np.matrix([[-8.], [22.], [30.]])
        c_matrix = np.matrix([[7, -2, 6, 0, 5, 2]])
        d_lower = np.matrix([[2, 1, 0, 0, 1, 1]])
        d_upper = np.matrix([[6, 6, 5, 2, 4, 6]])
        expected = 53
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper)
        self.assertAlmostEqual(result, expected)

    def test_example9(self):
        a_matrix = np.matrix([[1, 0, 0, 3, 1, -3, 4, -1], [0, 1, 0, 4, -3, 3, 5, 3], [0, 0, 1, 1, 0, 2, -2, 1]])
        b_matrix = np.matrix([[30.], [78.], [5.]])
        c_matrix = np.matrix([[2, 1, -2, -1, 4, -5, 5, 5]])
        d_lower = np.matrix([[0, 0, 0, 0, 0, 0, 0, 0]])
        d_upper = np.matrix([[5, 5, 3, 4, 5, 6, 6, 8]])
        expected = 70
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper)
        self.assertAlmostEqual(result, expected)

    def test_example10(self):
        a_matrix = np.matrix(
            [[1, -3, 2, 0, 1, -1, 4, -1, 0], [1, -1, 6, 1, 0, -2, 2, 2, 0], [2, 2, -1, 1, 0, -3, 2, -1, 1],
             [4, 1, 0, 0, 1, -1, 0, -1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1]])
        b_matrix = np.matrix([[18.], [18.], [30.], [15.], [18.]])
        c_matrix = np.matrix([[7, 5, -2, 4, 3, 1, 2, 8, 3]])
        d_lower = np.matrix([[0, 0, 0, 0, 0, 0, 0, 0, 0]])
        d_upper = np.matrix([[8, 8, 8, 8, 8, 8, 8, 8, 8]])
        expected = 78
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper)
        self.assertAlmostEqual(result, expected)

    def test_example11(self):
        a_matrix = np.matrix([[1, 0, 1, 0, 4, 3, 4], [0, 1, 2, 0, 55, 3.5, 5], [0, 0, 3, 1, 6, 2, -2.5]])
        b_matrix = np.matrix([[26.], [185], [32.5]])
        c_matrix = np.matrix([[1, 2, 3, -1, 4, -5, 6]])
        d_lower = np.matrix([[0, 2, 0, 0, 0, 0, 0]])
        d_upper = np.matrix([[1, 2, 5, 7, 8, 4, 2]])
        expected = 18
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper)
        self.assertAlmostEqual(result, expected)

    def test_example12(self):
        a_matrix = np.matrix(
            [[2, 0, 1, 0, 0, 3, 5], [0, 2, 2.1, 0, 0, 3.5, 5], [0, 0, 3, 2, 0, 2, 1.1], [0, 0, 3, 0, 2, 2, -2.5]])
        b_matrix = np.matrix([[58], [66.3], [36.7], [13.5]])
        c_matrix = np.matrix([[1, 2, 3, 1, 2, 3, 4]])
        d_lower = np.matrix([[1, 1, 1, 1, 1, 1, 1]])
        d_upper = np.matrix([[2, 3, 4, 5, 8, 7, 7]])
        expected = 74
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper)
        self.assertAlmostEqual(result, expected)

    def test_example13(self):
        a_matrix = np.matrix(
            [[1, 0, 0, 1, 1, -3, 4, -1, 3, 3], [0, 1, 0, -2, 1, 1, 7, 3, 4, 5], [0, 0, 1, 1, 0, 2, -2, 1, -4, 7]])
        b_matrix = np.matrix([[27.], [6], [18]])
        c_matrix = np.matrix([[-2, 1, -2, -1, 8, -5, 3, 5, 1, 2]])
        d_lower = np.matrix([[0.0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
        d_upper = np.matrix([[8.0, 7, 6, 7, 8, 5, 6, 7, 8, 5]])
        expected = 40
        result = solve(a_matrix, b_matrix, c_matrix, d_lower, d_upper)
        self.assertAlmostEqual(result, expected)


def solve(a, b, c, lower, upper):
    eps = 0.0001
    indexes = [i for i in range(a.shape[1])]
    solver = integer_simplex_algorithm.IntegerSimplexAlgorithm(a, b, c, lower, upper, indexes, eps)
    plan, result = solver.solve()
    return result


if __name__ == "__main__":
    unittest.main()
