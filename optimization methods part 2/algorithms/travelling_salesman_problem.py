from algorithms.assignment_problem import AssignmentProblem


class TravellingSalesmanProblem(object):
    def __init__(self, adjacency_matrix):
        self._initialize_record(adjacency_matrix)
        self._solve(adjacency_matrix)

    def cost(self):
        return self._record

    def cycle(self):
        return self._cycle

    def _initialize_record(self, matrix):
        n = len(matrix)
        self._cycle = [i for i in range(n)]
        self._record = 0
        for i in range(n):
            self._record += matrix[i - 1][i]

    def _solve(self, matrix):
        queue = [AssignmentProblem(matrix)]
        while len(queue) > 0:
            task = queue.pop()
            cost = task.cost()
            if cost >= self._record:
                continue
            assignment = task.assignment()
            cycle = min(self._cycles(assignment), key=lambda i: len(i))
            if len(cycle) == len(assignment):
                self._record = cost
                self._cycle = cycle
                continue
            matrix = task._costs
            for k in range(len(cycle)):
                i, j = cycle[k - 1], cycle[k]
                tmp = matrix[i][j]
                matrix[i][j] = float('inf')
                queue.append(AssignmentProblem(matrix, cost))
                matrix[i][j] = tmp

    def _cycles(self, assignment):
        queue = [i for i in range(len(assignment))]
        while len(queue) > 0:
            cycle = []
            element = queue.pop()
            cycle.append(element)
            while True:
                element = assignment[element]
                if element == cycle[0]:
                    break
                queue.remove(element)
                cycle.append(element)
            yield cycle
