def copy(matrix):
    result = matrix[:]
    for i in range(len(result)):
        result[i] = result[i][:]
    return result


def validate(matrix):
    if len(matrix) == 0 or len(matrix[0]) == 0:
        raise Exception("Matrix should not be empty.")
    column_count = len(matrix[0])
    for i in range(1, len(matrix)):
        if len(matrix[i]) != column_count:
            raise Exception("All rows should be the same length.")
