import numbers
from collections.abc import Sequence
from algorithms.utils.square_matrix import validate


class WeightedOrientedGraph(object):
    def __init__(self, nodes_amount):
        self._matrix = [[None] * nodes_amount for _ in range(nodes_amount)]

    @classmethod
    def from_adjacency_matrix(cls, adjacency_matrix, filler=0):
        validate(adjacency_matrix)
        graph = cls(len(adjacency_matrix))
        for i, row in enumerate(adjacency_matrix):
            for j, el in enumerate(row):
                if el != filler:
                    graph[i, j] = el
        return graph

    @property
    def nodes_amount(self):
        return len(self._matrix)

    def edge_exists(self, from_, to):
        self._validate_indices(from_, to)
        return self._matrix[from_][to] is not None

    def __iter__(self):
        n = self.nodes_amount
        return ((i, j) for i in range(n) for j in range(n) if self.edge_exists(i, j))

    def __getitem__(self, item):
        from_, to = self._unpack_item(item)
        if isinstance(from_, Sequence) and isinstance(to, Sequence):
            return iter(self)
        if isinstance(from_, Sequence):
            return self._incoming_edges(to, from_)
        if isinstance(to, Sequence):
            return self._outgoing_edges(from_, to)
        if not self.edge_exists(from_, to):
            raise NonexistentEdgeAccessException()
        return self._matrix[from_][to]

    def __setitem__(self, key, value):
        from_, to = self._unpack_item(key)
        if isinstance(from_, Sequence) or isinstance(to, slice):
            raise TypeError("This operation does not support slicing.")
        self._matrix[from_][to] = value

    def __delitem__(self, key):
        from_, to = self._unpack_item(key)
        if isinstance(from_, Sequence) or isinstance(to, slice):
            raise TypeError("This operation does not support slicing.")
        if not self.edge_exists(from_, to):
            import logging
            logging.warning("Removing edge ({},{}) which is non-existent.".format(from_, to))
        self._matrix[from_][to] = None

    def _outgoing_edges(self, node, choices):
        return (i for i in choices if self.edge_exists(node, i))

    def _incoming_edges(self, node, choices):
        return (i for i in choices if self.edge_exists(i, node))

    def _unpack_item(self, key):
        res = []
        for i in key:
            if isinstance(i, slice):
                res.append(range(*i.indices(self.nodes_amount)))
            elif isinstance(i, numbers.Integral):
                self._validate_indices(i)
                res.append(i)
            else:
                raise TypeError("index must be int or slice")
        return res

    def _validate_indices(self, *indices):
        for index in indices:
            if index < 0 or index >= self.nodes_amount:
                raise Exception("Index {} is out of range [0;{})".format(index, self.nodes_amount))


class NonexistentEdgeAccessException(Exception):
    """ The edge you are trying to access does not exist. """


def get_chain(graph, from_, to, predicate=lambda frm, to: True):
    def get_choices(node):
        allowed_in = [i for i in graph[:, node] if predicate(i, node)]
        allowed_out = [i for i in graph[node, :] if predicate(node, i)]
        return allowed_in + allowed_out

    visited = set()
    node_queue = []
    choices_queue = []
    node_queue.append(from_)
    choices_queue.append(get_choices(from_))
    visited.add(from_)
    while len(node_queue) > 0:
        if node_queue[-1] == to:
            return node_queue
        choices = choices_queue[-1]
        if len(choices) == 0:
            choices_queue.pop()
            node_queue.pop()
            continue
        chosen = choices.pop()
        if chosen not in visited:
            node_queue.append(chosen)
            choices_queue.append(get_choices(chosen))
            visited.add(chosen)
    raise Exception("There is no path from {} to {}.".format(from_, to))


def topological_sort(graph):
    result = []
    visited = set()
    temp = set()
    for i in range(graph.nodes_amount):
        if i not in visited:
            _visit(graph, i, visited, temp, result)
    result.reverse()
    return result


def _visit(graph, i, visited, temp, result):
    if i in temp:
        raise Exception("Sorting is impossible due to existing cycle in a graph.")
    if i not in visited:
        temp.add(i)
        for j in graph[i, :]:
            _visit(graph, j, visited, temp, result)
        temp.remove(i)
        visited.add(i)
        result.append(i)
