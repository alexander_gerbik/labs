class DisjointSet(object):
    def __init__(self, elements_amount):
        self.parent = [i for i in range(elements_amount)]
        self.rank = [0 for _ in range(elements_amount)]

    def unite(self, a, b):
        a, b = self._get_representative(a), self._get_representative(b)
        if a == b:
            return
        if self.rank[a] < self.rank[b]:
            self.parent[a] = self.parent[b]
        else:
            self.parent[b] = self.parent[a]
            if self.rank[a] == self.rank[b]:
                self.rank[a] += 1

    def is_same(self, a, b):
        return self._get_representative(a) == self._get_representative(b)

    def _get_representative(self, element):
        if element == self.parent[element]:
            return element
        self.parent[element] = self._get_representative(self.parent[element])
        return self.parent[element]
