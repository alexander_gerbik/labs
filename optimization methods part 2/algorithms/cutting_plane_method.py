import math
import random

import numpy

import sem7


def solve(a, b, c, eps=0.001):
    m, n = a.shape
    while True:
        solver = sem7.SimplexSolver(a, b, c)
        plan, basis = solver.solve()
        a, b, c, recalculate = remove_synthetic_vars(a, b, c, m, n, basis)
        if recalculate:
            continue
        i = find_violating_index(plan, basis, eps)
        if i == -1:
            return numpy.delete(plan, numpy.s_[n::], 1)
        y = get_y(a, basis, i)
        alpha, beta = get_another_restriction(y, a, b, basis)
        a, b, c = append_restriction(a, b, c, alpha, beta)


def remove_synthetic_vars(a, b, c, m, n, basis):
    if a.shape[1] < 2 * n:
        return a, b, c, False
    removed_anything = False
    for i in reversed(range(n, a.shape[1])):
        if i in basis:
            a, b, c = remove_synthetic_var(i, a, b, c, m, n)
            removed_anything = True
    return a, b, c, removed_anything


def remove_synthetic_var(i, a, b, c, m, n):
    k = i - n
    el = a[k + m, i]
    b[k + m, 0] /= el
    for j in range(a.shape[1]):
        a[k + m, j] /= el
    for j in range(a.shape[0]):
        if j != k + m:
            multiplier = - a[j, i]
            for p in range(a.shape[1]):
                a[j, p] += multiplier * a[k + m, p]
            b[j, 0] += multiplier * b[k + m, 0]
    a = numpy.delete(a, k + m, 0)
    a = numpy.delete(a, i, 1)
    b = numpy.delete(b, k + m, 0)
    c = numpy.delete(c, i, 1)
    return a, b, c


def append_restriction(a, b, c, alpha, beta):
    col = numpy.zeros((a.shape[0], 1))
    a = numpy.c_[a, col]
    a = numpy.r_[a, alpha]
    b = numpy.r_[b, [[beta]]]
    c = numpy.c_[c, [0]]
    return a, b, c


def get_another_restriction(y, a, b, basis):
    alpha = y * a
    beta = y * b
    for i in range(alpha.shape[1]):
        if i in basis:
            alpha[0, i] = 0
        else:
            alpha[0, i] = -(alpha[0, i] - math.floor(alpha[0, i]))
    beta = -(beta[0, 0] - math.floor(beta[0, 0]))
    alpha = numpy.c_[alpha, [1]]
    return alpha, beta


def get_y(a, basis, i):
    a_basis = numpy.matrix([[a[j, i] for i in basis] for j in range(a.shape[0])])
    inv_a = a_basis.I
    return inv_a[i, :]


def find_violating_index(plan, basis, eps):
    if random.random() > 0.05:
        return find_violating_index_faster(plan, basis, eps)
    return find_violating_index_stable(plan, basis, eps)


def find_violating_index_faster(plan, basis, eps):
    basis.sort()
    res = -1
    diff = 0
    for i, j in enumerate(basis):
        current = math.fabs(plan[0, j] - round(plan[0, j]))
        if current > eps and current > diff:
            res = i
            diff = current
    return res


def find_violating_index_stable(plan, basis, eps):
    basis.sort()
    for i, j in enumerate(basis):
        if not math.isclose(plan[0, j], round(plan[0, j]), abs_tol=eps):
            return i
    return -1
