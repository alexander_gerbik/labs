from algorithms.utils.graph import WeightedOrientedGraph


class ShortestPathTree(object):
    def __init__(self, graph: WeightedOrientedGraph, source_node):
        self._graph = graph
        self.source_node = source_node
        self._calculate_paths()

    def get_shortest_path_to(self, node):
        path = [node]
        while node != self.source_node:
            node = self._previous_node[node]
            path.append(node)
        path.reverse()
        return path

    def get_min_distance_to(self, node):
        return self._distance[node]

    def _calculate_paths(self):
        visited = set()
        self._distance = [float('inf')] * self._graph.nodes_amount
        self._distance[self.source_node] = 0
        self._previous_node = [-1] * self._graph.nodes_amount
        queue = [i for i in range(self._graph.nodes_amount)]
        while len(queue) > 0:
            u = min(queue, key=lambda i: self._distance[i])
            queue.remove(u)
            visited.add(u)
            for i in self._graph[u, :]:
                if i not in visited:
                    alternative_distance = self._distance[u] + self._graph[u, i]
                    if alternative_distance < self._distance[i]:
                        self._distance[i] = alternative_distance
                        self._previous_node[i] = u
