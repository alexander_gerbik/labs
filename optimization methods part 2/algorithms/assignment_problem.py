import copy
from algorithms.max_flow import MaxFlow
from algorithms.utils.graph import WeightedOrientedGraph
from algorithms.utils.square_matrix import validate


class AssignmentProblem(object):
    def __init__(self, costs, necessary_cost=0):
        validate(costs)
        self._costs = copy.deepcopy(costs)
        self._n = len(self._costs)
        self._assignment = [-1 for _ in range(self._n)]
        self._necessary_cost = necessary_cost
        self._solve()

    def assignment(self):
        return self._assignment

    def cost(self):
        return self._necessary_cost

    def _solve(self):
        self._subtract_min_elements()
        while True:
            network = self._get_network_for_max_matching()
            matching_amount, flow = self._solve_max_flow(network)
            if matching_amount == self._n:
                break
            not_marked_rows, marked_columns = self._mark(flow, network)
            alpha = min(self._unmarked_elements(not_marked_rows, marked_columns))
            self._change_costs(not_marked_rows, marked_columns, alpha)
        self._compose_assignment(flow)

    def _subtract_min_elements(self):
        for i in range(self._n):
            row_min = float('inf')
            for j in range(self._n):
                if self._costs[i][j] < row_min:
                    row_min = self._costs[i][j]
            self._necessary_cost += row_min
            for j in range(self._n):
                self._costs[i][j] -= row_min
        for j in range(self._n):
            col_min = float('inf')
            for i in range(self._n):
                if self._costs[i][j] < col_min:
                    col_min = self._costs[i][j]
            self._necessary_cost += col_min
            for i in range(self._n):
                self._costs[i][j] -= col_min

    def _get_network_for_max_matching(self):
        network_size = 2 * self._n + 2
        adjacency_matrix = []
        for i in range(network_size):
            adjacency_matrix.append([0 for _ in range(network_size)])
        for i in range(self._n):
            adjacency_matrix[0][i + 1] = 1
            adjacency_matrix[i + self._n + 1][network_size - 1] = 1
        for i in range(self._n):
            for j in range(self._n):
                if self._costs[i][j] == 0:
                    adjacency_matrix[i + 1][j + self._n + 1] = float('inf')
        network = WeightedOrientedGraph.from_adjacency_matrix(adjacency_matrix)
        return network

    def _solve_max_flow(self, network):
        solver = MaxFlow(network, 0, network.nodes_amount - 1)
        flow = solver.flow()
        flow_value = 0
        for i in flow[0, :]:
            flow_value += flow[0, i]
        return flow_value, flow

    def _mark(self, flow, network):
        # algorithm is described there
        # https://en.wikipedia.org/wiki/K%C5%91nig%27s_theorem_(graph_theory)#Proof
        not_marked_rows, marked_columns = [], []
        queue = []
        for i in network[0, :]:
            if not flow.edge_exists(0, i):
                queue.append(i)
        while len(queue) > 0:
            node = queue.pop()
            if node <= self._n:
                j = node - 1
                if j not in not_marked_rows:
                    not_marked_rows.append(j)
                    for i in network[node, :]:
                        if not flow.edge_exists(node, i):
                            queue.append(i)
            else:
                j = node - 1 - self._n
                if j not in marked_columns:
                    marked_columns.append(j)
                    for i in flow[:, node]:
                        queue.append(i)
        return not_marked_rows, marked_columns

    def _unmarked_elements(self, not_marked_rows, marked_columns):
        for i in not_marked_rows:
            for j in range(self._n):
                if j not in marked_columns:
                    yield self._costs[i][j]

    def _change_costs(self, not_marked_rows, marked_columns, alpha):
        self._necessary_cost += (len(not_marked_rows) - len(marked_columns)) * alpha
        for i in not_marked_rows:
            for j in range(self._n):
                self._costs[i][j] -= alpha
        for i in range(self._n):
            for j in marked_columns:
                self._costs[i][j] += alpha

    def _compose_assignment(self, flow):
        for i, j in flow:
            if i != 0 and j != 2 * self._n + 1:
                self._assignment[i - 1] = j - 1 - self._n

