from .utils.graph import NonexistentEdgeAccessException, get_chain
from .utils.disjoint_set import DisjointSet


class ConstrainedMinCostFlow(object):
    def __init__(self, costs, a, capacities):
        self.costs = costs
        self.a = a
        self.capacities = capacities

    def solve(self, flows):
        self.basis = self.get_basis(flows)
        while True:
            potentials = self.calculate_potentials()
            add_candidates = self.get_add_candidates(potentials, flows)
            while True:
                if not add_candidates:
                    return flows
                add_candidate, inverse = add_candidates.pop()
                if inverse:
                    path = get_chain(flows, add_candidate[0], add_candidate[1], self.is_edge_basis)
                else:
                    path = get_chain(flows, add_candidate[1], add_candidate[0], self.is_edge_basis)
                theta, remove_candidate = self.get_theta(flows, path)
                if remove_candidate is None:
                    raise Exception("Objective function is unbounded")
                self.change_flows(flows, path, theta, add_candidate, remove_candidate)
                if add_candidates != remove_candidate:
                    self.basis.remove(remove_candidate)
                    self.basis.append(add_candidate)
                    break

    def get_basis(self, flows):
        n = self.costs.nodes_amount
        ds = DisjointSet(n)
        n -= 1
        basis = []
        not_basis = []
        for i, j in flows:
            if flows[i, j] > 0 and flows[i, j] < self.capacities[i, j]:
                basis.append((i, j))
                ds.unite(i, j)
            else:
                not_basis.append((i, j))
        for i, j in not_basis:
            if not ds.is_same(i, j):
                basis.append((i, j))
                if len(basis) == n:
                    break
        if len(basis) != n:
            raise Exception("Cannot construct basis")
        return basis

    def calculate_potentials(self):
        # u0 = 0
        # ui - uj = cij, i->j
        potentials = {0: 0}
        queue = [0]
        while len(queue) != 0:
            current_node = queue.pop()
            for i, j in self.basis:
                try:
                    weight = self.costs[i, j]
                except NonexistentEdgeAccessException:
                    weight = 0
                if j not in potentials and i == current_node:
                    potentials[j] = potentials[current_node] - weight
                    queue.append(j)
                if i not in potentials and j == current_node:
                    potentials[i] = potentials[current_node] + weight
                    queue.append(i)
        return potentials

    def get_add_candidates(self, potentials, flows):
        result = []
        for i, j in self.costs:
            if not flows.edge_exists(i, j):
                assessment = potentials[i] - potentials[j] - self.costs[i, j]
                if assessment > 0:
                    result.append(((i, j), False))
            elif flows[i, j] == self.capacities[i, j]:
                assessment = potentials[i] - potentials[j] - self.costs[i, j]
                if assessment < 0:
                    result.append(((i, j), True))
        return result

    def get_theta(self, flows, path):
        theta = float('inf')
        remove_candidate = None
        for i in range(len(path)):
            a, b = path[i - 1], path[i]
            if self.is_edge_inverse(flows, a, b):
                if flows[b, a] < theta:
                    theta = flows[b, a]
                    remove_candidate = (b, a)
            else:
                try:
                    flow = flows[a, b]
                except NonexistentEdgeAccessException:
                    flow = 0
                alt_theta = self.capacities[a, b] - flow
                if alt_theta < theta:
                    theta = alt_theta
                    remove_candidate = (a, b)
        return theta, remove_candidate

    def change_flows(self, flows, path, theta, add_candidate, remove_candidate):
        for i in range(len(path) - 1):
            a, b = path[i], path[i + 1]
            if self.is_edge_inverse(flows, a, b):
                flows[b, a] -= theta
                if flows[b, a] == 0 and (b, a) == remove_candidate:
                    del flows[b, a]
            else:
                flows[a, b] += theta
        if flows.edge_exists(add_candidate[0], add_candidate[1]):
            flows[add_candidate[0], add_candidate[1]] -= theta
        else:
            flows[add_candidate[0], add_candidate[1]] = theta

    def is_edge_basis(self, i, j):
        return (i, j) in self.basis

    def is_edge_inverse(self, flows, a, b):
        return flows.edge_exists(b, a)
