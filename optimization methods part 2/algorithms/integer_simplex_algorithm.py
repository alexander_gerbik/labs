import math

from algorithms import dual_simplex_first_stage, dual_simplex_algorithm


class TaskData(object):
    def __init__(self, lower, upper, plan, basis, result):
        self.lower = lower
        self.upper = upper
        self.plan = plan
        self.basis = basis
        self.result = result

    def solve_sub_task(self, main_task, k, eps):
        lower_a = self.lower.copy()
        upper_a = self.upper.copy()
        upper_a[0, k] = math.floor(self.plan[0, k])
        try:
            plan_a, basis_a = dual_simplex_algorithm.solve(main_task.a, main_task.b, main_task.c, lower_a, upper_a,
                                                           self.basis, eps)
            result_a = (plan_a * main_task.c.T)[0, 0]
            task_a = TaskData(lower_a, upper_a, plan_a, self.basis[:], result_a)
        except dual_simplex_algorithm.FeasibleRegionIsEmptyError:
            task_a = None
        lower_b = self.lower.copy()
        lower_b[0, k] = math.ceil(self.plan[0, k])
        upper_b = self.upper.copy()
        try:
            plan_b, basis_b = dual_simplex_algorithm.solve(main_task.a, main_task.b, main_task.c, lower_b, upper_b,
                                                           self.basis[:], eps)
            result_b = (plan_b * main_task.c.T)[0, 0]
            task_b = TaskData(lower_b, upper_b, plan_b, self.basis, result_b)
        except dual_simplex_algorithm.FeasibleRegionIsEmptyError:
            task_b = None
        return task_a, task_b


class IntegerSimplexAlgorithm(object):
    def __init__(self, a, b, c, lower, upper, integer_indexes, eps):
        self.a = a
        self.b = b
        self.c = c
        self.lower = lower
        self.upper = upper
        self.integer_indexes = integer_indexes
        self.record = float('-inf')
        self.plan = None
        self.eps = eps

    def solve(self):
        basis = dual_simplex_first_stage.find_initial_basis_set(self.a)
        plan, basis = dual_simplex_algorithm.solve(self.a, self.b, self.c, self.lower, self.upper, basis, self.eps)
        result = (plan * self.c.T)[0, 0]
        task_data = TaskData(self.lower, self.upper, plan, basis, result)
        self._branch_and_bound(task_data)
        if not math.isfinite(self.record):
            raise dual_simplex_algorithm.FeasibleRegionIsEmptyError
        return self.plan, self.record

    def _branch_and_bound(self, first_task):
        tasks = [first_task]
        while len(tasks) != 0:
            task = tasks.pop()
            if task.result <= self.record:
                continue
            k = self._find_violating_index(task.plan)
            if k == -1:
                self.record = task.result
                self.plan = task.plan
                continue
            task_a, task_b = task.solve_sub_task(self, k, self.eps)
            if task_a is None:
                if task_b is None:
                    continue
                tasks.append(task_b)
            elif task_b is None:
                tasks.append(task_a)
            elif task_b.result > task_a.result:
                tasks.append(task_a)
                tasks.append(task_b)
            else:
                tasks.append(task_b)
                tasks.append(task_a)

    def _find_violating_index(self, plan):
        for i in self.integer_indexes:
            if not math.isclose(plan[0, i], round(plan[0, i]), abs_tol=self.eps):
                return i
        return -1
