import itertools

import numpy as np

from algorithms import dual_simplex_algorithm


def find_initial_basis_set(a):
    rows, cols = a.shape
    comb = itertools.combinations(range(cols), rows)
    for c in comb:
        lst = list(c)
        a_basis = np.matrix([[a[j, i] for i in lst] for j in range(rows)])
        try:
            np.linalg.inv(a_basis)
            return lst
        except np.linalg.LinAlgError:
            pass
    raise dual_simplex_algorithm.FeasibleRegionIsEmptyError()
