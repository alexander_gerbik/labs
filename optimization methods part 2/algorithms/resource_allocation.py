from algorithms.utils.square_matrix import validate


class ResourceAllocator(object):
    def __init__(self, profits):
        validate(profits)
        self.records = []
        self.distributions = []
        self.customer_amount = 0
        self.resource_amount = len(profits[0])
        for customer in profits:
            self._append_customer(customer)

    def solve(self, resource_amount, customer_amount):
        if resource_amount > self.resource_amount or customer_amount > self.customer_amount:
            raise Exception("This value is not calculated.")
        value = self.records[customer_amount - 1][resource_amount]
        distribution = [0] * customer_amount
        for i in reversed(range(customer_amount)):
            distribution[i] = self.distributions[i][resource_amount]
            resource_amount -= self.distributions[i][resource_amount]
        return distribution, value

    def _append_customer(self, customer):
        assert len(customer) >= self.resource_amount
        if self.customer_amount == 0:
            distribution, record = self._create_first_customer(customer)
        else:
            distribution, record = self._create_another_customer(customer)
        self.distributions.append(distribution)
        self.records.append(record)
        self.customer_amount += 1

    def _create_first_customer(self, customer):
        record = [0] * (self.resource_amount + 1)
        distribution = [0] * (self.resource_amount + 1)
        for i in range(self.resource_amount):
            distribution[i + 1] = i + 1
            record[i + 1] = customer[i]
        return distribution, record

    def _create_another_customer(self, customer):
        record = [0] * (self.resource_amount + 1)
        distribution = [0] * (self.resource_amount + 1)
        for i in range(self.resource_amount + 1):
            # assuming we have no negative profits, otherwise should be -inf
            max_profit = 0
            gain = 0
            for j in range(0, i + 1):
                current_profit = 0 if j == 0 else customer[j - 1]
                current_profit += self.records[-1][i - j]
                if current_profit > max_profit:
                    max_profit = current_profit
                    gain = j
            distribution[i] = gain
            record[i] = max_profit
        return distribution, record

