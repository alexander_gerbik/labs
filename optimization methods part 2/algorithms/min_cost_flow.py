from .utils.graph import get_chain


class MinCostFlow(object):
    def __init__(self, graph, a):
        self.graph = graph
        self.a = a

    def solve(self, basis_tree):
        while True:
            potentials = self.calculate_potentials(basis_tree)
            edge = self.get_edge_with_positive_assessment(potentials, basis_tree)
            if edge is None:
                break
            path = get_chain(basis_tree, edge[1], edge[0])
            theta, remove_candidate = self.get_theta(basis_tree, path)
            if remove_candidate is None:
                raise Exception("Objective function is unbounded")
            self.change_basis(basis_tree, path, theta, edge, remove_candidate)
        return basis_tree

    def calculate_potentials(self, tree):
        # u0 = 0
        # ui - uj = cij, i->j
        potentials = {0: 0}
        queue = [0]
        while len(queue) != 0:
            current_node = queue.pop()
            for j in tree[current_node, :]:
                if j not in potentials:
                    potentials[j] = potentials[current_node] - self.graph[current_node, j]
                    queue.append(j)
            for i in tree[:, current_node]:
                if i not in potentials:
                    potentials[i] = potentials[current_node] + self.graph[i, current_node]
                    queue.append(i)
        return potentials

    def get_edge_with_positive_assessment(self, potentials, basis_tree):
        for i, j in self.graph:
            if not basis_tree.edge_exists(i, j):
                assessment = potentials[i] - potentials[j] - self.graph[i, j]
                if assessment > 0:
                    return i, j
        return None

    def get_theta(self, basis_tree, path):
        theta = float('inf')
        edge = None
        for i in range(len(path) - 1):
            a, b = path[i], path[i + 1]
            if is_edge_inverse(basis_tree, a, b) and basis_tree[b, a] < theta:
                theta = basis_tree[b, a]
                edge = (b, a)
        return theta, edge

    def change_basis(self, basis, path, theta, add_candidate, remove_candidate):
        for i in range(len(path) - 1):
            a, b = path[i], path[i + 1]
            if is_edge_inverse(basis, a, b):
                basis[b, a] -= theta
            else:
                basis[a, b] += theta
        del basis[remove_candidate[0], remove_candidate[1]]
        basis[add_candidate[0], add_candidate[1]] = theta


def is_edge_inverse(graph, a, b):
    return graph.edge_exists(b, a)

