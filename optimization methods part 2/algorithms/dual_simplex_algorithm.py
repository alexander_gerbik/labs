import sem7.inverseMatrix as inverseMatrix
import numpy as np


class FeasibleRegionIsEmptyError(Exception):
    def __init__(self):
        pass


def solve(a, b, c, lower, upper, basis, eps):
    m, n = a.shape
    a_basis = np.matrix([[a[j, i] for i in basis] for j in range(m)])
    inv_a = inverseMatrix.get_inverse_matrix(a_basis, eps)
    c_basis = np.matrix([[c[j, i] for i in basis] for j in range(c.shape[0])])
    deltas = c_basis * inv_a * a - c
    j_pos = [i for i in range(n) if deltas[0, i] >= 0 and i not in basis]
    j_neg = [i for i in range(n) if deltas[0, i] < 0 and i not in basis]
    while True:
        plan = get_plan(basis, j_pos, j_neg, a, b, lower, upper, inv_a, n)
        nu, k = find_jk(basis, plan, lower, upper, eps)
        if nu == 0:
            return plan, basis
        delta_y = nu * inv_a[k, :]
        nus_pos, nus_neg = get_nus(delta_y, a, j_pos, j_neg)
        sigma, j_ast = get_sigma(j_pos, j_neg, nus_pos, nus_neg, deltas)
        if sigma == float('inf'):
            raise FeasibleRegionIsEmptyError()
        change_deltas(deltas, sigma, nus_pos, nus_neg, nu, basis, j_pos, j_neg, k)
        jk = basis[k]
        basis[k] = j_ast
        try:
            inv_a = inverseMatrix.get_new_inverse_matrix(inv_a, k, a[:, j_ast], eps)
        except inverseMatrix.DeterminantIsZeroException:
            raise FeasibleRegionIsEmptyError()
        if j_ast in j_pos:
            j_pos.remove(j_ast)
        if j_ast in j_neg:
            j_neg.remove(j_ast)
        if nu == 1:
            j_pos.append(jk)
        else:
            j_neg.append(jk)


def get_plan(basis, j_pos, j_neg, a, b, lower, upper, inv_a, n):
    result = np.matrix(np.zeros((1, n)))
    tmp = b.copy()
    for j in j_pos:
        result[0, j] = lower[0, j]
        tmp -= a[:, j] * result[0, j]
    for j in j_neg:
        result[0, j] = upper[0, j]
        tmp -= a[:, j] * result[0, j]
    tmp = inv_a * tmp
    for i, j in enumerate(basis):
        result[0, j] = tmp[i, 0]
    return result


def find_jk(basis, plan, lower, upper, eps):
    for k, j in enumerate(basis):
        if plan[0, j] < lower[0, j] - eps:
            return 1, k
        if plan[0, j] > upper[0, j] + eps:
            return -1, k
    return 0, 0


def get_nus(delta_y, a, j_pos, j_neg):
    m, _ = a.shape
    a_pos = np.matrix([[a[j, i] for i in j_pos] for j in range(m)])
    a_neg = np.matrix([[a[j, i] for i in j_neg] for j in range(m)])
    return delta_y * a_pos, delta_y * a_neg


def get_sigma(j_pos, j_neg, nu_pos, nu_neg, deltas):
    sigma = float('inf')
    j_ast = -1
    for i, j in enumerate(j_pos):
        if nu_pos[0, i] < 0:
            current = - deltas[0, j] / nu_pos[0, i]
            if current < sigma:
                sigma = current
                j_ast = j
    for i, j in enumerate(j_neg):
        if nu_neg[0, i] > 0:
            current = - deltas[0, j] / nu_neg[0, i]
            if current < sigma:
                sigma = current
                j_ast = j
    return sigma, j_ast


def change_deltas(deltas, sigma, nus_pos, nus_neg, nu, basis, j_pos, j_neg, k):
    for i, j in enumerate(j_pos):
        deltas[0, j] += sigma * nus_pos[0, i]
    for i, j in enumerate(j_neg):
        deltas[0, j] += sigma * nus_neg[0, i]
    deltas[0, basis[k]] += sigma * nu
    for j in basis:
        if j != basis[k]:
            deltas[0, j] = 0
