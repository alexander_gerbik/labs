from algorithms.utils.graph import WeightedOrientedGraph, topological_sort


class LongestPathTree(object):
    def __init__(self, graph: WeightedOrientedGraph):
        self._graph = graph
        self._calculate_paths()

    def get_longest_path_to(self, node):
        path = [node]
        while node != self._previous_node[node]:
            node = self._previous_node[node]
            path.append(node)
        path.reverse()
        return path

    def get_max_distance_to(self, node):
        return self._distance[node]

    def _calculate_paths(self):
        ordered_nodes = topological_sort(self._graph)
        source_node = ordered_nodes[0]
        self._previous_node = [-1] * self._graph.nodes_amount
        self._previous_node[source_node] = source_node
        self._distance = [float('-inf')] * self._graph.nodes_amount
        self._distance[source_node] = 0
        for k in range(1, len(ordered_nodes)):
            i = ordered_nodes[k]
            for j in self._graph[:, i]:
                alt_distance = self._distance[j] + self._graph[j, i]
                if self._distance[i] < alt_distance:
                    self._distance[i] = alt_distance
                    self._previous_node[i] = j
