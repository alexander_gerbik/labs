import copy


class ShortestPath(object):
    def __init__(self, adjacency_matrix):
        self._distance = copy.deepcopy(adjacency_matrix)
        self._initialize_next()
        self._calculate_paths()

    def _initialize_next(self):
        n = len(self._distance)
        self._next = []
        for i in range(n):
            self._next.append([j for j in range(n)])

    def get_shortest_path(self, from_, to):
        result = [from_]
        while from_ != to:
            from_ = self._next[from_][to]
            result.append(from_)
        return result

    def get_min_distance(self, from_, to):
        return self._distance[from_][to]

    def _calculate_paths(self):
        n = len(self._distance)
        for k in range(n):
            for i in range(n):
                for j in range(n):
                    alt_distance = self._distance[i][k] + self._distance[k][j]
                    if alt_distance < self._distance[i][j]:
                        self._distance[i][j] = alt_distance
                        self._next[i][j] = self._next[i][k]
