import itertools
from algorithms.utils.graph import WeightedOrientedGraph


class MaxFlow(object):
    def __init__(self, graph: WeightedOrientedGraph, source, sink):
        self._graph = graph
        self._source = source
        self._sink = sink
        self._initialize_flows()
        self._solve()

    def _initialize_flows(self):
        adjacency_matrix = []
        for _ in range(self._graph.nodes_amount):
            tmp = [0] * self._graph.nodes_amount
            adjacency_matrix.append(tmp)
        self._flows = WeightedOrientedGraph.from_adjacency_matrix(adjacency_matrix)

    def flow(self):
        return self._flows

    def _solve(self):
        while True:
            path = self._find_augmenting_path()
            if path is None:
                break
            self._reallocate_flows(path)

    def _find_augmenting_path(self):
        parent = [-1] * self._graph.nodes_amount
        parent[self._source] = self._source
        queue = [self._source]
        while len(queue) > 0:
            i = queue.pop(0)
            if i == self._sink:
                break
            for j in itertools.chain(self._graph[i, :], self._graph[:, i]):
                if parent[j] == -1 and self._get_leftover_capacity(i, j) > 0:
                    parent[j] = i
                    queue.append(j)
        else:
            return None
        node = self._sink
        result = [node]
        while node != self._source:
            node = parent[node]
            result.append(node)
        result.reverse()
        return result

    def _reallocate_flows(self, path):
        increment = float('inf')
        for k in range(len(path) - 1):
            capacity = self._get_leftover_capacity(path[k], path[k + 1])
            increment = min(increment, capacity)
        for k in range(len(path) - 1):
            self._fill_leftover_capacity(path[k], path[k + 1], increment)

    def _fill_leftover_capacity(self, i, j, increment):
        if self._graph.edge_exists(i, j):
            if self._flows.edge_exists(i, j):
                self._flows[i, j] += increment
            else:
                self._flows[i, j] = increment
            return
        if self._graph.edge_exists(j, i):
            self._flows[j, i] -= increment
            if self._flows[j, i] == 0:
                del self._flows[j, i]
            return
        raise Exception("Trying to increase flow through nonexistent edge.")

    def _get_leftover_capacity(self, i, j):
        if self._graph.edge_exists(i, j):
            if self._flows.edge_exists(i, j):
                return self._graph[i, j] - self._flows[i, j]
            return self._graph[i, j]
        if self._flows.edge_exists(j, i):
            return self._flows[j, i]
        return 0
