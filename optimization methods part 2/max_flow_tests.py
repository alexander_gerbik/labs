import unittest

from algorithms.max_flow import MaxFlow
from algorithms.utils.graph import WeightedOrientedGraph


class MaxFlowTests(unittest.TestCase):
    def test_example1(self):
        paths = [[0, 4, 0, 9, 0, 0, 0],
                 [0, 0, 0, 2, 4, 0, 0],
                 [0, 0, 0, 0, 1, 10, 0],
                 [0, 0, 1, 0, 0, 6, 0],
                 [0, 0, 0, 0, 0, 1, 2],
                 [0, 0, 0, 0, 0, 0, 9],
                 [0, 0, 0, 0, 0, 0, 0]]
        true_result = 10
        result = solve(paths)
        self.assertEqual(result, true_result)

    def test_example2(self):
        paths = [[0, 12, 0, 0, 0, 6, 0],
                 [0, 0, 2, 0, 0, 0, 0],
                 [0, 0, 0, 0, 5, 0, 1],
                 [0, 2, 6, 0, 0, 0, 0],
                 [0, 0, 0, 2, 0, 0, 15],
                 [0, 10, 0, 8, 5, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0]]
        true_result = 8
        result = solve(paths)
        self.assertEqual(result, true_result)

    def test_task1(self):
        paths = [[0, 3, 2, 1, 0, 6, 0, 0],
                 [0, 0, 0, 1, 2, 0, 0, 0],
                 [0, 0, 0, 1, 2, 4, 0, 0],
                 [0, 0, 0, 0, 7, 5, 4, 1],
                 [0, 0, 0, 0, 0, 0, 3, 2],
                 [0, 0, 0, 0, 0, 0, 0, 4],
                 [0, 0, 0, 0, 0, 3, 0, 5],
                 [0, 0, 0, 0, 0, 0, 0, 0]]
        true_result = 10
        result = solve(paths)
        self.assertEqual(result, true_result)

    def test_task2(self):
        paths = [[0, 4, 1, 0, 1, 5, 2, 0, 0],
                 [0, 0, 0, 1, 0, 6, 0, 0, 0],
                 [0, 0, 0, 0, 2, 0, 0, 0, 0],
                 [0, 0, 6, 0, 0, 0, 3, 0, 0],
                 [0, 0, 0, 0, 0, 4, 0, 3, 0],
                 [0, 0, 0, 0, 0, 0, 1, 3, 6],
                 [0, 0, 0, 0, 0, 0, 0, 4, 5],
                 [0, 0, 0, 0, 0, 0, 0, 0, 4],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0]]
        true_result = 13
        result = solve(paths)
        self.assertEqual(result, true_result)

    def test_task3(self):
        paths = [[0, 2, 1, 0, 2, 0, 0, 0, 0, 0],
                 [0, 0, 0, 2, 1, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 3, 2, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 1, 2, 0, 0],
                 [0, 0, 0, 0, 0, 1, 4, 3, 3, 0],
                 [0, 0, 0, 0, 0, 0, 0, 2, 2, 0],
                 [0, 0, 0, 0, 0, 0, 0, 5, 0, 3],
                 [0, 0, 0, 0, 0, 0, 0, 0, 1, 4],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
        true_result = 5
        result = solve(paths)
        self.assertEqual(result, true_result)

    def test_task4(self):
        paths = [[0, 3, 6, 3, 2, 0, 0, 0],
                 [0, 0, 4, 1, 4, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 3, 2],
                 [0, 0, 1, 0, 5, 0, 1, 2],
                 [0, 0, 0, 0, 0, 1, 0, 0],
                 [0, 0, 0, 3, 0, 0, 1, 0],
                 [0, 0, 0, 0, 0, 0, 0, 4],
                 [0, 0, 0, 0, 0, 0, 0, 0]]
        true_result = 8
        result = solve(paths)
        self.assertEqual(result, true_result)

    def test_task5(self):
        paths = [[0, 2, 5, 0, 3, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0],
                 [0, 4, 0, 0, 1, 0, 0, 3, 1],
                 [0, 0, 2, 0, 2, 4, 5, 0, 0],
                 [0, 4, 0, 0, 0, 4, 0, 0, 5],
                 [0, 0, 0, 0, 0, 0, 1, 0, 0],
                 [0, 0, 0, 0, 1, 0, 0, 4, 0],
                 [0, 0, 0, 0, 3, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 2, 0]]
        true_result = 6
        result = solve(paths)
        self.assertEqual(result, true_result)

    def test_task6(self):
        paths = [[0, 0, 0, 3, 6, 0, 0, 0],
                 [1, 0, 0, 4, 7, 0, 1, 0],
                 [5, 5, 0, 0, 0, 0, 0, 0],
                 [0, 0, 1, 0, 2, 2, 1, 3],
                 [0, 0, 0, 0, 0, 4, 3, 0],
                 [0, 0, 5, 0, 0, 0, 0, 2],
                 [0, 0, 0, 0, 0, 7, 0, 0],
                 [0, 0, 4, 0, 0, 0, 0, 0]]
        true_result = 5
        result = solve(paths)
        self.assertEqual(result, true_result)

    def test_task7(self):
        paths = [[0, 0, 0, 0, 4, 6, 2, 0, 0],
                 [3, 0, 2, 0, 7, 0, 4, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0],
                 [0, 3, 5, 0, 2, 0, 4, 0, 2],
                 [0, 0, 0, 0, 0, 1, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, 7],
                 [0, 0, 0, 0, 0, 1, 0, 0, 0],
                 [0, 0, 3, 6, 0, 0, 4, 0, 1],
                 [0, 0, 1, 0, 0, 0, 0, 0, 0]]
        true_result = 7
        result = solve(paths)
        self.assertEqual(result, true_result)

    def test_task8(self):
        paths = [[0, 3, 3, 0, 0, 2, 1, 0, 0, 0],
                 [0, 0, 6, 0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 1, 2, 4, 4, 0, 0, 0],
                 [0, 0, 0, 0, 7, 0, 0, 0, 0, 4],
                 [0, 0, 0, 0, 0, 0, 1, 0, 1, 1],
                 [0, 0, 0, 4, 7, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
                 [2, 0, 0, 0, 0, 5, 6, 0, 2, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 5],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
        true_result = 9
        result = solve(paths)
        self.assertEqual(result, true_result)


def solve(paths):
    graph = WeightedOrientedGraph.from_adjacency_matrix(paths)
    solver = MaxFlow(graph, 0, graph.nodes_amount - 1)
    flow = solver.flow()
    return sum(flow[0, i] for i in flow[0, :])


if __name__ == "__main__":
    unittest.main()
