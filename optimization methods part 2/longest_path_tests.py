import unittest

from algorithms.longest_path_tree import LongestPathTree
from algorithms.utils.graph import WeightedOrientedGraph


class LongestPathTreeTests(unittest.TestCase):
    def test_example(self):
        paths = [[0, 2, 0, 1, 0, 0],
                 [0, 0, 2, 0, 7, 0],
                 [0, 0, 0, 0, 0, 8],
                 [0, 4, 4, 0, 1, 0],
                 [0, 0, 1, 0, 0, 1],
                 [0, 0, 0, 0, 0, 0]]
        expected = 21
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task1(self):
        paths = [[0, 5, 6, 4, 1, 0, 0, 0],
                 [0, 0, 4, 3, 2, 0, 0, 0],
                 [0, 0, 0, 0, 5, 0, 3, 0],
                 [0, 0, 0, 0, 0, 4, 7, 3],
                 [0, 0, 0, 0, 0, 0, 0, 4],
                 [0, 0, 0, 0, 0, 0, 2, 5],
                 [0, 0, 0, 0, 2, 0, 0, 1],
                 [0, 0, 0, 0, 0, 0, 0, 0]]
        expected = 21
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task2(self):
        paths = [[0, 3, 4, 5, 3, 0, 0],
                 [0, 0, 0, 2, 0, 0, 0],
                 [0, 0, 0, 6, 0, 3, 0],
                 [0, 0, 0, 0, 4, 1, 4],
                 [0, 0, 0, 0, 0, 2, 5],
                 [0, 0, 0, 0, 0, 0, 1],
                 [0, 0, 0, 0, 0, 0, 0]]
        expected = 19
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task3(self):
        paths = [[0, 4, 1, 3, 0, 2, 7, 0],
                 [0, 0, 1, 5, 0, 0, 0, 0],
                 [0, 0, 0, 4, 3, 5, 0, 0],
                 [0, 0, 0, 0, 2, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 3, 1],
                 [0, 0, 0, 4, 0, 0, 2, 7],
                 [0, 0, 0, 0, 0, 0, 0, 6],
                 [0, 0, 0, 0, 0, 0, 0, 0]]
        expected = 25
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task4(self):
        paths = [[0, 3, 4, 6, 2, 0, 0, 0],
                 [0, 0, 0, 5, 1, 0, 0, 0],
                 [0, 3, 0, 2, 0, 6, 0, 0],
                 [0, 0, 0, 0, 4, 2, 7, 0],
                 [0, 0, 0, 0, 0, 3, 7, 1],
                 [0, 0, 0, 0, 0, 0, 1, 4],
                 [0, 0, 0, 0, 0, 0, 0, 6],
                 [0, 0, 0, 0, 0, 0, 0, 0]]
        expected = 29
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task5(self):
        paths = [[0, 7, 9, 6, 0, 3, 0],
                 [0, 0, 0, 0, 0, 6, 0],
                 [0, 4, 0, 0, 3, 1, 4],
                 [0, 2, 1, 0, 8, 0, 0],
                 [0, 0, 0, 0, 0, 5, 1],
                 [0, 0, 0, 0, 0, 0, 3],
                 [0, 0, 0, 0, 0, 0, 0]]
        expected = 22
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task6(self):
        paths = [[0, 6, 5, 0, 1, 4, 0, 0, 0],
                 [0, 0, 2, 0, 9, 3, 0, 0, 0],
                 [0, 0, 0, 10, 1, 0, 2, 0, 5],
                 [0, 0, 0, 0, 0, 0, 1, 7, 3],
                 [0, 0, 0, 7, 0, 6, 3, 0, 0],
                 [0, 0, 0, 5, 0, 0, 1, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 8, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, 2],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0]]
        expected = 37
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task7(self):
        paths = [[0, 7, 0, 4, 4, 0, 0, 0, 0],
                 [0, 0, 2, 5, 0, 0, 0, 0, 0],
                 [0, 0, 0, 6, 0, 1, 0, 7, 0],
                 [0, 0, 0, 0, 7, 4, 0, 0, 0],
                 [0, 0, 0, 0, 0, 9, 3, 0, 0],
                 [0, 0, 0, 0, 0, 0, 10, 0, 5],
                 [0, 0, 0, 0, 0, 0, 0, 0, 8],
                 [0, 0, 0, 0, 0, 0, 0, 0, 3],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0]]
        expected = 49
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task8(self):
        paths = [[0, 7, 2, 1, 0, 0, 0, 0, 0],
                 [0, 0, 0, 9, 5, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 4, 3, 0, 0],
                 [0, 0, 4, 0, 3, 5, 0, 7, 0],
                 [0, 0, 0, 0, 0, 10, 0, 4, 0],
                 [0, 0, 0, 0, 0, 0, 7, 0, 4],
                 [0, 0, 0, 0, 0, 0, 0, 0, 6],
                 [0, 0, 0, 0, 0, 8, 0, 0, 1],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0]]
        expected = 44
        result = solve(paths)
        self.assertEqual(result, expected)


def solve(paths):
    graph = WeightedOrientedGraph.from_adjacency_matrix(paths)
    solver = LongestPathTree(graph)
    result = solver.get_max_distance_to(len(paths) - 1)
    return result


if __name__ == "__main__":
    unittest.main()
