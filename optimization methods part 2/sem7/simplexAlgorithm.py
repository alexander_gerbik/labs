import math

import numpy as np

import sem7.inverseMatrix


class ObjectiveFunctionIsUnboundedError(Exception):
    def __init__(self):
        pass


class FeasibleRegionIsEmptyError(Exception):
    def __init__(self):
        pass


class SimplexSolver(object):
    def __init__(self, a, b, c, eps=0.001):
        self.eps = eps
        self.m, self.n = a.shape
        assert b.shape[0] == self.m and b.shape[1] == 1, "Vector B should have as much rows as matrix A does"
        assert c.shape[0] == 1 and c.shape[1] == self.n, "Vector C should have as much columns as matrix A does"
        self.a = np.matrix(np.copy(a))
        self.b = np.matrix(np.copy(b))
        self.c = np.matrix(np.copy(c))

    def solve(self):
        x, inv_a, j_b, j_nb, a = self.first_stage_calculation()
        x, inv_a, j_b, j_nb = self.transform_first_stage_solution(x, a, inv_a, j_b, j_nb)
        for i in range(self.n, x.shape[1]):
            x = np.delete(x, self.n, 1)
        a_basic = np.matrix([[a[j, i] for i in j_b] for j in range(self.a.shape[0])])
        inv_a = a_basic.I
        j_nb = [i for i in range(self.n) if i not in j_b]
        x, inv_a, j_b, j_nb = SimplexSolver._cycle(x, inv_a, j_b, j_nb, self.a, self.c, self.eps)
        return x, j_b

    def first_stage_calculation(self):
        SimplexSolver._liquidate_negative_b(self.b, self.a)
        c = np.matrix([[0] * (self.m + self.n)])
        for i in range(self.m):
            c[0, self.n + i] = -1
        a = self._get_matrix_a_for_first_stage()
        j_nb = [i for i in range(self.n)]
        j_b = [i + self.n for i in range(self.m)]
        inv_a = np.matrix(np.identity(self.m))
        x = np.matrix([[0.0] * (self.m + self.n)])
        for i in range(self.m):
            x[0, i + self.n] = self.b[i, 0]
        x, inv_a, j_b, j_nb = SimplexSolver._cycle(x, inv_a, j_b, j_nb, a, c, self.eps)
        return x, inv_a, j_b, j_nb, a

    def transform_first_stage_solution(self, x, a, inv_a, j_b, j_nb):
        for i in range(self.m):
            if abs(x[0, i + self.n]) > self.eps:
                raise FeasibleRegionIsEmptyError()
        while True:
            k = SimplexSolver._get_basic_synthetic_index(j_b, self.n)
            if k == -1:
                break
            v = SimplexSolver._first_non_zero(inv_a, a, k, self.n, j_nb, self.eps)
            if v == -1:
                i = j_b[k] - self.n
                self.a = np.delete(self.a, i, 0)
                self.b = np.delete(self.b, i, 0)
                self.m -= 1

                a = self._get_matrix_a_for_first_stage()
                el = j_b.pop(k)
                temp = []
                for j in j_b:
                    if j > el:
                        temp.append(j - 1)
                    else:
                        temp.append(j)
                j_b = temp
                temp = []
                for j in j_nb:
                    if j > el:
                        temp.append(j - 1)
                    else:
                        temp.append(j)
                j_nb = temp
                inv_a = np.delete(inv_a, i, 1)
                inv_a = np.delete(inv_a, k, 0)
                continue
            inv_a = sem7.inverseMatrix.get_new_inverse_matrix(inv_a, k, a[:, j_nb[v]], self.eps)
            j_nb[v], j_b[k] = j_b[k], j_nb[v]
        return x, inv_a, j_b, j_nb

    def _get_matrix_a_for_first_stage(self):
        a = np.matrix(np.zeros((self.m, self.n + self.m)))
        for i in range(self.m):
            for j in range(self.n + self.m):
                if j < self.n:
                    a[i, j] = self.a[i, j]
                elif j == i + self.n:
                    a[i, j] = 1
                else:
                    a[i, j] = 0
        return a

    @staticmethod
    def _cycle(x, inv_a, j_b, j_nb, a, c, eps):
        while True:
            j0 = SimplexSolver._find_negative(a, inv_a, c, j_b, j_nb, eps)
            if j0 == -1:
                return x, inv_a, j_b, j_nb
            z = inv_a * a[:, j0]
            positive_indexes = [i for i in range(z.shape[0]) if z[i, 0] > eps]
            if len(positive_indexes) == 0:
                raise ObjectiveFunctionIsUnboundedError()
            s, theta = SimplexSolver._find_s(z, positive_indexes, x, j_b, eps)
            x[0, j0] = theta
            for i in range(len(j_b)):
                x[0, j_b[i]] -= theta * z[i, 0]
            s1 = j_nb.index(j0)
            j_nb[s1], j_b[s] = j_b[s], j_nb[s1]
            a_basis = np.matrix([[a[j, i] for i in j_b] for j in range(a.shape[0])])
            inv_a = a_basis.I

    @staticmethod
    def _find_negative(a, inv_a, c, j_b, j_nb, eps):
        c_basis = np.matrix([[c[0, i] for i in j_b]])
        u = c_basis * inv_a
        delta = np.matrix([(u * a[:, j] - c[0, j])[0, 0] for j in range(a.shape[1])])
        result = delta.shape[1]
        for i in j_nb:
            if delta[0, i] < 0 - eps and i < result:
                result = i
        if result == delta.shape[1]:
            result = -1
        return result

    @staticmethod
    def _first_non_zero(inv_a, a, k, n, j_nb, eps):
        for i, j in enumerate(j_nb):
            if j < n and abs((inv_a * a[:, j])[k, 0]) > eps:
                return i
        return -1

    @staticmethod
    def _find_s(z, positive_indexes, x, j_b, eps):
        def has_lower_index(current, min_element, current_index, min_index):
            return math.isclose(current, min_element, abs_tol=eps) and current_index < min_index
        assert len(positive_indexes) > 0
        s = positive_indexes[0]
        min_element = x[0, j_b[s]] / z[s, 0]
        for i in range(1, len(positive_indexes)):
            current = x[0, j_b[positive_indexes[i]]] / z[positive_indexes[i], 0]
            if has_lower_index(current, min_element, j_b[positive_indexes[i]], j_b[s]) or current < min_element:
                min_element = current
                s = positive_indexes[i]
        return s, min_element

    @staticmethod
    def _get_basic_synthetic_index(j_b, n):
        for i, j in enumerate(j_b):
            if j >= n:
                return i
        return -1

    @staticmethod
    def _liquidate_negative_b(b, a):
        for i in range(b.shape[0]):
            if b[i, 0] < 0:
                b[i, 0] *= -1
                for j in range(a.shape[1]):
                    a[i, j] *= -1
