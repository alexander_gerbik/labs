import numpy as np
import math


class DeterminantIsZeroException(Exception):
    def __init__(self):
        pass


def get_inverse_matrix(matrix, eps):
    n = matrix.shape[0]
    assert matrix.shape[1] == n, "Matrix should be square"
    b = np.matrix(np.identity(n))
    j = [i for i in range(n)]
    s = [0 for _ in range(n)]
    for i in range(n):
        k = get_first_non_zero_element(i, matrix, b, j, eps)
        if k == -1:
            # inverse matrix doesn't exist
            raise DeterminantIsZeroException()
        j.remove(k)
        s[k] = i
        b = np.dot(get_matrix_d(n, i, b * matrix[:, k]), b)
    result = np.matrix(np.identity(n))
    for i in range(n):
        result[i, :] = b[s[i], :]
    return result


def get_new_inverse_matrix(inverse_matrix, column_index, column, eps):
    n = inverse_matrix.shape[0]
    e = get_vector_e(n, column_index)
    bc = inverse_matrix * column
    a = np.dot(e, bc)[0, 0]
    if math.fabs(a) < eps:
        # inverse matrix doesn't exist
        raise DeterminantIsZeroException()
    d = get_matrix_d(n, column_index, bc)
    result = d * inverse_matrix
    return result


def get_first_non_zero_element(i, matrix, b, j, eps):
    for j in j:
        a = (b * matrix[:, j])[i, 0]
        if math.fabs(a) > eps:
            return j
    return -1


def get_vector_e(n, k):
    e = np.zeros(n)
    e[k] = 1
    return e


def get_matrix_d(n, k, column):
    d = np.matrix(np.identity(n))
    element = -1 / column[k, 0]
    for i in range(n):
        d[i, k] = element * column[i, 0]
    d[k, k] = element * -1
    return d
