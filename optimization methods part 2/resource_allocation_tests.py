import unittest

from algorithms import resource_allocation


class ResourceAllocationTests(unittest.TestCase):
    def test_example(self):
        profits = [[3, 4, 5, 8, 9, 10],
                   [2, 3, 7, 9, 12, 13],
                   [1, 2, 6, 11, 11, 13]]
        expected_result = 16
        expected_distribution = [1, 1, 4]
        distribution, result = solve(profits)
        self.assertEqual(expected_distribution, distribution)
        self.assertEqual(result, expected_result)

    def test_task1(self):
        profits = [[1, 2, 2, 4, 5, 6],
                   [2, 3, 5, 7, 7, 8],
                   [2, 4, 5, 6, 7, 7]]
        expected_result = 11
        expected_distribution = [0, 4, 2]
        distribution, result = solve(profits)
        self.assertEqual(expected_distribution, distribution)
        self.assertEqual(result, expected_result)

    def test_task2(self):
        profits = [[1, 1, 3, 6, 10, 11],
                   [2, 3, 5, 6, 7, 13],
                   [1, 4, 4, 7, 8, 9]]
        expected_result = 13
        expected_distribution = [0, 6, 0]
        distribution, result = solve(profits)
        self.assertEqual(expected_distribution, distribution)
        self.assertEqual(result, expected_result)

    def test_task3(self):
        profits = [[1, 2, 4, 8, 9, 9, 23],
                   [2, 4, 6, 6, 8, 10, 11],
                   [3, 4, 7, 7, 8, 8, 24]]
        expected_result = 24
        expected_distribution = [0, 0, 7]
        distribution, result = solve(profits)
        self.assertEqual(expected_distribution, distribution)
        self.assertEqual(result, expected_result)

    def test_task4(self):
        profits = [[3, 3, 6, 7, 8, 9, 14],
                   [2, 4, 4, 5, 6, 8, 13],
                   [1, 1, 2, 3, 3, 10, 11]]
        expected_result = 14
        expected_distribution = [7, 0, 0]
        distribution, result = solve(profits)
        self.assertEqual(expected_distribution, distribution)
        self.assertEqual(result, expected_result)

    def test_task5(self):
        profits = [[2, 2, 3, 5, 8, 8, 10, 17],
                   [1, 2, 5, 8, 10, 11, 13, 15],
                   [4, 4, 5, 6, 7, 13, 14, 14],
                   [1, 3, 6, 9, 10, 11, 14, 16]]
        expected_result = 18
        expected_distribution = [0, 4, 1, 3]
        distribution, result = solve(profits)
        self.assertEqual(expected_distribution, distribution)
        self.assertEqual(result, expected_result)

    def test_task6(self):
        profits = [[1, 3, 4, 5, 8, 9, 9, 11, 12, 12, 14],
                   [1, 2, 3, 3, 3, 7, 12, 13, 14, 17, 19],
                   [4, 4, 7, 7, 8, 12, 14, 14, 16, 18, 22],
                   [5, 5, 5, 7, 9, 13, 13, 15, 15, 19, 24]]
        expected_result = 24
        expected_distribution = [2, 7, 1, 1]
        distribution, result = solve(profits)
        self.assertEqual(expected_distribution, distribution)
        self.assertEqual(result, expected_result)

    def test_task7(self):
        profits = [[4, 4, 6, 9, 12, 12, 15, 16, 19, 19, 19],
                   [1, 1, 1, 4, 7, 8, 8, 13, 13, 19, 20],
                   [2, 5, 6, 7, 8, 9, 11, 11, 13, 13, 18],
                   [1, 2, 4, 5, 7, 8, 8, 9, 9, 15, 19],
                   [2, 5, 7, 8, 9, 10, 10, 11, 14, 17, 21]]
        expected_result = 25
        expected_distribution = [7, 0, 2, 0, 2]
        distribution, result = solve(profits)
        self.assertEqual(expected_distribution, distribution)
        self.assertEqual(result, expected_result)

    def test_task8(self):
        profits = [[1, 2, 2, 2, 3, 5, 8, 9, 13, 14],
                   [1, 3, 4, 5, 5, 7, 7, 10, 12, 12],
                   [2, 2, 3, 4, 6, 6, 8, 9, 11, 17],
                   [1, 1, 1, 2, 3, 9, 9, 11, 12, 15],
                   [2, 7, 7, 7, 9, 9, 10, 11, 12, 13],
                   [2, 5, 5, 5, 6, 6, 7, 12, 18, 22]]
        expected_result = 22
        expected_distribution = [0, 0, 0, 0, 0, 10]
        distribution, result = solve(profits)
        self.assertEqual(expected_distribution, distribution)
        self.assertEqual(result, expected_result)


def solve(profits):
    resource_amount = len(profits[0])
    consumer_amount = len(profits)
    solver = resource_allocation.ResourceAllocator(profits)
    return solver.solve(resource_amount, consumer_amount)


if __name__ == "__main__":
    unittest.main()
