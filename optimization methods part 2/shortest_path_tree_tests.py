import unittest

from algorithms.shortest_path_tree import ShortestPathTree
from algorithms.utils.graph import WeightedOrientedGraph


class ShortestPathTreeTests(unittest.TestCase):
    def test_example(self):
        paths = [[0, 12, 0, 0, 0, 1, 0],
                 [0, 0, 2, 0, 0, 0, 0],
                 [0, 0, 0, 1, 5, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 15, 0, 0, 2],
                 [0, 10, 0, 0, 5, 0, 8],
                 [0, 2, 6, 0, 0, 0, 0]]
        expected = [0, 10, 12, 13, 6, 1, 8]
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task1(self):
        paths = [[0, 5, 0, 0, 0, 0, 0, 3, 0, 0],
                 [0, 0, 2, 0, 0, 0, 3, 0, 0, 0],
                 [0, 0, 0, 0, 5, 0, 0, 0, 0, 0],
                 [0, 0, 2, 0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 1, 0, 0, 0, 0, 0, 2],
                 [0, 0, 4, 0, 1, 0, 0, 6, 2, 0],
                 [2, 0, 2, 0, 0, 5, 0, 0, 0, 0],
                 [0, 1, 0, 0, 0, 0, 4, 0, 1, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 5],
                 [0, 0, 0, 6, 0, 3, 0, 0, 0, 0]]
        expected = [0, 4, 6, 12, 11, 12, 7, 3, 4, 9]
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task2(self):
        paths = [[0, 6, 2, 0, 0, 0, 2, 0, 0],
                 [0, 0, 5, 0, 0, 6, 0, 0, 0],
                 [0, 0, 0, 0, 0, 1, 0, 0, 0],
                 [0, 0, 2, 0, 3, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 4, 0],
                 [4, 0, 0, 0, 6, 0, 3, 7, 0],
                 [0, 0, 0, 0, 0, 0, 0, 4, 0],
                 [0, 1, 0, 0, 0, 0, 0, 0, 1],
                 [0, 0, 0, 1, 5, 2, 0, 0, 0]]
        expected = [0, 6, 2, 8, 9, 3, 2, 6, 7]
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task3(self):
        paths = [[0, 3, 0, 0, 0, 0, 4, 0],
                 [0, 0, 4, 0, 0, 8, 0, 6],
                 [0, 0, 0, 0, 6, 0, 0, 0],
                 [0, 0, 2, 0, 0, 0, 0, 0],
                 [0, 2, 0, 0, 0, 0, 2, 0],
                 [2, 0, 5, 1, 4, 0, 0, 2],
                 [0, 0, 0, 1, 0, 6, 0, 0],
                 [1, 0, 0, 0, 0, 0, 5, 0]]
        expected = [0, 3, 7, 5, 13, 10, 4, 9]
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task4(self):
        paths = [[0, 4, 3, 0, 0, 7, 2, 1],
                 [0, 0, 5, 0, 0, 0, 0, 1],
                 [0, 0, 0, 2, 0, 0, 0, 0],
                 [0, 0, 0, 0, 1, 3, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 6, 0, 0, 0],
                 [0, 0, 4, 0, 7, 2, 0, 0],
                 [0, 0, 4, 3, 0, 0, 5, 0]]
        expected = [0, 4, 3, 4, 5, 4, 2, 1]
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task5(self):
        paths = [[0, 4, 0, 0, 0, 6, 0],
                 [0, 0, 7, 1, 0, 3, 0],
                 [0, 0, 0, 2, 5, 0, 0],
                 [2, 0, 0, 0, 6, 4, 0],
                 [0, 0, 0, 0, 0, 0, 1],
                 [0, 0, 0, 0, 2, 0, 3],
                 [0, 7, 1, 3, 0, 0, 0]]
        expected = [0, 4, 10, 5, 8, 6, 9]
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task6(self):
        paths = [[0, 0, 4, 0, 0, 1, 5, 4],
                 [9, 0, 1, 0, 0, 0, 0, 0],
                 [0, 0, 0, 4, 2, 5, 1, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 3, 0, 0, 0, 8],
                 [0, 2, 0, 0, 7, 0, 0, 3],
                 [0, 10, 0, 0, 0, 0, 0, 3],
                 [0, 0, 0, 6, 0, 0, 0, 0]]
        expected = [0, 3, 4, 8, 6, 1, 5, 4]
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task7(self):
        paths = [[0, 5, 0, 0, 0, 0, 4, 3, 0],
                 [0, 0, 6, 0, 0, 0, 11, 0, 0],
                 [0, 0, 0, 3, 1, 8, 0, 0, 6],
                 [0, 0, 0, 0, 0, 0, 0, 7, 0],
                 [0, 0, 0, 5, 0, 0, 0, 0, 0],
                 [6, 2, 0, 0, 5, 0, 0, 0, 0],
                 [0, 0, 2, 0, 0, 0, 0, 7, 0],
                 [0, 0, 0, 0, 3, 4, 0, 0, 4],
                 [0, 0, 0, 1, 0, 0, 0, 0, 0]]
        expected = [0, 5, 6, 8, 6, 7, 4, 3, 7]
        result = solve(paths)
        self.assertEqual(result, expected)

    def test_task8(self):
        paths = [[0, 1, 0, 0, 0, 5, 6, 5],
                 [0, 0, 4, 4, 6, 0, 0, 0],
                 [0, 0, 0, 5, 7, 12, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 8],
                 [0, 0, 0, 3, 0, 0, 9, 1],
                 [0, 0, 0, 0, 4, 0, 3, 0],
                 [0, 2, 0, 0, 0, 0, 0, 0],
                 [0, 0, 2, 0, 0, 0, 2, 0]]
        expected = [0, 1, 5, 5, 7, 5, 6, 5]
        result = solve(paths)
        self.assertEqual(result, expected)


def solve(paths):
    graph = WeightedOrientedGraph.from_adjacency_matrix(paths)
    solver = ShortestPathTree(graph, 0)
    result = [solver.get_min_distance_to(i) for i in range(graph.nodes_amount)]
    return result


if __name__ == "__main__":
    unittest.main()
