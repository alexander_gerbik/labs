import unittest

from algorithms import constrained_min_cost_flow
from algorithms.utils.graph import WeightedOrientedGraph

# In matrices non-existent edges represented as 0, edges with weight 0 represented as float('-inf')
# because amount of former is much greater than amount of latter.


class ConstrainedMinCostFlowTests(unittest.TestCase):
    def test_example(self):
        paths = [[0, 1, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 3],
                 [0, 3, 0, 5, 0, 0],
                 [0, 0, 0, 0, 0, 0],
                 [0, 0, 4, 1, 0, 0],
                 [-2, 0, 3, 0, 4, 0]]
        a = [1, -4, -5, -6, 5, 9]
        u = [(0, 1, 1), (2, 1, 3), (5, 2, 9), (2, 3, 1), (4, 3, 5)]
        expected_result = 23
        result = solve(paths, u, a)
        self.assertEqual(result, expected_result)

    def test_task1(self):
        paths = [[0, 9, 0, 0, 0, 0, 0, 5, 0],
                 [0, 0, 1, 0, 0, 3, 5, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, -2],
                 [0, 0, -3, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 6, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 8, 0, 0, 0, 0],
                 [0, 0, -1, 4, 7, 0, 0, 0, 1],
                 [0, 0, 0, 0, 0, 0, 2, 0, 2],
                 [0, 0, 0, 0, 0, 6, 0, 0, 0]]
        a = [9, 5, -4, -3, -6, 2, 2, -7, 2]
        u = [(0, 1, 2), (0, 7, 7), (1, 6, 3), (1, 2, 4),
             (4, 3, 3), (5, 4, 4), (6, 4, 5), (8, 5, 2)]
        expected_result = 127
        result = solve(paths, u, a)
        self.assertEqual(result, expected_result)

    def test_task2(self):
        paths = [[0, 8, 0, 0, 0, 0, 0, 3],
                 [0, 0, 2, 0, 0, 0, 9, 0],
                 [0, 0, 0, 0, 0, 4, 0, 0],
                 [0, 0, -2, 0, 0, 1, 0, 0],
                 [0, 0, 0, 8, 0, 0, 0, 0],
                 [0, 0, 0, 0, 4, 0, 0, 0],
                 [0, 0, 11, 0, 6, 2, 0, 0],
                 [0, 0, 0, 0, 0, 5, 5, 0]]
        a = [5, -5, -1, -6, -1, -6, 3, 11]
        u = [(0, 1, 5), (0, 7, 0), (4, 3, 6), (6, 2, 1),
             (6, 4, 7), (7, 5, 6), (7, 6, 5)]
        expected_result = 186
        result = solve(paths, u, a)
        self.assertEqual(result, expected_result)

    def test_task3(self):
        paths = [[0, 8, 0, 0, 0, 0, 0, 3],
                 [0, 0, 2, 0, 0, 0, 9, 0],
                 [0, 0, 0, 0, 0, 4, 0, 0],
                 [0, 0, -2, 0, 0, 0, 0, 0],
                 [0, 0, 0, -3, 0, 0, 0, 0],
                 [0, 0, 0, 0, 8, 0, 0, 0],
                 [0, 0, 13, 1, 1, 7, 0, 0],
                 [0, 0, 0, 0, 0, -1, 1, 0]]
        a = [9, -2, -4, -6, -1, 4, 4, -4]
        u = [(0, 1, 5), (0, 7, 4), (1, 6, 3), (4, 3, 6),
             (5, 4, 7), (6, 2, 4), (6, 5, 3)]
        expected_result = 41
        result = solve(paths, u, a)
        self.assertEqual(result, expected_result)

    def test_task4(self):
        paths = [[0, 1, 0, 0, 0, 0, 4],
                 [0, 0, 5, 0, 3, 0, 0],
                 [0, 0, 0, 3, 0, 2, 0],
                 [0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 10, 5, 0, 2, 0],
                 [0, 0, 0, 1, 0, 0, 0],
                 [0, 0, 0, 0, 8, 6, 0]]
        a = [3, 2, -6, -7, 9, -5, 4]
        u = [(0, 6, 3), (1, 2, 2), (4, 2, 4),
             (4, 3, 7), (6, 4, 2), (6, 5, 5)]
        expected_result = 85
        result = solve(paths, u, a)
        self.assertEqual(result, expected_result)

    def test_task5(self):
        paths = [[0, 3, 0, 0, 5, 4, 2],
                 [0, 0, 5, 0, -1, 0, 7],
                 [0, 0, 0, 6, 0, 0, 1],
                 [0, 0, 0, 0, 0, 1, 0],
                 [0, 0, 2, -2, 0, 1, 0],
                 [0, 0, 0, 0, 0, 0, 7],
                 [0, 0, 0, 0, 0, 0, 0]]
        a = [6, 4, -1, -2, -2, 1, -6]
        u = [(0, 4, 2), (0, 5, 4), (1, 2, 3),
             (1, 6, 1), (2, 3, 2), (5, 6, 5)]
        expected_result = 13
        result = solve(paths, u, a)
        self.assertEqual(result, expected_result)

    def test_task6(self):
        paths = [[0, 6, 0, 0, 0, 2, -2, 0],
                 [0, 0, 3, 6, 0, 1, 0, 0],
                 [0, 0, 0, 0, 3, 4, 0, 0],
                 [0, 0, -1, 0, 0, 0, 0, 1],
                 [0, 0, 0, 0, 0, 0, 0, 7],
                 [0, 0, 0, 0, 5, 0, 5, 3],
                 [0, 4, 0, 0, 2, 0, 0, 2],
                 [0, 0, 0, 0, 0, 0, 0, 0]]
        a = [2, -4, 6, -2, 2, 0, 1, -5]
        u = [(0, 1, 2), (1, 3, 2), (2, 5, 6), (4, 7, 5),
             (5, 4, 3), (5, 6, 3), (6, 1, 4)]
        expected_result = 94
        result = solve(paths, u, a)
        self.assertEqual(result, expected_result)

    def test_task7(self):
        paths = [[0, 7, 6, 0, 3, 0, 0],
                 [0, 0, 4, 0, 0, 3, 0],
                 [0, 0, 0, 6, 5, 0, 0],
                 [0, 0, 0, 0, 0, 1, 0],
                 [0, 0, 0, 4, 0, -1, 0],
                 [0, 0, 0, 0, 0, 0, 4],
                 [2, 0, 0, 0, 7, 0, 0]]
        a = [5, -2, 5, -4, -9, 2, 3]
        u = [(0, 1, 2), (0, 2, 3), (2, 3, 4),
             (2, 4, 4), (5, 6, 2), (6, 4, 5)]
        expected_result = 85
        result = solve(paths, u, a)
        self.assertEqual(result, expected_result)

    def test_task8(self):
        paths = [[0, 5, 0, 0, 1, 5],
                 [0, 0, 0, 10, 0, 3],
                 [1, 3, 0, 6, 2, 0],
                 [0, 0, 0, 0, 3, 0],
                 [0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 2, 4, 0]]
        a = [6, 1, 1, -6, -3, 1]
        u = [(0, 1, 4), (0, 5, 2), (1, 3, 5),
             (2, 3, 1), (5, 4, 3)]
        expected_result = 37
        result = solve(paths, u, a)
        self.assertEqual(result, expected_result)

    def test_defense(self):
        paths = [[0, 1, 0, 0, 0, 0, 0],
                 [0, 0, 1, 3, 2, 0, 0],
                 [3, 0, 0, 3, 0, 4, 0],
                 [4, 0, 0, 0, 0, -1, 6],
                 [0, 0, 0, 5, 0, 0, 1],
                 [0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 2, 0]]
        capac = [[0, 5, 0, 0, 0, 0, 0],
                 [0, 0, 5, 5, 5, 0, 0],
                 [5, 0, 0, 5, 0, 5, 0],
                 [5, 0, 0, 0, 0, 7, 10],
                 [0, 0, 0, 5, 0, 0, 5],
                 [0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 5, 0]]
        a = [-2, 3, 1, 8, 4, -6, -8]
        u = [(3, 0, 2), (1, 2, 3), (2, 3, 4), (4, 3, 4), (3, 6, 8), (3, 5, 6)]
        expected_result = 29
        result = solve(paths, u, a, capac)
        self.assertEqual(result, expected_result)

    def test_defense2(self):
        paths = [[0, -3, 0, float('-inf'), 0, 0, 0],
                 [0, 0, 0, 10, 9, 0, 0],
                 [4, 0, 0, 0, 0, -2, 0],
                 [0, 0, 3, 0, -1, 1, 0],
                 [0, 0, 0, 0, 0, 0, -3],
                 [0, 0, 0, 0, 0, 0, 5],
                 [0, 0, 0, 1, 0, 0, 0]]
        capac = [[6, 6, 6, 6, 6, 6, 6],
                 [6, 6, 6, 6, 6, 6, 6],
                 [6, 6, 6, 6, 6, 6, 6],
                 [6, 6, 6, 6, 6, 6, 6],
                 [6, 6, 6, 6, 6, 6, 6],
                 [6, 6, 6, 6, 6, 6, 6],
                 [6, 6, 6, 6, 6, 6, 6]]
        u = [(0, 1, 2), (1, 3, 4), (1, 4, 5), (2, 0, 3), (3, 5, 2), (5, 6, 3), (3, 2, 6), (6, 3, 6)]
        a = [-2, 3, 1, 8, 4, -6, -8]
        expected_result = 84
        result = solve(paths, u, a, capac)
        self.assertEqual(result, expected_result)

    def test_defense3(self):
        paths = [[0, 8, 0, 0, 0, 10],
                 [0, 0, 10, 0, -10, 5],
                 [0, 0, 0, -4, 6, 3],
                 [0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 5, 0, 0],
                 [0, 0, 0, 0, 9, 0]]
        capac = [[0, 7, 0, 0, 0, 7],
                 [0, 0, 7, 0, 7, 7],
                 [0, 0, 0, 7, 7, 7],
                 [0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 7, 0, 0],
                 [0, 0, 0, 0, 7, 0]]
        a = [10, 4, 4, -2, -10, -6]
        u = [(0, 5, 6), (0, 1, 4), (1, 4, 7), (1, 2, 1), (2, 4, 3), (2, 3, 2)]
        expected_result = 37
        result = solve(paths, u, a, capac)
        self.assertEqual(result, expected_result)

    def test_gabasov(self):
        paths = [[0, 5, 0, 0, 0, 3],
                 [0, 0, 0, 1, 0, 0],
                 [1, 0, 0, 0, 2, 0],
                 [0, 0, 0, 0, 0, 0],
                 [1, 0, 0, 0, 0, 6],
                 [0, 0, 0, 4, 0, 0]]
        capac = [[0, 40, 0, 0, 0, 70],
                 [0, 0, 0, 40, 0, 0],
                 [25, 0, 0, 0, 35, 0],
                 [0, 0, 0, 0, 0, 0],
                 [55, 0, 0, 0, 0, 50],
                 [0, 0, 0, 40, 0, 0]]
        a = [0, 20, -30, 40, -80, 50]
        u = [(0, 1, 20), (0, 5, 40), (1, 3, 0), (2, 0, 5), (2, 4, 25), (4, 0, 55), (4, 5, 50), (5, 3, 40)]
        expected_result = 690
        result = solve(paths, u, a, capac)
        self.assertEqual(result, expected_result)


def solve(paths, u, a, caps=None):
    g = WeightedOrientedGraph.from_adjacency_matrix(preprocess(paths), float('-inf'))
    if caps is None:
        capacities = default_capacities(g)
    else:
        capacities = WeightedOrientedGraph.from_adjacency_matrix(preprocess(caps), float('-inf'))
    first_basis = get_graph_from_edges(g.nodes_amount, u)
    solver = constrained_min_cost_flow.ConstrainedMinCostFlow(g, a, capacities)
    basis = solver.solve(first_basis)
    result = get_cost(g, basis)
    return result


def preprocess(matrix):
    return [[replace_inf(el) for el in row] for row in matrix]


def replace_inf(x):
    if x == 0:
        return float('-inf')
    if x == float('-inf'):
        return 0
    return x


def get_cost(graph, basis_tree):
    return sum(basis_tree[i, j] * graph[i, j] for i, j in basis_tree)


def get_graph_from_edges(node_amount, edges):
    graph = WeightedOrientedGraph(node_amount)
    for i, j, weight in edges:
        graph[i, j] = weight
    return graph


def default_capacities(paths):
    res = WeightedOrientedGraph(paths.nodes_amount)
    for edge in paths:
        if paths.edge_exists(*edge):
            res[edge] = float('inf')
    return res


if __name__ == "__main__":
    unittest.main()
