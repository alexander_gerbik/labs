import unittest

import numpy as np

from algorithms import cutting_plane_method


class CuttingPlaneMethodTests(unittest.TestCase):
    def test_example1(self):
        a = np.matrix([[5, -1, 1, 0, 0],
                       [-1, 2, 0, 1, 0],
                       [-7, 2, 0, 0, 1]])
        b = np.matrix([[15], [6], [0]])
        c = np.matrix([[-3.5, 1, 0, 0, 0]])
        expected = 0
        result = solve(a, b, c)
        self.assertAlmostEqual(result, expected)

    def test_example2(self):
        c = np.matrix([[1, -1, 0, 0, 0]])
        a = np.matrix([[5, 3, 1, 0, 0],
                       [-1, 2, 0, 1, 0],
                       [1, -2, 0, 0, 1]])
        b = np.matrix([[4], [3], [7]])
        expected = 0
        result = solve(a, b, c)
        self.assertAlmostEqual(result, expected)

    def test_example3(self):
        c = np.matrix([[2, -5, 0, 0, 0]])
        a = np.matrix([[-2, -1, 1, 0, 0],
                       [3, 1, 0, 1, 0],
                       [-1, 1, 0, 0, 1]])
        b = np.matrix([[-1], [10], [3]])
        expected = 6
        result = solve(a, b, c)
        self.assertAlmostEqual(result, expected)

    def test_task1(self):
        c = np.matrix([[7, -2, 6, 0, 5, 2]])
        a = np.matrix([[1, -5, 3, 1, 0, 0],
                       [4, -1, 1, 0, 1, 0],
                       [2, 4, 2, 0, 0, 1]])
        b = np.matrix([[-8], [22], [30]])
        expected = 160
        result = solve(a, b, c)
        self.assertAlmostEqual(result, expected)

    @unittest.skip("it takes too much time to solve")
    def test_task2(self):
        a = np.matrix([[1, -3, 2, 0, 1, -1, 4, -1, 0],
                       [1, -1, 6, 1, 0, -2, 2, 2, 0],
                       [2, 2, -1, 1, 0, -3, 8, -1, 1],
                       [4, 1, 0, 0, 1, -1, 0, -1, 1],
                       [1, 1, 1, 1, 1, 1, 1, 1, 1]
                       ])
        b = np.matrix([[3], [9], [9], [5], [9]])
        c = np.matrix([[-1, 5, -2, 4, 3, 1, 2, 8, 3]])
        expected = 23
        result = solve(a, b, c)
        self.assertAlmostEqual(result, expected)

    def test_task3(self):
        a = np.matrix([[1, 0, 0, 12, 1, -3, 4, -1],
                       [0, 1, 0, 11, 12, 3, 5, 3],
                       [0, 0, 1, 1, 0, 22, -2, 1]
                       ])
        b = np.matrix([[40], [107], [61]])
        c = np.matrix([[2, 1, -2, -1, 4, -5, 5, 5]])
        expected = 311
        result = solve(a, b, c)
        self.assertAlmostEqual(result, expected)

    @unittest.skip("it takes too much time to solve")
    def test_task4(self):
        a = np.matrix([[1, 2, 3, 12, 1, - 3, 4, - 1, 2, 3],
                       [0, 2, 0, 11, 12, 3, 5, 3, 4, 5],
                       [0, 0, 2, 1, 0, 22, - 2, 1, 6, 7]
                       ])
        b = np.matrix([[153], [123], [112]])
        c = np.matrix([[2, 1, -2, -1, 4, -5, 5, 5, 1, 2]])
        expected = 543
        result = solve(a, b, c)
        self.assertAlmostEqual(result, expected)

    def test_task5(self):
        a = np.matrix([[2, 1, -1, -3, 4, 7],
                       [0, 1, 1, 1, 2, 4],
                       [6, -3, -2, 1, 1, 1]
                       ])
        b = np.matrix([[7], [16], [6]])
        c = np.matrix([[1, 2, 1, -1, 2, 3]])
        expected = 21
        result = solve(a, b, c)
        self.assertAlmostEqual(result, expected)

    def test_task6(self):
        a = np.matrix([[0, 7, 1, -1, -4, 2, 4],
                       [5, 1, 4, 3, -5, 2, 1],
                       [2, 0, 3, 1, 0, 1, 5]
                       ])
        b = np.matrix([[12], [27], [19]])
        c = np.matrix([[10, 2, 1, 7, 6, 3, 1]])
        expected = 157
        result = solve(a, b, c)
        self.assertAlmostEqual(result, expected)

    def test_task7(self):
        a = np.matrix([[0, 7, -8, -1, 5, 2, 1],
                       [3, 2, 1, -3, -1, 1, 0],
                       [1, 5, 3, -1, -2, 1, 0],
                       [1, 1, 1, 1, 1, 1, 1]
                       ])
        b = np.matrix([[6], [3], [7], [7]])
        c = np.matrix([[2, 9, 3, 5, 1, 2, 4]])
        expected = 26
        result = solve(a, b, c)
        self.assertAlmostEqual(result, expected)

    def test_task8(self):
        a = np.matrix([[1, 0, -1, 3, -2, 0, 1],
                       [0, 2, 1, -1, 0, 3, -1],
                       [1, 2, 1, 4, 2, 1, 1]
                       ])
        b = np.matrix([[4], [8], [24]])
        c = np.matrix([[-1, -3, -7, 0, -4, 0, -1]])
        expected = -16
        result = solve(a, b, c)
        self.assertAlmostEqual(result, expected)

    def test_task9(self):
        a = np.matrix([[1, -3, 2, 0, 1, -1, 4, -1, 0],
                       [1, -1, 6, 1, 0, -2, 2, 2, 0],
                       [2, 2, -1, 1, 0, -3, 2, -1, 1],
                       [4, 1, 0, 0, 1, -1, 0, -1, 1],
                       [1, 1, 1, 1, 1, 1, 1, 1, 1]
                       ])
        b = np.matrix([[3], [9], [9], [5], [9]])
        c = np.matrix([[-1, 5, -2, 4, 3, 1, 2, 8, 3]])
        expected = 25
        result = solve(a, b, c)
        self.assertAlmostEqual(result, expected)


def solve(a, b, c):
    plan = cutting_plane_method.solve(a, b, c)
    return (plan * c.T)[0, 0]

if __name__ == "__main__":
    unittest.main()
