import unittest

from algorithms import min_cost_flow
from algorithms.utils.graph import WeightedOrientedGraph


class MinCostFlowTests(unittest.TestCase):
    def test_example(self):
        paths = [[0, 1, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 3],
                 [0, 3, 0, 5, 0, 0],
                 [0, 0, 0, 0, 0, 0],
                 [0, 0, 4, 1, 0, 0],
                 [-2, 0, 3, 0, 4, 0]]
        a = [1, -4, -5, -6, 5, 9]
        u = [(0, 1, 1), (2, 1, 3), (5, 2, 9), (2, 3, 1), (4, 3, 5)]
        expected = 23
        result = solve(paths, u, a)
        self.assertEqual(result, expected)

    def test_task1(self):
        paths = [[0, 9, 0, 0, 0, 0, 0, 5, 0],
                 [0, 0, 1, 0, 0, 3, 5, 0, 0],
                 [0, 0, 0, 0, 0, 0, 0, 0, -2],
                 [0, 0, -3, 0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 6, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 8, 0, 0, 0, 0],
                 [0, 0, -1, 4, 7, 0, 0, 0, 1],
                 [0, 0, 0, 0, 0, 0, 2, 0, 2],
                 [0, 0, 0, 0, 0, 6, 0, 0, 0]]
        a = [9, 5, -4, -3, -6, 2, 2, -7, 2]
        u = [(0, 1, 2), (0, 7, 7), (1, 6, 3), (1, 2, 4),
             (4, 3, 3), (5, 4, 4), (6, 4, 5), (8, 5, 2)]
        expected = 127
        result = solve(paths, u, a)
        self.assertEqual(result, expected)

    def test_task2(self):
        paths = [[0, 8, 0, 0, 0, 0, 0, 3],
                 [0, 0, 2, 0, 0, 0, 9, 0],
                 [0, 0, 0, 0, 0, 4, 0, 0],
                 [0, 0, -2, 0, 0, 1, 0, 0],
                 [0, 0, 0, 8, 0, 0, 0, 0],
                 [0, 0, 0, 0, 4, 0, 0, 0],
                 [0, 0, 11, 0, 6, 2, 0, 0],
                 [0, 0, 0, 0, 0, 5, 5, 0]]
        a = [5, -5, -1, -6, -1, -6, 3, 11]
        u = [(0, 1, 5), (0, 7, 0), (4, 3, 6), (6, 2, 1),
             (6, 4, 7), (7, 5, 6), (7, 6, 5)]
        expected = 186
        result = solve(paths, u, a)
        self.assertEqual(result, expected)

    def test_task3(self):
        paths = [[0, 8, 0, 0, 0, 0, 0, 3],
                 [0, 0, 2, 0, 0, 0, 9, 0],
                 [0, 0, 0, 0, 0, 4, 0, 0],
                 [0, 0, -2, 0, 0, 0, 0, 0],
                 [0, 0, 0, -3, 0, 0, 0, 0],
                 [0, 0, 0, 0, 8, 0, 0, 0],
                 [0, 0, 13, 1, 1, 7, 0, 0],
                 [0, 0, 0, 0, 0, -1, 1, 0]]
        a = [9, -2, -4, -6, -1, 4, 4, -4]
        u = [(0, 1, 5), (0, 7, 4), (1, 6, 3), (4, 3, 6),
             (5, 4, 7), (6, 2, 4), (6, 5, 3)]
        expected = 41
        result = solve(paths, u, a)
        self.assertEqual(result, expected)

    def test_task4(self):
        paths = [[0, 1, 0, 0, 0, 0, 4],
                 [0, 0, 5, 0, 3, 0, 0],
                 [0, 0, 0, 3, 0, 2, 0],
                 [0, 0, 0, 0, 0, 0, 0],
                 [0, 0, 10, 5, 0, 2, 0],
                 [0, 0, 0, 1, 0, 0, 0],
                 [0, 0, 0, 0, 8, 6, 0]]
        a = [3, 2, -6, -7, 9, -5, 4]
        u = [(0, 6, 3), (1, 2, 2), (4, 2, 4),
             (4, 3, 7), (6, 4, 2), (6, 5, 5)]
        expected = 85
        result = solve(paths, u, a)
        self.assertEqual(result, expected)

    def test_task5(self):
        paths = [[0, 3, 0, 0, 5, 4, 2],
                 [0, 0, 5, 0, -1, 0, 7],
                 [0, 0, 0, 6, 0, 0, 1],
                 [0, 0, 0, 0, 0, 1, 0],
                 [0, 0, 2, -2, 0, 1, 0],
                 [0, 0, 0, 0, 0, 0, 7],
                 [0, 0, 0, 0, 0, 0, 0]]
        a = [6, 4, -1, -2, -2, 1, -6]
        u = [(0, 4, 2), (0, 5, 4), (1, 2, 3),
             (1, 6, 1), (2, 3, 2), (5, 6, 5)]
        expected = 13
        result = solve(paths, u, a)
        self.assertEqual(result, expected)

    def test_task6(self):
        paths = [[0, 6, 0, 0, 0, 2, -2, 0],
                 [0, 0, 3, 6, 0, 1, 0, 0],
                 [0, 0, 0, 0, 3, 4, 0, 0],
                 [0, 0, -1, 0, 0, 0, 0, 1],
                 [0, 0, 0, 0, 0, 0, 0, 7],
                 [0, 0, 0, 0, 5, 0, 5, 3],
                 [0, 4, 0, 0, 2, 0, 0, 2],
                 [0, 0, 0, 0, 0, 0, 0, 0]]
        a = [2, -4, 6, -2, 2, 0, 1, -5]
        u = [(0, 1, 2), (1, 3, 2), (2, 5, 6), (4, 7, 5),
             (5, 4, 3), (5, 6, 3), (6, 1, 4)]
        expected = 94
        result = solve(paths, u, a)
        self.assertEqual(result, expected)

    def test_task7(self):
        paths = [[0, 7, 6, 0, 3, 0, 0],
                 [0, 0, 4, 0, 0, 3, 0],
                 [0, 0, 0, 6, 5, 0, 0],
                 [0, 0, 0, 0, 0, 1, 0],
                 [0, 0, 0, 4, 0, -1, 0],
                 [0, 0, 0, 0, 0, 0, 4],
                 [2, 0, 0, 0, 7, 0, 0]]
        a = [5, -2, 5, -4, -9, 2, 3]
        u = [(0, 1, 2), (0, 2, 3), (2, 3, 4),
             (2, 4, 4), (5, 6, 2), (6, 4, 5)]
        expected = 85
        result = solve(paths, u, a)
        self.assertEqual(result, expected)

    def test_task8(self):
        paths = [[0, 5, 0, 0, 1, 5],
                 [0, 0, 0, 10, 0, 3],
                 [1, 3, 0, 6, 2, 0],
                 [0, 0, 0, 0, 3, 0],
                 [0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 2, 4, 0]]
        a = [6, 1, 1, -6, -3, 1]
        u = [(0, 1, 4), (0, 5, 2), (1, 3, 5),
             (2, 3, 1), (5, 4, 3)]
        expected = 37
        result = solve(paths, u, a)
        self.assertEqual(result, expected)


def solve(paths, u, a):
    g = WeightedOrientedGraph.from_adjacency_matrix(paths)
    first_basis = get_graph_from_edges(g.nodes_amount, u)
    solver = min_cost_flow.MinCostFlow(g, a)
    basis = solver.solve(first_basis)
    return get_cost(g, basis)


def get_cost(graph, basis_tree):
    return sum(basis_tree[i, j] * graph[i, j] for i, j in basis_tree)


def get_graph_from_edges(node_amount, edges):
    g = WeightedOrientedGraph(node_amount)
    for i, j, weight in edges:
        g[i, j] = weight
    return g


if __name__ == "__main__":
    unittest.main()
