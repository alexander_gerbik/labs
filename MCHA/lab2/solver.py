import numpy

def solve(a, b, t0, t1, spaceStepsAmount, timeStepsAmount, k, timeZeroFunction, leftFunction, rightFunction, func):
    timeN = timeStepsAmount + 1
    spaceN = spaceStepsAmount + 1
    h = (b-a)/spaceStepsAmount
    tau = (t1-t0)/timeStepsAmount
    assert tau <= (h*h)/(2*k), "Stability condition is not satisfied"
    u = numpy.zeros((timeN, spaceN))
    for i in range(timeN):
        u[i][0] = leftFunction(t0 + tau*i)
        u[i][spaceN-1] = rightFunction(t0 + tau*i)
    for i in range(spaceN):
        u[0][i] = timeZeroFunction(a + h*i)
    for n in range(1, timeN):
        for i in range(1, spaceN-1):
            u[n][i] = (u[n-1][i-1] + u[n-1][i+1] - 2 * u[n-1][i])*k*tau/(h*h)+ tau*func(a + h*i, t0 + tau*(n-1)) + u[n-1][i]
    return u