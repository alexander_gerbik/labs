import solver
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np
import math

# var 2.12
a, b, k, T = -1, 1, 0.5, 0.4
phi = lambda x: x*x
gLeft = lambda t: 1
gRight = lambda t: 1
func = lambda x, t: x

if __name__ == "__main__":
    spaceStepsAmount = 10
    h = (b-a)/spaceStepsAmount
    tau = h**2/(4*k)
    timeStepsAmount = math.ceil(T / tau)
    result = solver.solve(a, b, 0, T, spaceStepsAmount, timeStepsAmount, k, phi, gLeft, gRight, func)
    X = np.arange(a, b+h, h)
    Y = np.arange(0, T+tau, tau)
    X, Y = np.meshgrid(X, Y)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(X, Y, result, rstride=1, cstride=1, cmap=cm.coolwarm,
            linewidth=0, antialiased=False)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()