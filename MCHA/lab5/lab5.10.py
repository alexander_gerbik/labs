from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np
import math

nu, a, b = 1, 1, 1
xMin, xMax = 0, math.pi
yMin, yMax = 0, math.pi
tMin, tMax = 0, math.pi
func = lambda x, y, t: math.sin(x)*math.sin(y)*(nu*math.cos(nu*t)+(a+b)*math.sin(nu*t))
xMinFunc = lambda y, t: 0
yMinFunc = lambda x, t: 0
xMaxDerivative = lambda y, t: -math.sin(y)*math.sin(nu*t)
yMaxDerivative = lambda x, t: -math.sin(x)*math.sin(nu*t)
timeZeroFunc = lambda x, y: 0
analyticSolution = lambda x, y, t: math.sin(x)*math.sin(y)*math.sin(nu*t)

def main():
    xN, yN, tN = 20, 40, 30
    result = solve(nu, a, b, xMin, xMax, yMin, yMax, tMin, tMax, xN, yN, tN, func, xMinFunc, yMinFunc, xMaxDerivative,
                   yMaxDerivative, timeZeroFunc)
    hX = (xMax - xMin) / xN
    hY = (yMax - yMin) / yN
    hT = (tMax - tMin) / tN
    xN, yN, tN = xN+1, yN+1, tN+1
    error = 0
    for i in range(xN):
        x = xMin+hX*i
        for j in range(yN):
            y = yMin+hY*j
            for k in range(tN):
                t = tMin+hT*k
                currentError = math.fabs(result[i, j, k] - analyticSolution(x, y, t))
                error = max(error, currentError)
    print(error)
    X = np.arange(xMin, xMax+hX, hX)
    Y = np.arange(yMin, yMax+hY, hY)
    T = np.arange(tMin, tMax+hT/2, hT)

    fig = plt.figure()
    k = tN//2
    t = tMin+hT*k
    currentResult = result[:, :, k]
    Y1, X1 = np.meshgrid(Y, X)
    fig.suptitle('t = '+ str(t), fontsize=20)
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(X1, Y1, currentResult, rstride=1, cstride=1, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()


def solve(nu, a, b, xMin, xMax, yMin, yMax, tMin, tMax, xN, yN, tN, func, xMinFunc, yMinFunc, xMaxDerivative, yMaxDerivative,
          timeZeroFunc):
    hX = (xMax - xMin) / xN
    hY = (yMax - yMin) / yN
    hT = (tMax - tMin) / tN
    xN, yN, tN = xN+1, yN+1, tN+1
    U = np.zeros((xN, yN, tN))
    for i in range(xN):
        x = xMin+hX*i
        for j in range(yN):
            y = yMin+hY*j
            k = 0
            U[i, j, k] = timeZeroFunc(x, y)
    for j in range(yN):
        y = yMin+hY*j
        for k in range(tN):
            t = tMin+hT*k
            i = 0
            U[i, j, k] = xMinFunc(y, t)
    for i in range(xN):
        x = xMin+hX*i
        for k in range(tN):
            t = tMin+hT*k
            j = 0
            U[i, j, k] = yMinFunc(x, t)
    intermediateU = np.zeros((xN, yN))
    for k in range(tN-1):
        t = tMin+hT*k
        for j in range(yN):
            y = yMin+hY*j
            i = 0
            intermediateU[i, j] = xMinFunc(y, t+hT/2)
        for i in range(xN):
            x = xMin+hX*i
            j = 0
            intermediateU[i, j] = yMinFunc(x, t+hT/2)
        for j in range(1, yN):
            y = yMin+hY*j
            previousVector = []
            currentVector = []
            nextVector = []
            rightPartVector = []

            x = xMin + hX
            previous = 0
            current = 1 + 2*hT*a/(hX*hX)
            next = -hT*a/(hX*hX)
            rightPart = hT*func(x, y, t)/2 + U[1, j, k] + a*hT*intermediateU[0, j]/(hX*hX)
            previousVector.append(previous)
            currentVector.append(current)
            nextVector.append(next)
            rightPartVector.append(rightPart)
            for i in range(2, xN-1):
                x = xMin+hX*i
                previous = -hT*a/(hX*hX)
                current = 1 + 2*hT*a/(hX*hX)
                next = -hT*a/(hX*hX)
                rightPart = hT*func(x, y, t)/2+U[i, j, k]
                previousVector.append(previous)
                currentVector.append(current)
                nextVector.append(next)
                rightPartVector.append(rightPart)
            x = xMax
            previous = -hT*a/(hX*hX)
            current = 1 + hT*a/(hX*hX)
            next = 0
            rightPart = hT*a*xMaxDerivative(y, t)/hX + hT*func(x, y, t)/2+U[xN-1, j, k]
            previousVector.append(previous)
            currentVector.append(current)
            nextVector.append(next)
            rightPartVector.append(rightPart)
            y = tridiagonalMatrixSolve(previousVector, currentVector, nextVector, rightPartVector)
            for i in range(1, xN):
                intermediateU[i, j] = y[i-1]

        for i in range(1, xN):
            x = xMin+hX*i
            previousVector = []
            currentVector = []
            nextVector = []
            rightPartVector = []

            y = yMin + hY
            previous = 0
            current = 1 + 2*hT*b/(hY*hY)
            next = -hT*b/(hY*hY)
            rightPart = hT*func(x, y, t+hT)/2 + intermediateU[i, 1] + b*hT*U[i, 0, k+1]/(hY*hY)
            previousVector.append(previous)
            currentVector.append(current)
            nextVector.append(next)
            rightPartVector.append(rightPart)
            for j in range(2, yN-1):
                y = yMin+hY*j
                previous = -hT*b/(hY*hY)
                current = 1 + 2*hT*b/(hY*hY)
                next = -hT*b/(hY*hY)
                rightPart = hT*func(x, y, t+hT)/2+intermediateU[i, j]
                previousVector.append(previous)
                currentVector.append(current)
                nextVector.append(next)
                rightPartVector.append(rightPart)
            y = yMax
            previous = -hT*b/(hY*hY)
            current = 1 + hT*b/(hY*hY)
            next = 0
            rightPart = hT*b*yMaxDerivative(x, t)/hY + hT*func(x, y, t+hT)/2+intermediateU[i, yN-1]
            previousVector.append(previous)
            currentVector.append(current)
            nextVector.append(next)
            rightPartVector.append(rightPart)
            y = tridiagonalMatrixSolve(previousVector, currentVector, nextVector, rightPartVector)
            for j in range(1, yN):
                U[i, j, k+1] = y[j-1]
    return U


def tridiagonalMatrixSolve(a, c, b, f):
    n = len(a)
    assert n == len(c) and n == len(b) and n == len(f), "a, c, b, f should have the same length"
    x = [0.0]*n
    for i in range(1, n):
        m = a[i]/c[i-1]
        c[i] -= m*b[i-1]
        f[i] -= m*f[i-1]
    x[n-1] = f[n-1]/c[n-1]
    for i in range(n-2,-1, -1):
        x[i] = (f[i] - b[i]*x[i+1])/c[i]
    return x


if __name__ == "__main__":
    main()
