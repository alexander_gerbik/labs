from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np
import math

nu1, nu2, a = 1, 1, 1
xMin, xMax = 0, math.pi
yMin, yMax = 0, math.pi
tMin, tMax = 0, math.pi
func = lambda x, y, t: 0
xMinFunc = lambda y, t: math.cos(nu2*y)*math.exp(-(nu1*nu1+nu2*nu2)*a*t)
yMinFunc = lambda x, t: math.cos(nu1*x)*math.exp(-(nu1*nu1+nu2*nu2)*a*t)
xMaxFunc = lambda y, t: math.pow(-1, nu1)*math.cos(nu2*y)*math.exp(-(nu1*nu1+nu2*nu2)*a*t)
yMaxFunc = lambda x, t: math.pow(-1, nu2)*math.cos(nu1*x)*math.exp(-(nu1*nu1+nu2*nu2)*a*t)
timeZeroFunc = lambda x, y: math.cos(nu1*x)*math.cos(nu2*y)
analyticSolution = lambda x, y, t: math.cos(nu1*x)*math.cos(nu2*y)*math.exp(-(nu1*nu1+nu2*nu2)*a*t)

def main():
    xN, yN, tN = 20, 40, 30
    result = solve(nu1, nu2, a, xMin, xMax, yMin, yMax, tMin, tMax, xN, yN, tN, func, xMinFunc, yMinFunc, xMaxFunc,
                   yMaxFunc, timeZeroFunc)
    hX = (xMax - xMin) / xN
    hY = (yMax - yMin) / yN
    hT = (tMax - tMin) / tN
    xN, yN, tN = xN+1, yN+1, tN+1
    error = 0
    for i in range(xN):
        x = xMin+hX*i
        for j in range(yN):
            y = yMin+hY*j
            for k in range(tN):
                t = tMin+hT*k
                currentError = math.fabs(result[i, j, k] - analyticSolution(x, y, t))
                error = max(error, currentError)
    print(error)
    X = np.arange(xMin, xMax+hX, hX)
    Y = np.arange(yMin, yMax+hY, hY)
    T = np.arange(tMin, tMax+hT/2, hT)

    fig = plt.figure()
    k = tN//2
    t = tMin+hT*k
    currentResult = result[:, :, k]
    Y1, X1 = np.meshgrid(Y, X)
    fig.suptitle('t = '+ str(t), fontsize=20)
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(X1, Y1, currentResult, rstride=1, cstride=1, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()


def solve(nu1, nu2, a, xMin, xMax, yMin, yMax, tMin, tMax, xN, yN, tN, func, xMinFunc, yMinFunc, xMaxFunc, yMaxFunc,
          timeZeroFunc):
    hX = (xMax - xMin) / xN
    hY = (yMax - yMin) / yN
    hT = (tMax - tMin) / tN
    xN, yN, tN = xN+1, yN+1, tN+1
    U = np.zeros((xN, yN, tN))
    for i in range(xN):
        x = xMin+hX*i
        for j in range(yN):
            y = yMin+hY*j
            k = 0
            U[i, j, k] = timeZeroFunc(x, y)
    for j in range(yN):
        y = yMin+hY*j
        for k in range(tN):
            t = tMin+hT*k
            i = 0
            U[i, j, k] = xMinFunc(y, t)
            U[xN-1, j, k] = xMaxFunc(y, t)
    for i in range(xN):
        x = xMin+hX*i
        for k in range(tN):
            t = tMin+hT*k
            j = 0
            U[i, j, k] = yMinFunc(x, t)
            U[i, yN-1, k] = yMaxFunc(x, t)
    intermediateU = np.zeros((xN, yN))
    for k in range(tN-1):
        t = tMin+hT*k
        for j in range(yN):
            y = yMin+hY*j
            i = 0
            intermediateU[i, j] = xMinFunc(y, t+hT/2)
        for i in range(xN):
            x = xMin+hX*i
            j = 0
            intermediateU[i, j] = yMinFunc(x, t+hT/2)
        for j in range(1, yN-1):
            y = yMin+hY*j
            previousVector = []
            currentVector = []
            nextVector = []
            rightPartVector = []

            x = xMin + hX
            previous = 0
            current = 1 + 2*hT*a/(hX*hX)
            next = -hT*a/(hX*hX)
            rightPart = hT*func(x, y, t)/2 + U[1, j, k] + a*hT*intermediateU[0, j]/(hX*hX)
            previousVector.append(previous)
            currentVector.append(current)
            nextVector.append(next)
            rightPartVector.append(rightPart)
            for i in range(2, xN-2):
                x = xMin+hX*i
                previous = -hT*a/(hX*hX)
                current = 1 + 2*hT*a/(hX*hX)
                next = -hT*a/(hX*hX)
                rightPart = hT*func(x, y, t)/2+U[i, j, k]
                previousVector.append(previous)
                currentVector.append(current)
                nextVector.append(next)
                rightPartVector.append(rightPart)
            x = xMax-hX
            previous = -hT*a/(hX*hX)
            current = 1 + 2*hT*a/(hX*hX)
            next = 0
            rightPart = + hT*func(x, y, t)/2+U[xN-2, j, k]+hT*a*xMaxFunc(y, t)/(hX*hX)
            previousVector.append(previous)
            currentVector.append(current)
            nextVector.append(next)
            rightPartVector.append(rightPart)
            y = tridiagonalMatrixSolve(previousVector, currentVector, nextVector, rightPartVector)
            for i in range(1, xN-1):
                intermediateU[i, j] = y[i-1]

        for i in range(1, xN-1):
            x = xMin+hX*i
            previousVector = []
            currentVector = []
            nextVector = []
            rightPartVector = []

            y = yMin + hY
            previous = 0
            current = 1 + 2*hT*a/(hY*hY)
            next = -hT*a/(hY*hY)
            rightPart = hT*func(x, y, t+hT)/2 + intermediateU[i, 1] + a*hT*U[i, 0, k+1]/(hY*hY)
            previousVector.append(previous)
            currentVector.append(current)
            nextVector.append(next)
            rightPartVector.append(rightPart)
            for j in range(2, yN-2):
                y = yMin+hY*j
                previous = -hT*a/(hY*hY)
                current = 1 + 2*hT*a/(hY*hY)
                next = -hT*a/(hY*hY)
                rightPart = hT*func(x, y, t+hT)/2+intermediateU[i, j]
                previousVector.append(previous)
                currentVector.append(current)
                nextVector.append(next)
                rightPartVector.append(rightPart)
            y = yMax-hY
            previous = -hT*a/(hY*hY)
            current = 1 + 2*hT*a/(hY*hY)
            next = 0
            rightPart = hT*func(x, y, t+hT)/2+intermediateU[i, yN-2]+hT*a*yMaxFunc(x, t+hT)/(hY*hY)
            previousVector.append(previous)
            currentVector.append(current)
            nextVector.append(next)
            rightPartVector.append(rightPart)
            y = tridiagonalMatrixSolve(previousVector, currentVector, nextVector, rightPartVector)
            for j in range(1, yN-1):
                U[i, j, k+1] = y[j-1]
    return U


def tridiagonalMatrixSolve(a, c, b, f):
    n = len(a)
    assert n == len(c) and n == len(b) and n == len(f), "a, c, b, f should have the same length"
    x = [0.0]*n
    for i in range(1, n):
        m = a[i]/c[i-1]
        c[i] -= m*b[i-1]
        f[i] -= m*f[i-1]
    x[n-1] = f[n-1]/c[n-1]
    for i in range(n-2,-1, -1):
        x[i] = (f[i] - b[i]*x[i+1])/c[i]
    return x


if __name__ == "__main__":
    main()
