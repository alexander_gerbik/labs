import matplotlib.pyplot as plt
import lab1Solver as solver
import math

# Вариант 2.5.8
a = 0
b = 1.5
c = 0.925
k1 = 1.5
k2 = 0.4
q1 = 7.5
q2 = 12
def function(x):
    return 7*math.exp(-x)

def getK(x):
    if x < c:
        return k1
    elif x == c:
        return 0.5*(k1 + k2)
    else:
        return k2

def getQ(x):
    if x < c:
        return q1
    elif x == c:
        return 0.5*(q1 + q2)
    else:
        return q2

def checkSolution(args, y):
    result = []
    n = len(args)
    h = args[1] - args[0]
    derivative = (y[1] - y[0])/h
    delta = -getK(args[0])*derivative + 0.5*y[0]
    result.append(delta)
    for i in range(1, n-1):
        #derivative = 0.5*(ys[i+1]-ys[i-1])/h
        secondDerivative = (y[i+1]-2*y[i]+y[i-1])/(h*h)
        delta = -getK(args[i])*secondDerivative + getQ(args[i])*y[i] - function(args[i])
        result.append(delta)
    derivative = (y[n-1]-y[n-2])/h
    delta = getK(args[n-1])*derivative + 0.5*y[n-1]
    result.append(delta)
    return result

def main():
    args, ys = solver.solve(a, b, getK, getQ, function, 1000)
    plt.figure(1)
    plt.plot(args, ys)
    deltas = checkSolution(args, ys)
    plt.figure(2)
    plt.axis([a-0.1, b+0.1, -0.01, 0.01])
    plt.plot(args, deltas)
    plt.show()

if __name__ == "__main__":
    main()
