# lab1Solver.py
def solve(a, b, getK, getQ, yFunction, n):
    assert a < b, "a < b !"
    argVector = []
    previousVector = []
    currentVector = []
    nextVector = []
    rightPartVector = []
    h = (b - a) / n
    x = a
    p = 0.5 * h * yFunction(x)
    previous = 0
    current = 0.5 + getK(x + h)/h + 0.5*h*getQ(x)
    next = - getK(x + h) / h
    argVector.append(x)
    previousVector.append(previous)
    currentVector.append(current)
    nextVector.append(next)
    rightPartVector.append(p)
    for i in range(1, n):
        x = a + h * i
        p = h * yFunction(x)
        previous = - getK(x) / h
        current = getK(x + h) / h + getK(x) / h + getQ(x)*h
        next = - getK(x + h) / h
        argVector.append(x)
        previousVector.append(previous)
        currentVector.append(current)
        nextVector.append(next)
        rightPartVector.append(p)
    x = b
    p = 0.5 * h * yFunction(x)
    previous = - getK(x) / h
    current = 0.5 + getK(x) / h + 0.5 * h * getQ(x)
    next = 0
    argVector.append(x)
    previousVector.append(previous)
    currentVector.append(current)
    nextVector.append(next)
    rightPartVector.append(p)
    y = tridiagonalMatrixSolve(previousVector, currentVector, nextVector, rightPartVector)
    return argVector, y


def tridiagonalMatrixSolve(a, c, b, f):
    n = len(a)
    assert n == len(c) and n == len(b) and n == len(f), "a, c, b, f should have the same length"
    x = [0.0]*n
    for i in range(1, n):
        m = a[i]/c[i-1]
        c[i] -= m*b[i-1]
        f[i] -= m*f[i-1]
    x[n-1] = f[n-1]/c[n-1]
    for i in range(n-2,-1, -1):
        x[i] = (f[i] - b[i]*x[i+1])/c[i]
    return x
