from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np
import math


def main():
    x0 = 0
    x1 = math.pi
    t0 = 0
    t1 = 5
    h = 0.1
    tau = h / 2
    func = lambda x, t: math.cos(x) * math.exp(-t)
    leftFunction = lambda t: 0
    rightFunction = lambda t: 0
    zeroFunction = lambda x: math.sin(x)
    zeroFunctionDerivative = lambda x: -math.sin(x)
    analyticSolution = lambda x, t: math.exp(-t) * math.sin(x)

    def firstLayerFiller(U, i, h, tau, func):
        x = x0 + i * h
        U[i, 1] = U[i, 0] + tau * zeroFunctionDerivative(x)

    def firstLayerFillerAccurate(U, i, h, tau, func):
        x = x0 + i * h
        t = t0 + tau
        U[i, 1] = U[i, 0] + (tau - 1.5 * tau * tau) * zeroFunctionDerivative(x)
        A = (U[i + 1, 0] - 2 * U[i, 0] + U[i - 1, 0]) / (h * h) + 0.5 * (U[i + 1, 0] - U[i - 1, 0]) / h - U[
            i, 0] - func(x, t)
        U[i, 1] += 0.5 * tau * tau * A

    accurateResult = solve(x0, x1, t0, t1, h, tau, leftFunction, rightFunction, zeroFunction, firstLayerFillerAccurate,
                           func)
    result = solve(x0, x1, t0, t1, h, tau, leftFunction, rightFunction, zeroFunction, firstLayerFiller, func)
    X = np.arange(x0, x1 + h, h)
    Y = np.arange(t0, t1 + tau, tau)
    accurateResultError = 0
    resultError = 0
    xN, tN = accurateResult.shape
    for n in range(tN):
        t = t0 + n * tau
        for i in range(xN):
            x = x0 + i * h
            trueResult = analyticSolution(x, t)
            accurateRes = accurateResult[i, n]
            res = result[i, n]
            accurateResultError = max(accurateResultError, math.fabs(trueResult - accurateRes))
            resultError = max(resultError, math.fabs(trueResult - res))
    print(accurateResultError)
    print(resultError)
    Y, X = np.meshgrid(Y, X)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(X, Y, accurateResult, rstride=1, cstride=1, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()


def solve(x0, x1, t0, t1, h, tau, leftFunction, rightFunction, zeroFunction, firstLayerFiller, func):
    xN = math.ceil((x1 - x0) / h) + 1
    tN = math.ceil((t1 - t0) / tau) + 1
    U = np.zeros((xN, tN))
    for i in range(tN):
        ti = t0 + i * tau
        U[0, i] = leftFunction(ti)
        U[xN - 1, i] = rightFunction(ti)
    for i in range(xN):
        xi = x0 + i * h
        U[i, 0] = zeroFunction(xi)
    for i in range(1, xN - 1):
        firstLayerFiller(U, i, h, tau, func)
    for n in range(2, tN):
        t = t0 + n * tau
        for i in range(1, xN - 1):
            x = x0 + i * h
            U[i, n] = (2 * U[i, n - 1] - U[i, n - 2]) / (tau * tau) + 1.5 * U[i, n - 2] / tau
            U[i, n] += (U[i - 1, n - 1] + U[i + 1, n - 1] - 2 * U[i, n - 1]) / (h * h)
            U[i, n] += 0.5 * (U[i + 1, n - 1] - U[i - 1, n - 1]) / h - U[i, n - 1] - func(x, t)
            U[i, n] /= 1 / (tau * tau) + 1.5 / tau
    return U


if __name__ == "__main__":
    main()
