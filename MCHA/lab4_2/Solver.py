from __future__ import division
from sympy import zeros


class Solver(object):
    """
    :type L:int
    :type T:int
    :type n:int
    :type m:int
    """

    def __init__(self, L, T, n, m):
        self.L = L
        self.T = T
        self.n = n + 1
        self.m = m + 1
        self.grid_function = zeros(self.m, self.n)
        self.time_vector = zeros(1, self.m)
        self.tau = T / m
        for i in range(self.m):
            self.time_vector[0, i] = self.tau * i
        self.coordinate_vector = zeros(1, self.n)
        self.h = L / n
        for i in range(self.n):
            self.coordinate_vector[0, i] = i * self.h

    def expicit_solve(self, zero_layer_function, zero_layer_derivative, a, left_condition, right_condition):
        """
        :type zero_layer_function:(float)->float
        :type zero_layer_derivative:(float)->float
        :type a:float
        :type left_conditon: (float)->float
        :type righ_condition: (float)->float
        """
        # get edge conditions
        for i in range(self.n):
            self.grid_function[0, i] = zero_layer_function(self.coordinate_vector[0, i])
        for i in range(1,self.m):
            self.grid_function[i, 0] = left_condition(self.grid_function[i - 1, 0], self.tau)
            self.grid_function[i, self.n - 1] = right_condition(self.grid_function[i - 1, self.n - 1], self.tau)
        for i in range(1, self.n - 1):
            self.grid_function[1, i] = self.grid_function[0, i] + self.tau * zero_layer_derivative(
                self.coordinate_vector[0, i])
        lambda_factor = a ** 2 * (self.tau) ** 2 / (self.h) ** 2
        for j in range(1, self.m - 1):
            for i in range(1, self.n - 1):
                self.grid_function[j + 1, i] = 2 * (1 - lambda_factor) * self.grid_function[j, i] + \
                                               lambda_factor * (self.grid_function[j, i + 1] +
                                                                self.grid_function[j, i - 1]) - self.grid_function[
                                                   j - 1, i]
        return self.grid_function

    def implicit_solve(self, zero_layer_function, zero_layer_derivative, a, left_condition, right_condition):
        """
        :type zero_layer_function:(float)->float
        :type zero_layer_derivative:(float)->float
        :type a:float
        :type left_conditon: (float)->float
        :type righ_condition: (float)->float
        """
        # get edge conditions
        for i in range(self.n):
            self.grid_function[0, i] = zero_layer_function(self.coordinate_vector[0, i])
        for i in range(1, self.m):
            self.grid_function[i, 0] = left_condition(self.grid_function[i - 1, 0], self.tau)
            self.grid_function[i, self.n - 1] = right_condition(self.grid_function[i - 1, self.n - 1], self.tau)
        for i in range(1, self.n - 1):
            self.grid_function[1, i] = self.grid_function[0, i] + self.tau * zero_layer_derivative(
                self.coordinate_vector[0, i])
        lambda_factor = (self.h) ** 2 / (a ** 2 * (self.tau) ** 2)
        for j in range(2, self.m):
            matrix_a = zeros(self.n - 2, 1)
            matrix_b = zeros(self.n - 2, 1)
            matrix_c = zeros(self.n - 2, 1)
            matrix_f = zeros(self.n - 2, 1)
            for i in range(self.n - 2):
                matrix_a[i, 0] = 1
                matrix_b[i, 0] = -(2 + lambda_factor)
                matrix_c[i, 0] = 1
                matrix_f[i, 0] = lambda_factor * (
                self.grid_function[j - 2, i + 1] - 2 * self.grid_function[j - 1, i + 1])
            matrix_x = self.tridiagonalMatrixSolve(matrix_a, matrix_b, matrix_c, matrix_f)
            for i in range(1, self.n - 1):
                self.grid_function[j, i] = matrix_x[i - 1, 0]
        return self.grid_function

    def tridiagonalMatrixSolve(self, a, c, b, f):
        n = a.shape[0]
        x = zeros(n, 1)
        for i in range(1, n):
            m = a[i, 0] / c[i - 1, 0]
            c[i, 0] -= m * b[i - 1, 0]
            f[i, 0] -= m * f[i - 1, 0]
        x[n - 1, 0] = f[n - 1, 0] / c[n - 1, 0]
        for i in range(n - 2, -1, -1):
            x[i, 0] = (f[i, 0] - b[i, 0] * x[i + 1, 0]) / c[i, 0]
        return x
