from Solver import Solver
import math
import numpy
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


def two_point_aprox_left(u_0_t, dt):
    return 0

def two_point_aprox_right(u_0_t, dt):
    return 0


def zero_layer_func(x):
    return math.sin(x) + math.cos(x)


def zero_layer_derivative(x):
    return -(math.sin(x) + math.cos(x))


if __name__ == "__main__":
    solver = Solver(math.pi, 1, 10, 10)
    x = []
    for i in range(solver.n):
        x.append(float(solver.coordinate_vector[0, i]))
    x = numpy.array(x)
    y = []
    for i in range(solver.m):
        y.append(float(solver.time_vector[0, i]))
    y = numpy.array(y)
    grid_values = solver.expicit_solve(zero_layer_func, zero_layer_derivative, 1, two_point_aprox_left, two_point_aprox_right)
    func = numpy.zeros((solver.m, solver.n))
    for i in range(grid_values.shape[0]):
        for j in range(grid_values.shape[1]):
            func[i, j] = grid_values[i, j]
    X, Y = numpy.meshgrid(x, y)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(X, Y, func, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    for i in range(grid_values.shape[0]):
        for j in range(grid_values.shape[1]):
            func[i, j] = math.sin(x[j] - y[i]) + math.cos(x[j] + y[i])
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(X, Y, func, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()
