import solver as sv
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np
import math

# var 3.12
def func(x, y):
    a = math.exp(-0.49 * x ** 2)
    return a
derivativeFunc = lambda x, y: -0.7 * x * math.exp(-0.49 * x ** 2)
anotherFunc = lambda x, y: math.sin(x + y)


def funcs(i, x, y):
    if i == 1:
        return -1 / 1
    elif i == 2:
        return -1 / 1
    elif i == 3:
        return 1.3*x-0.65*y-1


def main():
    x0 = -1
    x1 = 6.5
    y0 = -1
    y1 = 6.5
    h = 0.1
    tri = triangle(3, 6, 6, 0, 0, 0)
    result = sv.solve(x0, x1, y0, y1, h, 0.01, tri, funcs, func, derivativeFunc, anotherFunc)
    xN = math.ceil((x1 - x0) / h) + 1
    yN = math.ceil((y1 - y0) / h) + 1
    X, Y, Z = [], [], []
    for i in range(xN):
        x = x0 + i * h
        for j in range(yN):
            y = y0 + j * h
            if tri.contains(x, y):
                X.append(x)
                Y.append(y)
                Z.append(result[i, j])
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_trisurf(X, Y, Z, cmap=cm.coolwarm, linewidth=0.2)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()


class triangle(object):
    def __init__(self, x1, y1, x2, y2, x3, y3):
        """Starting upper-left point, clockwise order"""
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.x3 = x3
        self.y3 = y3
        assert sv.side(x1, y1, x2, y2, x3, y3) <= 0 and sv.side(x2, y2, x3, y3, x1, y1) <= 0 and \
               sv.side(x3, y3, x1, y1, x2, y2) <= 0, "order should be clockwise"

    def contains(self, x, y):
        a = sv.side(self.x1, self.y1, self.x2, self.y2, x, y)
        b = sv.side(self.x2, self.y2, self.x3, self.y3, x, y)
        c = sv.side(self.x3, self.y3, self.x1, self.y1, x, y)
        return not (a > 0 or b > 0 or c > 0)

    def isRegular(self, x, y, h):
        return self.contains(x - h, y) and self.contains(x + h, y) and \
               self.contains(x, y - h) and self.contains(x, y + h)

    def nearestBorderPoint(self, x, y, h):
        minDeltaX = 2*h
        minDeltaY = 2*h
        sideX = 0
        sideY = 0
        deltaX, deltaY = sv.deltaCrossings(self.x1, self.y1, self.x2, self.y2, x, y)
        if math.fabs(deltaX) <= math.fabs(minDeltaX):
            minDeltaX = deltaX
            sideX = 1
        elif math.fabs(deltaY) <= math.fabs(minDeltaY):
            minDeltaY = deltaY
            sideY = 1
        deltaX, deltaY = sv.deltaCrossings(self.x2, self.y2, self.x3, self.y3, x, y)
        if math.fabs(deltaX) <= math.fabs(minDeltaX):
            minDeltaX = deltaX
            sideX = 2
        elif math.fabs(deltaY) <= math.fabs(minDeltaY):
            minDeltaY = deltaY
            sideY = 2
        deltaX, deltaY = sv.deltaCrossings(self.x3, self.y3, self.x1, self.y1, x, y)
        if math.fabs(deltaX) <= math.fabs(minDeltaX):
            minDeltaX = deltaX
            sideX = 3
        elif math.fabs(deltaY) <= math.fabs(minDeltaY):
            minDeltaY = deltaY
            sideY = 3
        return minDeltaX, minDeltaY, sideX, sideY


if __name__ == "__main__":
    main()
