import numpy
import math


def side(aX, aY, bX, bY, cX, cY):
    """ :return
    >0, if point C is on the left side of vector A->B,
    <0, if point C is on the other  side,
    0, if point C lies on the line through AB"""
    return (bX - aX) * (cY - bY) - (cX - bX) * (bY - aY)


def crossings(aX, aY, bX, bY, cX, cY):
    """
    :return: (X, Y) such that points (X, cY) and (cX, Y) are crossings of line AB and
    horizontal and vertical lines that go through point C
    """
    if aX == bX:
        y1 = float('inf')
        x1 = aX
        return x1, y1
    if aY == bY:
        x1 = float('inf')
        y1 = aY
        return x1, y1
    a = (aY - bY) / (aX - bX)
    b = aY - a * aX
    y1 = a * cX + b
    x1 = (cY - b) / a
    return x1, y1


def deltaCrossings(aX, aY, bX, bY, cX, cY):
    """
    :return: (deltaX, deltaY) such that points (cX + deltaX, cY) and (cX, cY + deltaY) are crossings of line AB and
    horizontal and vertical lines that go through point C
    """
    x1, y1 = crossings(aX, aY, bX, bY, cX, cY)
    deltaX = x1 - cX
    deltaY = y1 - cY
    return deltaX, deltaY


def solve(x0, x1, y0, y1, h, eps, figure, funcs, func, derivativeFunc,
          anotherFunc):
    xN = math.ceil((x1 - x0) / h) + 1
    yN = math.ceil((y1 - y0) / h) + 1
    u = numpy.zeros((xN, yN))
    while True:
        currentEps = 0
        for i in range(1, xN - 1):
            x = x0 + i * h
            for j in range(1, yN - 1):
                y = y0 + j * h
                if not figure.contains(x, y):
                    u[i, j] = -1
                    continue
                if figure.isRegular(x, y, h):
                    old = u[i, j]
                    u[i, j] = h * derivativeFunc(x, y) * (u[i + 1, j] - u[i - 1, j]) / 2
                    u[i, j] += func(x, y) * (u[i + 1, j] + u[i - 1, j]) + u[i, j + 1] + u[i, j - 1]
                    u[i, j] -= h * h * anotherFunc(x, y)
                    u[i, j] /= 2 + 2 * func(x, y)
                    currentEps = max(currentEps, abs(old - u[i, j]))
                    continue
                deltaX, deltaY, sideX, sideY = figure.nearestBorderPoint(x, y, h)
                if sideX != 0:
                    u[i, j] = funcs(sideX, x + deltaX, y)
                elif sideY != 0:
                    u[i, j] = funcs(sideY, x, y + deltaY)
                else:
                    print(x, y, deltaX, deltaY, sideX, sideY)
        if currentEps < eps:
            break
    return u
