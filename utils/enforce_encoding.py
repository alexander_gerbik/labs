#!/usr/bin/env python3
"""
Enforce given files to have given encoding, which is done by detecting
encoding of file, decoding it and encoding the file in target encoding.
"""
import argparse
import chardet
import os
import shutil
import sys
from difflib import unified_diff


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('files', action='store', nargs='+',
                        help='Files to be processed')
    parser.add_argument('--html', action='store_true',
                        help='Should html charsets be updated '
                             'according to new encoding')
    parser.add_argument('-t', '--threshold', action='store', type=float,
                        default=0.9,
                        help='Do not modify file if confidence of '
                             'detecting encoding is below threshold. Must be '
                             'in range [0; 1], default: %(default)s')
    parser.add_argument('-e', '--encoding', action='store', default='utf-8',
                        help='Target encoding. Default: %(default)s')
    parser.add_argument('-b', '--backup-prefix', action='store', default='',
                        help='Leave blank to not to create backup file')
    args = parser.parse_args()

    for file in args.files:
        try:
            enforce(file, args.encoding, args.threshold, args.html,
                    args.backup_prefix)
        except Exception as e:
            print(file, e)


def enforce(file, encoding, threshold, html, prefix):
    with open(file, 'rb') as f:
        content = f.read()
    source_encoding, confidence = detect_encoding(content)
    if confidence < threshold:
        print(f'{file} is not processed, as confidence {confidence} is'
              f' not enough. Possible encoding: {source_encoding}')
        return
    text = content.decode(source_encoding)
    if html:
        original_text = text
        text = update_charsets(text, source_encoding, encoding)
        diff = unified_diff(original_text.splitlines(True),
                            text.splitlines(True), file, file)
        sys.stdout.writelines(diff)
    if prefix:
        shutil.move(file, file + prefix)
    text = text.encode(encoding)
    with open(file, 'wb') as f:
        f.write(text)


def update_charsets(text, source_enc, dest_enc):
    text = text.replace(f'charset={source_enc}', f'charset={dest_enc}')
    text = text.replace(f'charset="{source_enc}"', f'charset="{dest_enc}"')
    return text


def detect_encoding(content):
    summary = chardet.detect(content)
    encoding = summary['encoding']
    confidence = summary['confidence']
    return encoding, confidence


if __name__ == "__main__":
    main()
