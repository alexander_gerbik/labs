import datetime
import math
import os
import signal
import sys
import urllib
from collections import namedtuple
from urllib.parse import urlparse
from urllib.request import urlretrieve

import feedparser

podcasts = [
    ('http://teacherluke.libsyn.com/rss', 'luke_thompson'),
    ('https://talkpython.fm/episodes/rss', 'talk_python_to_me'),
    ('https://www.podcastinit.com/feed/mp3/', 'podcast_init'),
    ('https://pythonbytes.fm/episodes/rss', 'python_bytes'),
]
base_dir = os.path.dirname(__file__)
podcasts = [(i, os.path.join(base_dir, j)) for i, j in podcasts]
date_format = '%a, %d %b %Y %H:%M:%S %z'
Record = namedtuple('Record', 'link filename pub_date')


def main():
    for rss_link, podcast_folder in podcasts:
        downloaded = get_downloaded_files(podcast_folder)
        records = get_records(rss_link)
        to_download = [i for i in records if i.filename not in downloaded]
        download(to_download, podcast_folder)


def get_downloaded_files(folder):
    return set(os.listdir(folder))


def get_records(rss_url):
    parsed = feedparser.parse(rss_url)
    for entry in parsed['entries']:
        pub_date = datetime.datetime.strptime(entry['published'], date_format)
        link = next((l['href'] for l in entry['links'] if 'length' in l))
        file_name = get_file_name(link)
        yield Record(link, file_name, pub_date)


def get_file_name(link):
    parse_result = urllib.parse.urlparse(link)
    return os.path.basename(parse_result.path)


def download(to_download, folder):
    count = len(to_download)
    print(f"{count} files to download")
    with KeyboardInterceptor() as ki:
        for i, (link, filename, pub_date) in enumerate(to_download):
            if ki.interrupted:
                print("Interrupted, exiting...")
                sys.exit()
            print(f"Downloading {filename} ...")
            filename = os.path.join(folder, filename)
            urlretrieve(link, filename, reporthook=report_progress)
            change_modification_date(filename, pub_date)
            print(f"Done {i + 1} out of {count}.")


def report_progress(blocknum, bs, size):
    print_progress(blocknum * bs, size, bar_length=50)


def print_progress(iteration: int, total: int, prefix: str = '',
                   suffix: str = '', decimals: int = 2,
                   bar_length: int = 100):
    """
    Call in a loop to create terminal progress bar
        :param iteration:   - Required  : current iteration
        :param total:       - Required  : total iterations
        :param prefix:      - Optional  : prefix string
        :param suffix:      - Optional  : suffix string
        :param decimals:    - Optional  : number of decimals
        in percent complete
        :param bar_length:   - Optional  : character length of bar
    """
    iteration = total if iteration > total else iteration
    filled_length = int(round(bar_length * iteration / float(total)))
    percents = round(100.00 * (iteration / float(total)), decimals)
    frac, integer = math.modf(percents)
    percents_string = str(int(integer)).rjust(3) + '.' + str(
        int(frac * 10 ** decimals)).ljust(decimals)
    bar = '+' * filled_length + '-' * (bar_length - filled_length)
    sys.stdout.write(
        '\r{} |{}| {}% {}'.format(prefix, bar, percents_string, suffix))
    sys.stdout.flush()
    if iteration == total:
        sys.stdout.write('\n')
        sys.stdout.flush()


def change_modification_date(filename, date):
    date = int(date.timestamp())
    os.utime(filename, (date, date))


class KeyboardInterceptor:
    def __init__(self):
        self.interrupted = False

    def __enter__(self):
        def interrupted_handler(sig, frame):
            self.interrupted = True

        self._previous_handler = signal.getsignal(signal.SIGINT)
        signal.signal(signal.SIGINT, interrupted_handler)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        signal.signal(signal.SIGINT, self._previous_handler)


if __name__ == '__main__':
    main()
